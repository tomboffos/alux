<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@Index')->name('Index');
Route::get('portfolio.html','MainController@Projects')->name('Projects');
Route::get('web_portfolio.html','MainController@WebProjects')->name('WebProjects');
Route::get('mobile_portfolio.html','MainController@MobileProjects')->name('MobileProjects');
Route::get('branding_portfolio.html','MainController@BrandingProjects')->name('BrandingProjects');
Route::get('contacts.html','MainController@Contacts')->name('Contacts');
Route::get('customer-reviews.html','MainController@CustomerReviews')->name('CustomerReviews');
Route::get('about-us.html','MainController@AboutUs')->name('AboutUs');
Route::get('about.html','MainController@AboutUs')->name('AboutUs');

Route::get('internetmagazin.html','MainController@Shop')->name('Shop');
Route::get('korporativniysite.html','MainController@CorporateSite')->name('CorporateSite');
Route::get('razrabotkabrandbook.html','MainController@Brand')->name('Brand');
Route::get('videoinfografika.html','MainController@Video')->name('Video');
Route::get('smm.html','MainController@SMM')->name('SMM');
Route::get('landingpage.html','MainController@Landing')->name('Landing');
Route::get('services.html','MainController@Services')->name('Services');
Route::get('razrabotkamobilnihprilozheniy.html','MainController@MobileApps')->name('MobileApps');
Route::get('posts','MainController@Posts')->name('Posts');
Route::get('post/{id}','MainController@PostPage')->name('PostPage');
Route::get('game.html','MainController@Game')->name('Game');
Route::get('{url}','MainController@Work')->name('Work');
Route::get('admin/login','AdminController@Login')->name('Login');
Route::post('admin/log','AdminController@Log')->name('Log');



Route::name('admin.')->prefix('admin/')->middleware(['adminCheck'])->group(function(){
    Route::get('main','AdminController@Main')->name('Main');
    Route::post('update/post','AdminController@UpdatePost')->name('UpdatePost');
    Route::get('edit/post/{id}','AdminController@EditPost')->name('EditPost');
    Route::get('projects','AdminController@Projects')->name('Projects');
    Route::post('upload','AdminController@Upload')->name('Upload');
    Route::get('edit/project/{id}','AdminController@EditProject')->name('EditProject');
    Route::get('delete/{id}','AdminController@DeleteProject')->name('DeleteProject');
    Route::get('delete/WebDelete/{id}/{key}','AdminController@deleteWeb')->name('DeleteWeb');
    Route::get('custom/answer/{id}','AdminController@CustomAnswer')->name('CustomAnswer');
    Route::post('find/project','AdminController@FindProject')->name('FindProject');
    Route::get('settings/','AdminController@Settings')->name('Settings');
    Route::post('setting/edit','AdminController@SettingEdit')->name('SettingEdit');
    Route::get('logotypes','AdminController@Logotypes')->name('Logotypes');
    Route::post('add/logotype','AdminController@AddLogotype')->name('AddLogotype');
    Route::get('delete/logotype/{id}','AdminController@DeleteLogotype')->name('DeleteLogotype');
    Route::get('reviews','AdminController@Reviews')->name('Reviews');
    Route::get('delete/review/{id}','AdminController@DeleteReview')->name('DeleteReview');
    Route::post('create/review','AdminController@CreateReview')->name('CreateReview');
    Route::get('posts','AdminController@Posts')->name('Posts');
    Route::get('delete/post/{id}','AdminController@DeletePost')->name('DeletePost');
    Route::post('create/post','AdminController@CreatePost')->name('CreatePost');
    Route::get('meta/tags/','AdminController@MetaTags')->name('MetaTags');
    Route::get('certificates','AdminController@Certificates')->name('Certificates');
    Route::post('add/certificate','AdminController@AddCertificate')->name('AddCertificate');
    Route::get('tests','AdminController@Tests')->name('Tests');
    Route::get('delete/certificate/{id}','AdminController@DeleteCertificate')->name('DeleteCertificate');
    Route::post('create/test','AdminController@createTest')->name('createTest');
    Route::get('test/{id}','AdminController@Test')->name('Test');
    Route::post('create/step','AdminController@createStep')->name('createStep');
    Route::get('step/{id}','AdminController@Step')->name('Step');
    Route::post('answer','AdminController@createAnswer')->name('createAnswer');
    Route::get('delete/question/{id}','AdminController@DeleteQuestion')->name('DeleteQuestion');
    Route::get('delete/answer/{id}','AdminController@DeleteAnswer')->name('DeleteAnswer');
    Route::get('delete/custom/{id}','AdminController@DeleteCustom')->name('DeleteCustom');
    Route::post('update/project','AdminController@UpdateProject')->name('Update');
});







