<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('icon.png')}}" sizes="42x42">
    <title>
        {!! $title['content'] !!}
    </title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="keywords" content="{!!$meta_tag_keywords['content']!!}">
    <meta name="description" content="{!! $meta_tag_description['content'] ?? 0 !!}" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
   
    <link href="/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>

    <nav id="header_menu" class="navbar navbar-expand-md fixed-top d-none d-lg-block">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="img/logo_alux.svg" width="100px" ></a>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault" style="position:relative; top:-7px">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active nav_color">
                        <a class="nav-link nav_size nav_mar_top" href="/">Главная <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item nav_color">
                        <a class="nav-link nav_size nav_mar_top" href="about_us.php" tabindex="-1" aria-disabled="true">О нас</a>
                    </li>


                    <li class="nav-item dropdown nav_color">
                        <a class="nav-link nav_size nav_mar_top" href="services.php" target="_blank" id="dropdown01" aria-haspopup="true" aria-haspopup="true" aria-expanded="false">Наши услуги</a>
                        <div class="dropdown-menu hidden_menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item nav_color nav_size" href="e-commerce.php">Интернет магазин</a>
                            <a class="dropdown-item nav_color nav_size" href="landing.php">Лендинг пейдж</a>
                            <a class="dropdown-item nav_color nav_size" href="corporate.php">Корпоративный сайт</a>
                            <a class="dropdown-item nav_color nav_size" href="firm-design.php">Фирменный стиль</a>
                            <a class="dropdown-item nav_color nav_size" href="video.php">Видеографика</a>
                            <a class="dropdown-item nav_color nav_size" href="mob-apps.php">Мобильные приложения</a>
                            <a class="dropdown-item nav_color nav_size" href="smm.php">SMM продвижение</a>
                        </div>
                    </li>
                    <li class="nav-item nav_color">
                        <a class="nav-link nav_size nav_mar_top" href="portfolio.php" tabindex="-1" aria-disabled="true">Портфолио</a>
                    </li>
                    
                    <li class="nav-item nav_color">
                        <a class="nav-link nav_size nav_mar_top" href="contacts.php" tabindex="-1" aria-disabled="true">Контакты</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="btn btn-md btn_outline_white order-btn mt-3" href="#" tabindex="-1" aria-disabled="true">Заказать услугу</a>
                    </li>
                  

                </ul>
            </div>
        </div>
    </nav>
    
    
    <nav id="header_menu" class="navbar navbar-expand-md fixed-top  d-lg-none d-xs-block">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="img/logo_alux.svg"  width="100px" class="logo__img"></a>
             
            
        <content class="mob-view">
            
            <input type="checkbox" id="hamburger" class="hamburger ">
            <label for="hamburger" class="hamburger">
                <i></i>
                <text style="display: none"></text>
            </label>
           
            <section class="drawer-list">
                <ul>
                    <li>
                        <a href="index.html" data-id="1">Главная</a>
                    </li>
                    <li>
                        <a href="about.html" data-id="2">О нас</a>
                    </li>
                    <li class="dropdown">
                        <label for="dropdown">
                            <input type="hidden" class="ourService" value="{{route('Services')}}">
                            <a data-id="3">
                                <span style="display: inline" onclick="window.location.href = document.querySelector('.ourService').value">
                                    Наши услуги
                                </span>
                            </a>
                        </label>
                        <input type="checkbox" id="dropdown">
                        <ul>
                            <li>
                                <a href="e-commerceagazin.html">Интернет магазин</a>
                            </li>
                            <li>
                                <a href="landingpage.html">Лендинг пейдж</a>
                            </li>
                            <li>
                                <a href="korporativniysite.html">Корпоративный сайт</a>
                            </li>
                            <li>
                                <a href="razrabotkabrandbook.html">Фирменный стиль</a>
                            </li>
                            <li><a href="videoinfografika.html">Видеоинфографика</a></li>
                            <li><a href="razrabotkamobilnihprilozheniy.html">Мобильные приложения</a></li>
                            <li><a href="smm.html">SMM продвижение</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="portfolio.html" data-id="4">Наши работы</a>
                    </li>
                    <li>
                        <a href="otzyvy.html" data-id="5" target="_blank">Отзывы</a>
                    </li>
                    <li>
                        <a href="contacts.html" data-id="6">Контакты</a>
                    </li>
                    <li>
                        <a href="#" class="btn-hollow order-btn">Заказать
                            услугу</a>
                    </li>

                </ul>
            </section>
        </content>
            
        </div>
    </nav>
    
