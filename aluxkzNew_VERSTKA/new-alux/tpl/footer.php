<footer id="footer">
    <div class="container">
        <div class="row">

            <div class="col-lg-5 col-md-4 col" align="left">
                <img src="/img/logo_alux.svg" width="150px" class="footer_pb">
                <p class="white">COPYRIGHT © 2007-2021 ALMATY<br>LUXURY WEB DESIGN STUDIO </p>

                <div class="footer_pt">
                    <a href="#" target="_blank"><img src="img/fb.svg"></a>
                    <a href="https://www.instagram.com/aluxkz/" class="ml-4 mr-4" target="_blank"><img src="img/inst.svg"></a>
                    <a href="#" target="_blank"><img src="img/yt.svg"></a>
                </div>
            </div>

            <div class="col-lg-7 col-md-8 col" align="center">

               
                <nav id="header_menu" class="navbar navbar-expand-md d-none d-lg-block">
                    <div class="container">
                        <div>
                            <h5 class="white upper footer_pb20">Навигация</h5>
                        </div>
                    </div>
                    <div class="container footer_pb50">

                        <div class=" navbar-collapse" id="navbarsExampleDefault">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav_color nav_size" href="/">Главная <span class="sr-only">(current)</span></a>
                                </li>

                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="about_us.php" tabindex="-1" aria-disabled="true">О нас</a>
                                </li>


                                <li class="nav-item dropdown nav_mar">
                                    <a class="nav_color nav_size" href="services.php" target="_blank" id="dropdown01" aria-haspopup="true" aria-haspopup="true" aria-expanded="false">Наши услуги</a>

                                </li>
                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="works.php" tabindex="-1" aria-disabled="true">Портфолио</a>
                                </li>

                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="contacts.php" tabindex="-1" aria-disabled="true">Контакты</a>
                                </li>
                            </ul>

                        </div>


                    </div>
                    <div class="container ">
                        <div>
                            <h5 class="white upper footer_pb20">Контакты</h5>
                        </div>
                    </div>
                    <div class="container">

                        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav_color nav_size" href="/">+7 (727) 317-16-98</a>
                                </li>

                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="about_us.php" tabindex="-1" aria-disabled="true">+7 (705) 450-39-16</a>
                                </li>



                            </ul>

                        </div>


                    </div>
                </nav>
            </div>
        </div>
    </div>

</footer>









<a href="https://api.whatsapp.com/send?phone=+77054503916&amp;text=Здравствуйте, меня интересует разработка веб сайта." target="_blank" id="wa__toggle">
    <div class="circlephone"></div>
    <div class="circle-fill"></div>
    <div class="img-circle">
        <img src="../img/whatsapp_icon.png">
    </div>
</a>

<button type="button" class="btn btn-price btn_test btn-primary" data-toggle="modal" data-target="#exampleModalLong">
    <span>Узнать стоимость сайта</span>
</button>

<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="horizontal-position-modal-item">
                                <h5 class="modal-title" id="exampleModalLongTitle">{{$test->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background: transparent; outline: none; color: #FFFFFF;">
                                    <span style="outline: none; font-size: 4rem; font-weight: 300;" aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-subtitle-window">
                                Доброго времени суток, уважаемый посетитель сайта a-lux.kz. Мы подготовили для Вас короткий
                                опрос, который поможет нам в расчете стоимости необходимой Вам услуги.
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                @foreach($test->steps as $step)

                                <div class="col-lg-12 test_block  @if($loop->index == 0) active @endif" data-test-step="{{ $step->id }}" id="test_block-{{$step->id}}">
                                    <div class="range_block">

                                        <div class="range">

                                        </div>

                                    </div>
                                    <h3>
                                        {{$step->question}}
                                    </h3>

                                    <div class="choose_content">
                                        @foreach($step->answers as $answer)
                                        <button data-answer='{{$answer->id}}' data-next-question='{{$answer->next_step_id}}' data-step='{{$step->id}}' class="choose_button choose_{{$step->id}}">
                                            {{$answer->answer}}
                                        </button>
                                        @endforeach

                                        <form action="{{route('admin.createAnswer')}}" method="post" class="style-form-width">
                                            {{csrf_field()}}
                                            <div class="form-group mt-2">
                                                <input data-answer='{{$answer->id}}' data-next-question='{{$answer->next_step_id}}' data-step='{{$step->id}}' type="text" id="answer_type_{{ $step->id }}" class="form-control answer-input choose_button choose_{{$step->id}}" value="" name="answer-input" placeholder="Свой вариант ответа">
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                @endforeach
                                <div class="col-lg-12 main_setting">
                                    <div class="d-flex align-items-center" style="
										  display: flex;
										  justify-content: flex-end;
										  align-items: center;
									 ">
                                        <button class="previous_arrow" id="previous">
                                            <span9>Назад</span>
                                        </button>


                                        <button disabled class="next_button" id="next_button">
                                            Вперед
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>




<input type="hidden" id="steps_count" value="{{count($test->steps)}}">
<script>
    let nextStep = 0;
    var counter = $("#steps_count").val(),
        step = 1,
        main = "",
        step_percent = 0,
        percent_one = 100 / counter;
    step_percent += percent_one, $(document).ready(function() {
            console.log("hi");
            var e = 100 - Math.ceil(step_percent);
            $(".range").css("background", "linear-gradient(to right, #406BFF " + Math.ceil(step_percent /
                    2) +
                +",#406BFF " + Math.ceil(step_percent / 2) +
                "%, #E3E3E3 " + e + "%)")
        }), $("#previous").on("click", function() {
            (step -= 1) > 0 ? ($(".test_block.active").removeClass("active"), $(".test_block:nth-child(" +
                step + ")").addClass("active")) : step += 1
        }),
        $("#next_button").on("click", function() {
            $(this).attr('data-next-step', nextStep)
            let currentStepId = $('.test_block.active').attr('data-test-step')
            console.log(currentStepId)
            console.log(document.getElementById(`answer_type_${currentStepId}`).value)
            if ($(`.answer_type_${currentStepId}`).val !== '') {
                $.ajax({
                    type: "POST",
                    url: "/api/request/test/custom/answer",
                    data: {
                        title: document.getElementById(`answer_type_${currentStepId}`).value,
                        step_id: currentStepId
                    },
                    // success: function(e) {
                    //     alert(e.msg)
                    //     $('.modal-content, .modal-backdrop, .btn_test').hide(),
                    //     $('.modal-backdrop').removeClass('show')
                    // }
                })
            }
            if (nextStep === '') console.log("ok"), $.ajax({
                type: "POST",
                url: "/api/request/test",
                data: {
                    answers: JSON.stringify(answers)
                },
                success: function(e) {
                    alert(e.msg)
                    $('.modal-content, .modal-backdrop, .btn_test').hide(),
                        $('.modal-backdrop').removeClass('show')
                }
            });
            else {
                step += 1;
                var e = 100 - (step_percent += percent_one);
                console.log(step_percent),
                    $(".test_block.active").removeClass("active"),
                    $(".range").css("background", "linear-gradient(to right, #406BFF " + step_percent +
                        "%, #E3E3E3 " + e + "%)")
                console.log(`test_block-${nextStep}`)
                $(`#test_block-${nextStep}`).addClass("active")
            }
        });
    var answers = [];
    $(".choose_button").on("click", function() {
        $('.next_button').prop("disabled", false);
        $('.next_button').css({
            'background': 'linear-gradient(90deg, #456FFF -15.22%, #003AFF 150%), #3A66FF',
            'color': '#fff'
        })
        var e = $(this).attr("data-step");
        nextStep = $(this).attr('data-next-question')
        console.log($(this).attr('data-next-question'))
        parseInt(e) === parseInt(step) && $(".choose_" + e + ".active").removeClass("active"), $(this)
            .toggleClass("active"), main = $(this).attr("data-answer");
        $(this).siblings().removeClass('active');

        var t = {
                step_id: parseInt(e),
                answer_id: parseInt(main)
            },
            s = 0;
        for (let t of Object.values(answers)) t.step_id === parseInt(e) && (t.answer_id = parseInt(
            main), s = 1);
        0 === s && answers.push(t), console.log(answers)
        $('.answer-input').on('click', function() {
            $('.choose_button').removeClass('active')
        });
    });

</script>


<script src="../js/sweetalert.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdn.rawgit.com/tonystar/bootstrap-hover-tabs/v3.1.1/bootstrap-hover-tabs.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
<link rel="stylesheet" href="http://kenwheeler.github.io/slick/slick/slick-theme.css">
<script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="js/scripts.js" type="application/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

</body>

</html>
