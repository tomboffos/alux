<?php include('tpl/header.php'); ?>


<section class="first_screen">
    <div class="container d-flex">
        <div class="d-flex content">
            <div class="circle">
                <div class="sm_circle">
                    <img src="img/hands_with_hat.svg" alt="" class="hands_with_hat">
                </div>
            </div>
            <div class="text_center part">
                Теперь Вы точно получите то, что планировали <br> за те деньги, на которые расcчитывали
            </div>
        </div>
    </div>
</section>
<section class="second_screen">
    <div class="container">
        <div class="d-flex nav_item_bar">
            <div class="offer_text">
                Посмотрите наши свежие <br> кейсы в различных сферах
            </div>
            <div class="divider">

            </div>
            <div class="content_menu">
                <h5 class="">
                    <a href="{{route('Projects')}}">
                        Все сферы
                    </a>
                </h5>
                <span>

                </span>
                <h5 class="">
                    <a href="{{route('WebProjects')}}">
                        Web
                    </a>
                </h5>
                <span>

                </span>
                <h5>

                    <a href="{{route('MobileProjects')}}">
                        Mobile
                    </a>
                </h5>
                <span>

                </span>
                <h5>
                    <a href="{{route('BrandingProjects')}}">
                        Branding
                    </a>
                </h5>

                <span>

                </span>
                <h5>
                    <a href="{{route('BrandingProjects')}}">
                        SEO/SMM
                    </a>
                </h5>
                <span>

                </span>
                <h5>
                    <a href="{{route('BrandingProjects')}}">
                        SRM
                    </a>
                </h5>


            </div>
            
            
            
        </div>


    </div>
</section>
<section class="third_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="img/setting.svg" alt="">
                    <div></div>
                </div>
                <h5>Разработка веб сайтов</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="img/cubes.svg" alt="">
                    <div></div>
                </div>
                <h5>Разработка CMS,CRM систем любой сложности</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="img/cube.svg" alt="">
                    <div></div>
                </div>
                <h5>Разработка приложений и игр для мобильных устройств</h5>
                <button>Заказать</button>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="img/art.svg" alt="">
                    <div></div>
                </div>
                <h5>Разработка брендбука, фирменного стиля, полиграфический дизайн</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="img/tools.svg" alt="">
                    <div></div>
                </div>
                <h5>Техническая поддержка</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="img/brain.svg" alt="">
                    <div></div>
                </div>
                <h5>SEO, SMM, SERM, контекстная реклама</h5>

                <button>Заказать</button>
            </div>
        </div>
    </div>

</section>
<section class="fourth_screen">
    <div class="container">
        <div class="row stylized">
            <div class="col-lg-6 size-images-about">
                <img src="/img/about-images.jpg" alt="">
            </div>
            <div class="col-lg-6 third_text">
                <h3>
                    Только <span> современные технологии </span> и <br> проекты любого уровня сложности.
                </h3>
                <p>
                    Ваш сайт будет адаптирован под все существующие устройства. Для управления будет использована наша
                    собственная, интуитивно понятная и в то же время функциональная система управления сайтами "LUX
                    CMS",которая вам позволит управлять и получать новые заказы с вашего сайта как через ПК так через
                    мобильный телефон. Мы в состоянии оперативно разработать проект любой сложности под ключ. Беремся за
                    те работы, от которых другие отказались.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="sixth_screen">
    <div class="container">
        <div class="row">

            <div class="sixth_screen_row">
                <div class="sixth_screen_column">
                    <div class="icon_field">
                        <img src="img/arrow.svg" alt="">
                    </div>
                    <h5 class="bold title_services">
                        Работаем <span> до полного утверждения </span> Заказчиком
                    </h5>
                    <p>
                        Заказав разработку веб сайта,логотипа или мобильного приложения у нас Вы можете быть уверены,что
                        <span> 100 % достигните нужного результата.</span> Мы работаем до Вашего полного утверждения и
                        готовы удовлетворить даже самых требовательных клиентов
                    </p>
                </div>
                <div class="sixth_screen_column mt-4">
                    <div class="icon_field">
                        <img src="img/frame.svg" alt="">
                    </div>
                    <h5 class="bold title_services">
                        <span>Бесплатная</span> техническая поддержка
                    </h5>
                    <p>
                        Мы предоставляем бесплатную техническую поддержку сроком на <span> 6 месяцев </span> на все веб
                        сайты разработанные нашей веб студией. Автоматическое бекапирование данных, антивирусная система и
                        еще много приятных бонусов, которые позволят Вашему веб сайту всегда находиться в боевом состоянии и
                        быть доступным для Ваших клиентов 24/7
                    </p>
                </div>
            </div>

            <div class="col-lg-12">
                <hr>
            </div>
        </div>
    </div>
</section>
<section class="seventh_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 title_seven">
                <h1>
                    LUX<span>CMS</span>
                </h1>
            </div>
            <div class="col-lg-6 text_seven">
                <p>
                    Модуль создания и редактирования <br> разделов и подразделов сайта <br> <span> (Неограниченное
                        количество страниц любой вложенности)</span>
                </p>
            </div>
        </div>
    </div>
</section>
<div class="eighth_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="d-flex pointer">
                    <div class="props_guide d-flex"><img src="img/vector.svg" alt="">
                        <p>
                            Возможность управления структурой сайта <br> "в один клик"
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="img/image.svg" alt="">

                        <p>
                            Работа с графикой <br> (автоматическое масштабирование <br> изображений)
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="img/loop.svg" alt="">
                        <p>Поиск по сайту</p>
                    </div>
                    <div class="props_guide d-flex"><img src="img/card.svg" alt="">
                        <p>
                            Форма обратной связи <br> (неограниченное количество)
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="img/figures.svg" alt="">
                        <p>
                            Модуль новостей с выведением всех новостей списком и постраничного выведения каждой новости
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="img/loop_second.svg" alt="">
                        <p>
                            Статистика посещений, курсы валют,погода,время
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="img/document.svg" alt="">
                        <p>
                            Визуальный редактор сайта <br> (аналог Microsoft Word)
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="img/setting_page.svg" alt="">
                        <p>
                            Использование технологии AJAX
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 screen_card">
                <div class="card">
                    <h5>
                        Модуль <br> для самостоятельной <br> оптимизации сайта <br> под поисковые системы
                    </h5>
                    <p>
                        Добавление ключевых слов и meta тэгов для каждой страницы в отдельности
                    </p>
                    <p>
                        Автоматически генерируемый файл sitemap для поисковых машин
                    </p>
                    <button>
                        Заказать CMS
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="nineth_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-8">
                <div class="d-flex">
                    <button class="mains">SMM</button>
                    <button class="mains">SEO</button>
                </div>
                <div class="nine_text">
                    <span class="bold">
                        Создание эффективных сайтов
                    </span>
                    <p class="mt-2">
                        Мы не просто создаем красивые и удобные сайты. <br>
                        Мы максимально эффективно настраиваем и запускаем рекламную компанию для <br>
                        каждого проекта индивидуально. Что позволяет нашим клиентам получать прибыль уже с 
                        первых дней работы сайта разработанного нами.
                    </p>
                    <button>
                        Заказать ПРОДВИЖЕНИЕ
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer_black">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 d-flex footer_props">
                <img src="img/mark.svg" alt="">
                <p>
                    Республика Казахстан, г. Алматы, <br>
                    Ул. Шевченко 165Б, офис 511
                </p>
            </div>
            <div class="col-lg-4 d-flex footer_props">
                <img src="img/phone_call.svg" alt="">
                <p>
                    +7 (727) 317-16-98 <br>
                    +7 (705) 450-39-16
                </p>
            </div>
            <div class="col-lg-4 d-flex footer_props">
                <img src="img/mail.svg" alt="">
                <p>
                    info@a-lux.kz
                </p>
            </div>
        </div>
    </div>
</div>


<?php include('tpl/footer.php'); ?>
