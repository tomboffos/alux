<?php include('tpl/header.php'); ?>

<section id="about_page_sec_1">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">

                <h1 class="white bold title_page_about">Любой <span class="t_blue">КЛИЕНТ</span>, для нас в первую очередь – <span class="t_blue">ДРУГ!</span></h1>
                <br>
                <p class="white text_about_us">Вот уже второй десяток лет мы выстраиваем долгосрочные взаимовыгодные отношения с нашими клиентами.</p>
                <br>
                <p class="white text_about_us">Наша бизнес модель построена таким образом, что наши доходы напрямую зависят от доходов наших партнеров.<br> Это гарантирует нашу максимальную заинтересованность в росте Ваших продаж</p>
            </div>

            <div class="col-lg-6">
                <div class="rel_block"><img src="img/round_blue_1.png" class="about_round_blue d-none d-lg-block"></div>
                <div class="pt_play"><a href="#" class="intro-banner-vdo-play-btn pinkBg " target="_blank">
                        <i class="glyphicon glyphicon-play whiteText " aria-hidden="true"><img src="img/play.svg" width="30px" class="mt-2 ml-1"></i>
                        <span class="ripple pinkBg"></span>
                        <span class="ripple pinkBg"></span>
                        <span class="ripple pinkBg"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about_page_sec_2">
    <div class="rel_block"><img src="img/2_icon.png" class="cube_block_2"></div>
    <div class="rel_block"><img src="img/3_icon.png" class="cube_block_3"></div>
    <div class="container">
        <div class="rel_block"><img src="img/1_icon.png" class="cube_block_1"></div>

        <div class="row text-center">


            <div class="col-lg-4 col-md-4 mar_about400">
                <img src="img/400+.svg">
                <p class="mt-4 text_about_us">веб-сайтов</p>
            </div>

            <div class="col-lg-4 col-md-4 mar_about14  animate__animated animate__fadeIn">
                <img src="img/14_alux.png">
                <p class="mt-4 text_about_us">лет на рынке</p>
            </div>

            <div class="col-lg-4 col-md-4 mar_about30">
                <img src="img/30+.svg">
                <p class="mt-4 text_about_us">мобильных приложений</p>

                <div class="rel_block"><img src="img/4_icon.png" class="cube_block_4"></div>
            </div>
        </div>
    </div>
</section>
<section id="about_page_sec_3">
    <div class="container">
        <div class="row">


            <div class="col-lg-6">
                <h3 class="titles_about_text">Не хотите тратить время<br> на чтение материала?</h3>
                <p class="mini_text">Можете познакомиться с нашей<br class="d-md-none d-xs-block"> веб-студией<br class="d-none d-xs-block"> с помощью презентации</p>
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3 ">
                <div class="pdf_position">
                    <img src="img/pdf.png">
                    <a href="#" class="download_btn"><button class="btn btn-md btn_new_blue"><img src="img/download.svg" class="mr-1"> Скачать PDF файл</button></a>
                </div>
            </div>


        </div>
    </div>
</section>

<section id="about_page_sec_4">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <h3 class="titles_about_text">Веб студия «Алматы Люкс» основана в 2007 году. За это время было реализовано более 400 веб проектов различной сложности.</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 pt-255">
                <img src="img/gray_alux.png" class="gray_alux">
                <p class="mini_text">Над каждым сайтом работают профессионалы в области: программирования, дизайна, рекламы и интернет-маркетинга.</p>
                <br>
                <p class="mini_text">В данный момент наша команда<br class="d-md-none d-xs-block"> состоит из 11 человек стремящихся преуспеть в своем деле.</p>
            </div>
            <div class="col-lg-7 pt_team" align="center">
                <img src="img/photo_team.jpg" class="w-80 image_team">
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12 pt-100 text-center">
                <div class="row justify-content-center ">
                    <div class="col-lg-10">
                        <p class="mini_text t_align_about_page">Очень многие Веб студии Алматы предложат Вам свои услуги, но при создании сайта, мы, в первую очередь, думаем о Ваших клиентах и создаем привлекательный дизайн, формирующий позитивное впечатление о компании и настраиваем максимально эффективно Вашу рекламную компанию в Google, Yandex и Соц сетях. Так же детально продумываем мобильную версию веб&nbsp;сайта..</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="about_page_sec_5">
    <div class="container bg_blue_mac rel_block">
        <div class="row">

            <div class="col-lg-6 pl_text_about_page">

                <p class="mini_text">В то же время, большое внимание мы уделяем<br class="d-none d-lg-block"> структуре сайта. Это делается для того, чтобы<br class="d-none d-lg-block"> Ваш потенциальный клиент мог легко найти<br> интересующую его информацию.</p>

                <p class="mini_text mt-4">Как результат, гармоничное сочетание<br> красоты и юзабилити делает пребывание<br> Вашего клиента на сайте комфортным,<br> приятным, а главное перспективным!</p>
            </div>
            <div class="col-lg-6">
                <img src="img/mac.png" class="mac_image d-none d-lg-block">
                <img src="img/mac_mob.png" class="mac_image d-lg-none d-xs-block">
            </div>
        </div>
    </div>
</section>


<section id="about_page_sec_6">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 text-center mb-5">
                <h3 class="smi">СМИ о нас</h3>
            </div>

            <div class="col-lg-4 mt-4">
                <img src="img/forbes.png">
            </div>

            <div class="col-lg-4 mt-4">
                <img src="img/habar.png">

            </div>

            <div class="col-lg-4 mt-4">
                <img src="img/euraair.png">

            </div>
        </div>
    </div>
</section>


<section id="about_page_sec_7">
    <div class="container rel_block">
        <div class="row qoute_ivan">
            <img src="img/quote.svg" class="qoute_icon">
            <div class="col-lg-6">

                <p class="mini_text white pl-100">Каждый вечер 95 % всех активов моей компании разъезжаются на машинах по домам. Моя задача - создать такие условия труда, чтобы на следующее утро у всех этих людей возникло желание вернуться обратно. Креативность, которую они приносят в компанию, создает конкурентное преимущество.</p>
                <p class=" white mt-4  qoute_ivan_text pl-100">— Востриков Иван Сергеевич</p>

            </div>
            <div class="col-lg-6">
                <img src="img/ivan.png" class="ivan_image ">

            </div>
        </div>
    </div>
</section>





<section id="about_page_sec_8">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center mb-5">
                <h3 class="smi" align="left">Наши атрибуты качества и успеха</h3>
            </div>

            <div class="col-lg-4">
                <img src="img/team.svg">
                <p class="mini_text mt-3">Мы профессиональная команда<br> web–дизайнеров, программистов <br>и руководителей проектов</p>
            </div>

            <div class="col-lg-4">
                <img src="img/time-and-date.svg">
                <p class="mini_text mt-3">Мы предоставляем пожизненную гарантию на создаваемые нами <br>сайты</p>
            </div>

            <div class="col-lg-4">
                <img src="img/calendar.svg">
                <p class="mini_text mt-3">Мы создаем сайты за<br> максимально короткие сроки<br>(от 5 рабочих дней)</p>

            </div>

            <div class="w-100 mt-5 pt-4"></div>
            <div class="col-lg-4">
                <img src="img/coding.svg">
                <p class="mini_text mt-3">Мы предлагаем нашим клиентам полный комплекс услуг в <br>Интернете: Создание сайтов,<br> мобильных приложений и <br> фирменного стиля, продвижение<br> в поисковых системах<br> (Google, Яндекс)</p>
            </div>

            <div class="col-lg-4">
                <img src="img/web-development.svg">
                <p class="mini_text mt-3">Мы создаем сайты на собственной Системе Управления Сайтами LUX CMS, что говорит о высоком уровне навыков наших программистов</p>
            </div>

            <div class="col-lg-4">
                <img src="img/laptop.svg">
                <p class="mini_text mt-3">Мы предлагаем высокоэффективную настройку и ведение рекламных компаний в поисковых системах и социальных сетях бесплатно для сайтов разработанных в нашей веб студии</p>

            </div>

        </div>
    </div>
</section>

<?php include('tpl/footer.php'); ?>
