<?php include('tpl/header.php'); ?>

<header class="top">
    <div class="owl-carousel owl-theme main_home_slider owl-loaded owl-drag">
        


        
    <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-3777px, 0px, 0px); transition: all 0s ease 0s; width: 7554px;"><div class="owl-item cloned" style="width: 1259px;"><div class="main_home_slider_item">
            <div class="main_home_slider_item_bg item-first" style="background-image: url(img/slide-banner.png); background-position-x: 50%;"></div>
            <div class="container item-first-text">
                <div class="main_home_slider_item_inner right">
                    <div class="main_home_slider_item_content">
                        <!--												<img src="img/logo.png" class="mobile-logo-alux">-->
                        <div class="main_home_slider_item_title" style="line-height: 1;margin-bottom: 30px;">
                            <span style="font-size: 2.2rem;margin-bottom: 35px;" class="color_blue">Создание уникальных корпоративных веб-сайтов в Казахстане.</span><br>
                        </div>

                        <div class="main_home_slider_item_title" style="margin-bottom: 30px;">
                            <span style="font-size: 2.7rem;margin-bottom: 50px;" class="uppercase">s<span class="color_blue uppercase">mart.</span> s<span class="color_blue uppercase">imple.</span> s<span class="color_blue uppercase">uperawesome</span></span>
                        </div>
                        <div class="main_home_slider_item_desc">
                            На протяжении 13 лет мы занимаемся успешным созданием уникальных веб-сайтов класса LUX. Благодаря комплексному подходу и качественной разработке, желаемый результат гарантируется. Веб-студия «A-Lux» занимается созданием сайтов различной сложности, начиная от landing page, и заканчивая крупными корпоративными проектами.

                        </div>

                    </div>
                </div>
            </div>
        </div></div><div class="owl-item cloned" style="width: 1259px;"><style>
            .main_home_slider .owl-item.active .main_home_slider_item_bg {
                animation: none;
            }

        </style></div><div class="owl-item" style="width: 1259px;"><div class="main_home_slider_item">
            <div class="main_home_slider_item_bg item-first" style="background-image: url(img/slide-banner.png); background-position-x: 50%;"></div>
            <div class="container item-first-text">
                <div class="main_home_slider_item_inner right">
                    <div class="main_home_slider_item_content">
                        <!--												<img src="img/logo.png" class="mobile-logo-alux">-->
                        <div class="main_home_slider_item_title" style="line-height: 1;margin-bottom: 30px;">
                            <span style="font-size: 2.2rem;margin-bottom: 35px;" class="color_blue">Создание уникальных корпоративных веб-сайтов в Казахстане.</span><br>
                        </div>

                        <div class="main_home_slider_item_title" style="margin-bottom: 30px;">
                            <span style="font-size: 2.7rem;margin-bottom: 50px;" class="uppercase">s<span class="color_blue uppercase">mart.</span> s<span class="color_blue uppercase">imple.</span> s<span class="color_blue uppercase">uperawesome</span></span>
                        </div>
                        <div class="main_home_slider_item_desc">
                            На протяжении 13 лет мы занимаемся успешным созданием уникальных веб-сайтов класса LUX. Благодаря комплексному подходу и качественной разработке, желаемый результат гарантируется. Веб-студия «A-Lux» занимается созданием сайтов различной сложности, начиная от landing page, и заканчивая крупными корпоративными проектами.

                        </div>

                    </div>
                </div>
            </div>
        </div></div><div class="owl-item active" style="width: 1259px;"><style>
            .main_home_slider .owl-item.active .main_home_slider_item_bg {
                animation: none;
            }

        </style></div><div class="owl-item cloned" style="width: 1259px;"><div class="main_home_slider_item">
            <div class="main_home_slider_item_bg item-first" style="background-image: url(img/slide-banner.png); background-position-x: 50%;"></div>
            <div class="container item-first-text">
                <div class="main_home_slider_item_inner right">
                    <div class="main_home_slider_item_content">
                        <!--												<img src="img/logo.png" class="mobile-logo-alux">-->
                        <div class="main_home_slider_item_title" style="line-height: 1;margin-bottom: 30px;">
                            <span style="font-size: 2.2rem;margin-bottom: 35px;" class="color_blue">Создание уникальных корпоративных веб-сайтов в Казахстане.</span><br>
                        </div>

                        <div class="main_home_slider_item_title" style="margin-bottom: 30px;">
                            <span style="font-size: 2.7rem;margin-bottom: 50px;" class="uppercase">s<span class="color_blue uppercase">mart.</span> s<span class="color_blue uppercase">imple.</span> s<span class="color_blue uppercase">uperawesome</span></span>
                        </div>
                        <div class="main_home_slider_item_desc">
                            На протяжении 13 лет мы занимаемся успешным созданием уникальных веб-сайтов класса LUX. Благодаря комплексному подходу и качественной разработке, желаемый результат гарантируется. Веб-студия «A-Lux» занимается созданием сайтов различной сложности, начиная от landing page, и заканчивая крупными корпоративными проектами.

                        </div>

                    </div>
                </div>
            </div>
        </div></div><div class="owl-item cloned" style="width: 1259px;"><style>
            .main_home_slider .owl-item.active .main_home_slider_item_bg {
                animation: none;
            }

        </style></div></div></div><div class="owl-nav disabled"><div class="owl-prev">prev</div><div class="owl-next">next</div></div><div class="owl-dots"><div class="owl-dot">1</div><div class="owl-dot active">2</div></div></div>
</header>



<div class="col-md-12 col-sm-12 blockk1">
    <script type="application/ld+json">
        {
            "@context": "http://www.schema.org",
            "@type": "Organization",
            "name": "A-Lux",
            "url": "https://www.a-lux.kz/",
            "logo": "https://www.a-lux.kz/img/logo.png",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Ул. Шевченко 165 Б, офис. 511",
                "addressLocality": "Алматы",
                "addressRegion": "Алматинская область",
                "postalCode": "050009",
                "addressCountry": "Казахстан"
            },
            "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "+77273171698",
                "email": "info@a-lux.kz",
                "contactType": "customer service",
                "availableLanguage": "Russian"
            }
        }

    </script>

    <div class="conwrap">
        <h1 class="text-uppercase">МЫ ВЛИВАЕМСЯ В СФЕРУ ФУНЦИОНИРОВАНИЯ БИЗНЕСА НАШИХ КЛИЕНТОВ</h1>
        <p class="italict">ОБЕСПЕЧИВАЕМ КАЧЕСТВЕННЫЙ И ИНДИВИДУАЛЬНЫЙ ПОДХОД</p>
        <div class="col-md-8 utp">
            <div class="row">
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(1).png alt="">
                    <p>Проводим детальной анализ конкурентоспособной среды.</p>
                </div>
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(2).png alt="">
                    <p>Тщательно исследуем целевую аудиторию в целях правильной расстановки приоритетов.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(3).png alt="">
                    <p>Мы работаем до полного утверждения сайта заказчиком. Количество макетов неограниченно.</p>
                </div>
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(4).png alt="">
                    <p>Даем полную гарантию на бесперебойное и надежное функционирование сайта.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 utp-list">
            <h2 class="text-uppercase">цели и задачи <br> корпоративного сайта</h2>
            <div class="utp-list-text">
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Значительное увеличение клиентов</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Повышение уровня вашего бизнеса в глазах конкурентов</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Формирование серьезного и солидного имиджа</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Предоставление информации обо всех направлениях деятельности компании</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Возможность обратной связи и консультация в режиме онлайн</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Повышение лояльности посетителей</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-sm-12 block2">
    <div class="conwrap">
        <div class="brif-content col-md-5 col-sm-12">
            <h1 class=" text-uppercase">Бриф на разработку корпоративного сайта</h1>
            <p>Заполненный опросник, именуемый брифом, окажет значительное влияние на скорость и получение желаемого результата, ведь благодаря четкому пониманию требований и пожеланий, процесс разработки дизайна будет осуществляться гораздо быстрее. </p>
            <label>
                <a download href="Бриф на дизайн (format).rtf">
                    <span>Скачать бриф</span>

                </a></label>
        </div>
        <div class="col-md-7">
        </div>
    </div>
</div>
<div class="col-md-12 col-sm-12 block3 mb-5">
    <div class="conwrap">
        <div class="row">
            <h1 class="text-uppercase">Примеры корпоративных сайтов,</h1>
            <p class="italict">созданных нашей веб студией</p>
            <img src="img/line-tran.png" alt="" style="margin: auto;display: block;">
            <div class="col-md-4 col-sm-12 utp-item1">
                <img src=img/1200px-VTB_Logo_2018.png alt=""><br>
                <div class="block3-itext" style="padding: 19px 10px;line-height: 1;">ВТБ Банк</div>
            </div>
            <div class="col-md-4 col-sm-12 utp-item2">
                <img src=img/logo_aziakredit.png alt="">
                <div class="block3-itext">AsiaCredit Bank</div>
            </div>

            <div class="col-md-4 col-sm-12 utp-item3">
                <img src=img/LG-lifes-good-gray-lettering.png alt="">
                <div class="block3-itext">LG</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12 utp-item4">
                <img src=img/logo_small.png alt=""><br>
                <div class="block3-itext">iPoint</div>
            </div>
            <div class="col-md-4 col-sm-12 utp-item5">
                <img src=img/asialife-logo.png alt="">
                <div class="block3-itext">Asia Life</div>
            </div>

            <div class="col-md-4 col-sm-12 utp-item6">
                <img src=img/payment-method-logo-homebank.png alt="">
                <div class="block3-itext">HOMEBANK.</div>
            </div>
        </div>
        <br><br>
        <span><a href="/portfolio.html">Все работы</a></span>
        <br><br>
    </div>
    <div class="row">
        <div class="form-wrapp">
            <h2>заявка на разработку сайта</h2>
            <form action="" id="btn-hollow1">
                <input type="text" name="name" placeholder="Представьтесь, пожалуйста">
                <input type="text" name="phone" placeholder="Как с Вами связаться?"><br><br>
                <textarea name="msg" id="" cols="97" rows="3" placeholder="Опишите задачу" style="margin-bottom:20px;"></textarea>
                 </form>
             <a class="btn-hollow order-btn" href="#" style="padding: 14px 53px!important; color: #4490c3;">заказать</a>
        </div>
    </div>
    <div class="row">
        <div class="conwrap">
            <h1 class="text-uppercase">Над вашим сайтом будут работать</h1>
            <p class="italict">Лучшие профессионалы своего дела</p>
            <div class="col-md-12 col-sm-12 utp">
                <div class="row">
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/secretary.png alt="">
                        <p class="text-uppercase">Личный менеджер проекта</p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/programmer.png alt="">
                        <p class="text-uppercase">верстальщики</p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/training.png alt="">
                        <p class="text-uppercase">seo-оптимизаторы</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/paint-palette.png alt="">
                        <p class="text-uppercase"> ui / ux дизайнеры </p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/learning.png alt="">
                        <p class="text-uppercase"> программисты </p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/writer.png alt="">
                        <p class="text-uppercase">рекламные копирайтеры</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>




<?php include('tpl/footer.php'); ?>
