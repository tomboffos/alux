<?php include('tpl/header.php'); ?>

<section id="contacts">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="row justify-content-center ">
                    <div class="col-lg-4 col-12">
                        <div class="row">
                            <div class="col-lg-1 col-1 text-right">
                                <img src="img/c1.png" class="mt-2">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p>Республика Казахстан, г. Алматы,</p>
                                <p>Ул. Шевченко 165Б, офис.511</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="row">
                            <div class="col-lg-1 col-1 text-righ">
                                <img src="img/c2.png" class="mt-2">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p><a href="tel:+77273171698" target="_blank">+7 (727) 317-16-98</a></p>
                                <p><a href="tel:+77054503916" target="_blank">+7 (705) 450-39-16</a></p>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-1 col-1 text-righ">
                                <img src="img/c3.png">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p><a href="mailto:info@a-lux.kz">info@a-lux.kz</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="map" class="container-fluid">
    <div class="row">
        <div class="col-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d726.6011801227112!2d76.89139882923801!3d43.24293897563113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3883691455da550d%3A0x1959a4a17fe8981a!2z0YPQu9C40YbQsCDQqNC10LLRh9C10L3QutC-IDE2NWIsINCQ0LvQvNCw0YLRiyAwNTAwMjY!5e0!3m2!1sru!2skz!4v1593773285688!5m2!1sru!2skz" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
</section>

<?php include('tpl/footer.php'); ?>
