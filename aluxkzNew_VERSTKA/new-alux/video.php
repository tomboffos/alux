<?php include('tpl/header.php'); ?>

 <section id="about_us_page_sec_one">
    <div class="container">
            <h1 class="wow zoomInUp" data-wow-offset="200"><span>С</span>ОЗДАНИЕ <span>В</span>ИДЕО <span>И</span>НФОГРАФИКИ</h1>
            <div class="row">
                <div class="col-lg-6 gloves-and-video wow fadeInLeft" data-wow-offset="200">
                    <img src="/img/eggs-in-white-gloves.png" alt="" class="gloves">
                    <p class="p-to-go">Давайте мы за 60 секунд расскажем и <span class="blue-span">покажем</span>, зачем Вам нужна видеоинографика ?</p>
                    <p class="play-btn desktop-btn" data-toggle="modal" data-target="#exampleModal"><img src="/img/play.png" alt=""></p>
                    <a class="play-btn mobile-btn" href="https://www.youtube.com/watch?v=JlzDqZVLKgs" target="_blank"><img src="/img/play.png" alt=""></a>
                </div>
                <div class="col-lg-6 all-main-info wow fadeInRight" data-wow-offset="200">
                    <p class="concept-development">разработка концепции сценария, дизайн персонажей, профессиональная озвучка лучшими дикторами Казахстана и СНГ.</p>
                    <div class="main-text">
                        <p class="small-white-main-p">Увеличим количество заявок с сайта в 2 раза с помощью видео инфографики, рассказывающей о продукте за 60 секунд</p>
                        <ul class="main-ul">
                            <li><span>Готовый проект от 15 дней</span></li>
                            <li><span>Более 100 проектов в портфолио</span></li>
                            <li><span>Более 30 дикторов на выбор</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</section>




<?php include('tpl/footer.php'); ?>
