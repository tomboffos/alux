<?php include('tpl/header.php'); ?>

<section id="about_us_page_sec_one">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12 text-right">
                <img src="img/intro-heart.png" width="100%">
            </div>
            <div class="col-lg-6 col-md-6 col-12 mt-5">
                <div class="container border-l mt-5">
                    <h4 class="heading_review bold upper">Что говорят клиенты A-LUX</h4>
                    <p class="white">Мы создаем мощные инструменты, которые помогают вашему бизнесу занять лидирующие позиции.</p>

                </div>
                <img src="img/shining-black.png">
            </div>
        </div>
    </div>
</section>

<section id="review_sert">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h5 class="upper bold white">Что думают о нас наши клиенты</h5>
            </div>

            <div class="col-lg-12 mt-3 text-center">
                <div id="blogCarousel" class="carousel slide" data-ride="carousel">




                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <!--.item-->

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 mt-3">
                                    <a data-fancybox="gallery" href="https://sport-market.kz/images/review-1.jpg">
                                        <img src="https://sport-market.kz/images/review-1.jpg" alt="Image" style="max-width:100%;">
                                    </a>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <!--.item-->

                    </div>
                    <!--.carousel-inner-->
                </div>

                <!--.Carousel-->
                <!--.Carousel-->
                <div class="text-center mt-4">
                    <a class="mr-2" href="#blogCarousel" role="button" data-slide="prev">
                        <img src="img/arrow-left.png">

                    </a>
                    <a class="ml-2" href="#blogCarousel" role="button" data-slide="next">
                        <img src="img/arrow-right.png">
                    </a>
                </div>
            </div>
        </div>
    </div>


</section>




<?php include('tpl/footer.php'); ?>
