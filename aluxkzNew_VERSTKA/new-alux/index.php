<?php include('tpl/header.php'); ?>

<section style="height:100%; background:#000; padding-top:70px; padding-bottom:150px">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="height:550px">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="row">
                    <div class="col-lg-7">
                        <img src="img/slide_1.jpg" class="d-block slider_zoom" alt="...">
                    </div>
                    <div class="col-lg-4" style="padding-top:150px">
                        <h3 class="main_home_slider_item_title"><span class="uppercase"><span class="color_blue">Лидеры</span> Казахстанского рынка доверяют нам создание сайтов</span></h3>
                        <p class="main_home_slider_item_desc main-text-left">На протяжении 13 лет наша веб-студия успешно разрабатывает и продвигает уникальные веб сайты класса "LUX"</p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-lg-7">
                        <img src="img/slide_1.jpg" class="d-block slider_zoom" alt="...">
                    </div>
                    <div class="col-lg-4" style="padding-top:150px">
                        <h3 class="main_home_slider_item_title"><span class="uppercase"><span class="color_blue">Лидеры</span> Казахстанского рынка доверяют нам создание сайтов</span></h3>
                        <p class="main_home_slider_item_desc main-text-left">На протяжении 13 лет наша веб-студия успешно разрабатывает и продвигает уникальные веб сайты класса "LUX"</p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row" >
                    <div class="col-lg-7">
                       <img src="img/slide_1.jpg" class="d-block slider_zoom" alt="...">
                    </div>
                    <div class="col-lg-4" style="padding-top:150px">
                        <h3 class="main_home_slider_item_title"><span class="uppercase"><span class="color_blue">Лидеры</span> Казахстанского рынка доверяют нам создание сайтов</span></h3>
                        <p class="main_home_slider_item_desc main-text-left">На протяжении 13 лет наша веб-студия успешно разрабатывает и продвигает уникальные веб сайты класса "LUX"</p>
                    </div>
                </div>
            </div>
            
            <div class="carousel-item" style="background: url('/img/pacman_new.jpg'); background-size:cover; padding-top:150px">
                <div class="row" >
                    <div class="col-lg-12" align="center">
                        <h3 class="slider_pacman_text">Играй и получай <span class="t_blue">скидки!</span></h3>
                        <a class="btn btn-md btn_outline_white order-btn mt-3" href="#" tabindex="-1" aria-disabled="true">Начать играть!</a>
                       <img src="img/pacman_new2.jpg" class="d-block" alt="...">
                    </div>
                   
                </div>
            </div>
        </div>
        
    </div>
</section>





<section id="brands">
    <div class="container brands">
        <div class="row">
            <div class="col">
                <div class="brands_slider_container">
                    <div class="logo_slider"> <a class="slide" href="/portfolio.html">
                            <span class="view_all upper">Просмотреть всех</span></a>
                    </div>
                    <div class="owl-carousel owl-theme brands_slider text-center" align="center">
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/1.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/2.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/3.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/4.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/5.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/6.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/7.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/8.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/9.jpg" alt=""></div>
                        </div>
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center"><img src="img/logo/10.jpg" alt=""></div>
                        </div>
                    </div> <!-- Brands Slider Navigation -->

                    <div class="brands_nav brands_prev"><i class=""><img src="/img/arrow-left.png"></i></div>
                    <div class="brands_nav brands_next"><i class=""><img src="/img/arrow-right.png"></i></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about_us_index">
    <div class="container mr-auto ">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <div class="row justify-content-center ">
                    <div class="col-9">
                        <h1><span class="title_main_text">Создание сайтов в Алматы</span></h1>
                        <p class="about_main_text">Разработка сайтов премиального класса в Алматы и других городах Казахстана с 2007 года. Компания базируется на двух крепко взаимосвязанных равноценных компонентах: люди и технологии. Наши специалисты постоянно исследуют актуальные отраслевые тренды и развивают инструменты продаж и управление ими. Это главная причина успеха нашей веб-студии в своём регионе.</p>

                        <div class="row mt-3">
                            <div class="col-lg-2 col-12 text-center">
                                <a href="#" class="btn btn-lg btn_blue about_btn_pos">О нас</a>
                            </div>

                            <div class="col-lg-10 col-12">
                                <p align="right" class="about_text_quote">ВЕДЬ ГЛАВНОЕ ДЛЯ НАС – ДОСТИЖЕНИЕ ВАШИХ ЦЕЛЕЙ.</p>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</section>


<section class="testimonial text-center">
    <div class="container">
        <div class="row">

            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">

                            <p>С кем и как мы работаем? С Заказчиками, имеющими серьезные планы на свой бизнес и разделяющими точку зрения, что Сайт и Digital-маркетинг - это серьезные инструменты продаж. </p>
                            <div align="right"><span class="client_name white">Иван Востриков - Основатель компании</span></div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <p>С кем и как мы работаем? С Заказчиками, имеющими серьезные планы на свой бизнес и разделяющими точку зрения, что Сайт и Digital-маркетинг - это серьезные инструменты продаж. </p>
                            <div align="right"><span class="client_name white">Иван Востриков - Основатель компании</span></div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">

                            <p>С кем и как мы работаем? С Заказчиками, имеющими серьезные планы на свой бизнес и разделяющими точку зрения, что Сайт и Digital-маркетинг - это серьезные инструменты продаж. </p>
                            <div align="right"><span class="client_name white">Иван Востриков - Основатель компании</span></div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <img src="/img/arrow-left.png">
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <img src="/img/arrow-right.png">
                </a>
            </div>
        </div>
    </div>
</section>


<section style="height: 100%;">
    <div class="benefits-main-block parallax-window" data-parallax="scroll" data-image-src="img/benefits.jpg">
        <div class="features-slider-big">
            <div class="container">
                <div class="row">
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 col-12 benefits-item-tracker"><span>1</span></div>
                        <div class="col-md-10 col-12 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img src="/img/b1.png" alt="">
                            </div>
                            <div class="benefits-item-text">
                                <span>
                                    14 ЛЕТ ОПЫТА В ОБЛАСТИ СОЗДАНИЯ ВЕБ-САЙТОВ И МОБИЛЬНЫХ РАЗРАБОТОК - БОЛЕЕ 400 УСПЕШНО РЕАЛИЗОВАННЫХ ПРОЕКТОВ
                                </span>
                                <p>
                                    С 2007 года наша компания занимается разработкой сайтов в Казахстане,за это время успешно реализовали более 400 различных проектов. Многие ошибки, которые делают начинающие студии при создании веб-сайтов, мы уже сделали, проанализировали и зафиксировали в своих инструкциях и стандартах качества.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 col-12 benefits-item-tracker"><span>2</span></div>
                        <div class="col-md-10 col-12 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img src="/img/b2.png" alt="">
                            </div>
                            <div class="benefits-item-text">
                                <span>
                                    ТОЛЬКО ОРИГИНАЛЬНЫЕ РЕШЕНИЯ И ПЕРЕДОВЫЕ ТЕХНОЛОГИИ

                                </span>
                                <p>
                                    Мы нацелены на разработку уникальных и эффективных проектов. Разрабатываем только эксклюзивный современный дизайн и всегда работаем до полного утверждения заказчиком. Мы постоянно развиваемся и используем самые новые и лучшие технологии.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 col-12 benefits-item-tracker"><span>3</span></div>
                        <div class="col-md-10 col-12 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img src="/img/b3.png" alt="">
                            </div>
                            <div class="benefits-item-text">
                                <span>
                                    СТРОГОЕ СОБЛЮДЕНИЕ СРОКОВ ПРИ РАЗРАБОТКЕ САЙТА

                                </span>
                                <p>Даже если это экстремальные сроки, если мы взялись за заказ, значит мы выполним его вовремя! Потому что мы трезво оцениваем наши возможности и не обещаем того, чего не можем сделать. Разработка сайтов в Казахстане является нашим ключевым направлением.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 col-12 benefits-item-tracker"><span>4</span></div>
                        <div class="col-md-10 col-12 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img src="/img/b4.png" alt="">
                            </div>
                            <div class="benefits-item-text">
                                <span style="font-weight: 500;font-size: 14px;text-transform: uppercase;">
                                    БЕСПЛАТНОЕ ГАРАНТИЙНОЕ И ПОСЛЕГАРАНТИЙНОЕ ОБСЛУЖИВАНИЕ:
                                </span>
                                <p>
                                    На все услуги и продукты предоставляется бесплатное гарантийное сопровождение и устранение неполадок сроком на 6 месяцев. Наша техническая поддержка обеспечит бесперебойную работу вашего проекта, приедет и окажет первую техническую помощь в случае нестандартных ситуаций.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 col-12 benefits-item-tracker"><span>5</span></div>
                        <div class="col-md-10 col-12 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img src="/img/b5.png" alt="">
                            </div>
                            <div class="benefits-item-text">
                                <span>
                                    ВЕБ-СТУДИЯ АЛМАТЫ LUX С ГИБКОЙ ЦЕНОВОЙ ПОЛИТИКОЙ

                                </span>
                                <p>
                                    Веб-студия Алматы LUX финансово устойчивая компания, поэтому мы предлагаем нашим клиентам привлекательные условия поэтапной оплаты наших услуг. Мы готовы обсуждать стоимость работ, принимая во внимание все аспекты сотрудничества.


                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item benefits-item-end scrollin">
                        <div class="col-md-2 benefits-item-tracker-end"><span></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid services-main">
        <div class="row">
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s1.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-1">Создание <br> сайтов любой <br> сложности </h4>
                        <div class="services-item-desc">Мы создали и постоянно дорабатываем собственный движок позволяющий реализовывать проекты любой сложности в сжатые сроки.</div><a class="services-item-btn" href="/sozdaniesaitovvkazahstane.html">Создание сайтов</a>
                    </figcaption>
                </figure>
            </div>
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s2.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-2">Разработка Мобильных <br> Приложений</h4>
                        <div class="services-item-desc">За 3 года работы отдела мобильных разработок мы успели создать более 30 мобильных приложений под Android и IOS.
                        </div><a class="services-item-btn" href="/razrabotkamobilnihprilozheniy.html">Разработка приложений</a>
                    </figcaption>
                </figure>
            </div>
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s3.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-1">Брендинг</h4>
                        <div class="services-item-desc">Уникальность, Креатив, Стиль. Это основные качества, которыми мы руководствуемся при создании Вашей айдентики.</div><a class="services-item-btn" href="/razrabotkabrandbook.html">Брендинг</a>
                    </figcaption>
                </figure>
            </div>
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s4.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-1">Digital <br> Marketing</h4>
                        <div class="services-item-desc">SEO, контекстная реклама в поисковых системах, SMM-продвижение и комплексное продвижение бренда в сети интернет.<a class="services-item-btn" href="#">Интернет-маркетинг</a></div>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
</section>



<section id="news_blog_slider">

    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-12 col" align="center">
                <h4 class="upper"><span class="news">Статьи</span></h4>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="news_blog">
                    <div class="col-lg-3 col-md-4 col-12">
                        <a href="#">
                            <h4 class="heading_news_slider">Реалии европейского рынка труда 2020 - 2021</h4>
                        </a>
                        <p class="date_news">8.9.2020 г.</p>
                        <p class="text_news">Жизнь меняется быстрее, чем мы могли себе представить.&nbsp;Кто-то может подумать, что бизнес может быть осуществлен дистанционно?&nbsp;Это изменение, которо...</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-12">
                        <a href="#">
                            <h4 class="heading_news_slider">Реалии европейского рынка труда 2020 - 2021</h4>
                        </a>
                        <p class="date_news">8.9.2020 г.</p>
                        <p class="text_news">Жизнь меняется быстрее, чем мы могли себе представить.&nbsp;Кто-то может подумать, что бизнес может быть осуществлен дистанционно?&nbsp;Это изменение, которо...</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-12">
                        <a href="#">
                            <h4 class="heading_news_slider">Реалии европейского рынка труда 2020 - 2021</h4>
                        </a>
                        <p class="date_news">8.9.2020 г.</p>
                        <p class="text_news">Жизнь меняется быстрее, чем мы могли себе представить.&nbsp;Кто-то может подумать, что бизнес может быть осуществлен дистанционно?&nbsp;Это изменение, которо...</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-12">
                        <a href="#">
                            <h4 class="heading_news_slider">Реалии европейского рынка труда 2020 - 2021</h4>
                        </a>
                        <p class="date_news">8.9.2020 г.</p>
                        <p class="text_news">Жизнь меняется быстрее, чем мы могли себе представить.&nbsp;Кто-то может подумать, что бизнес может быть осуществлен дистанционно?&nbsp;Это изменение, которо...</p>

                    </div>
                    <div class="col-lg-3 col-md-4 col-12">
                        <a href="#">
                            <h4 class="heading_news_slider">Реалии европейского рынка труда 2020 - 2021</h4>
                        </a>
                        <p class="date_news">8.9.2020 г.</p>
                        <p class="text_news">Жизнь меняется быстрее, чем мы могли себе представить.&nbsp;Кто-то может подумать, что бизнес может быть осуществлен дистанционно?&nbsp;Это изменение, которо...</p>
                    </div>
                </div>



                <div class="col-12 mt-5" align="center">
                    <a href="#" class="btn btn-lg btn_blue about_btn_pos">Все статьи</a>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="contacts_index">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="row justify-content-center ">
                    <div class="col-lg-4 col-12">
                        <div class="row">
                            <div class="col-lg-1 col-1 text-right">
                                <img src="img/c1.png" class="mt-2">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p>Республика Казахстан, г. Алматы,</p>
                                <p>Ул. Шевченко 165Б, офис.511</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="row">
                            <div class="col-lg-1 col-1 text-righ">
                                <img src="img/c2.png" class="mt-2">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p><a href="tel:+77273171698" target="_blank">+7 (727) 317-16-98</a></p>
                                <p><a href="tel:+77054503916" target="_blank">+7 (705) 450-39-16</a></p>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-1 col-1 text-righ">
                                <img src="img/c3.png">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p><a href="mailto:info@a-lux.kz">info@a-lux.kz</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="webstudio_block">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12 block-text-blue">

                <div class="row">
                    <div class="col-lg-8 col-12">

                        <h3 class="bold">Разработка сайтов в Алматы</h3>
                        <p style="text-align: justify; ">При проектировании и развитии наших ключевых сервисов мы старались не брать за основу распространенные решения от коллег, а с нуля проработали базовые принципы взаимодействия как с клиентом, так и со средствами разработки . Выделим ключевые из них:
                        </p>
                        <ul>
                            <li>Персональный подход и погружение в каждый проект позволяет закрыть конкретные потребности и создать уникальное решение;</li>
                            <li>Прямая связь со специалистом обеспечивает качественный контакт, который в других компаниях теряет в качестве из-за включения в цепочку посредника (например, оператора на телефоне);</li>
                            <li>В технологическом плане мы обеспечиваем максимальную простоту в управлении, скорость и надежность работы веб-сайта, безопасность и жизнеспособность;</li>
                            <li>В современных условиях для того, чтобы быть победителем, необходимо уже на стадии проектирования прототипов закладывать продающие возможности для достижения максимальной конверсии, то есть коммерческой эффективности продукта.</li>
                        </ul>

                        <p>Ко всему этому стоит добавить стремление компании «А-Люкс» избавляться от второстепенных расходов на сотрудников, задачи которых можно делегировать без потери качества: секретари, тестировщики, call-центр. Таким образом, заказчик получает превосходный код, упакованный в премиум-дизайн за достойные для рынка деньги. Мы занимаемся созданием сайтов в Алматы уже на протяжении 14 лет.</p>
                        <br>
                    </div>
                    <div class="col-lg-4 col-12">

                        <h2 class="bold white">Веб-студия №1</h2>
                        <ul>
                            <li> Перед тем, как обратиться к специалисту, определите цели и задачи проекта - лучше Вас о них не знает никто.

                            </li>
                            <li> Начальные данные можно оформить в список или в виде брифа (опросника) и отправить нам на электронную почту.
                            </li>
                            <li>
                                Обсуждение проекта происходит по телефону или при личной встрече с исполнителями.

                            </li>
                            <li> Далее создание сайта подразумевает постоянный контакт и участие в работе, что позволяет добиться максимального соответствия поставленным задачам и качества готового продукта. Наша веб-студия работает именно по данной схеме.

                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

<section id="map" class="container-fluid">
    <div class="row">
        <div class="col-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d726.6011801227112!2d76.89139882923801!3d43.24293897563113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3883691455da550d%3A0x1959a4a17fe8981a!2z0YPQu9C40YbQsCDQqNC10LLRh9C10L3QutC-IDE2NWIsINCQ0LvQvNCw0YLRiyAwNTAwMjY!5e0!3m2!1sru!2skz!4v1593773285688!5m2!1sru!2skz" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
</section>



<?php include('tpl/footer.php'); ?>
