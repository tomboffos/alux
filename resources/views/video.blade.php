@extends('layouts.main')
@section('content')


<div class="main-block block">
    <div class="container">
        <h1 class="wow zoomInUp" data-wow-offset="200"><span>С</span>ОЗДАНИЕ <span>В</span>ИДЕО <span>И</span>НФОГРАФИКИ</h1>
        <div class="row">
            <div class="col-lg-6 gloves-and-video wow fadeInLeft" data-wow-offset="200">
                <img src="/img/eggs-in-white-gloves.png" alt="" class="gloves">
                <p class="p-to-go">Давайте мы за 60 секунд расскажем и <span class="blue-span">покажем</span>, зачем Вам нужна видеоинографика ?</p>
                <p class="play-btn desktop-btn" data-toggle="modal" data-target="#exampleModal"><img src="/img/play.png" alt=""></p>
                <a class="play-btn mobile-btn" href="https://www.youtube.com/watch?v=JlzDqZVLKgs" target="_blank"><img src="/img/play.png" alt=""></a>
            </div>
            <div class="col-lg-6 all-main-info wow fadeInRight" data-wow-offset="200">
                <p class="concept-development">разработка концепции сценария, дизайн персонажей, профессиональная озвучка лучшими дикторами Казахстана и СНГ.</p>
                <div class="main-text">
                    <p class="small-white-main-p">Увеличим количество заявок с сайта в 2 раза с помощью видео инфографики, рассказывающей о продукте за 60 секунд</p>
                    <ul class="main-ul">
                        <li>Готовый проект от 15 дней</li>
                        <li>Более 100 проектов в портфолио</li>
                        <li>Более 30 дикторов на выбор</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="sec-block block">
    <div class="container">
        <h2>Получите идею сценария <br><span>продающей видео инфографики</span></h2>
        <form id="form2">
            <input type="hidden" name="event" value="order_task" />
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="form-group">
                        
                        <input name="name" type="text" class="form-control inp_bor" data-wow-offset="200" placeholder="Представьтесь, пожалуйста">
                    </div>
                    <div class="form-group">
                     
                        <input name="contacts" type="text" class="form-control inp_bor wow fadeInUp phoneMask" data-wow-offset="200" placeholder="Укажите Ваш телефон">
                    </div>
                     <div class="form-group">
                       
                        <textarea name="task" class="form-control inp_bor" placeholder="Опишите задачу" rows="5"></textarea>
                    </div>
                </div>
             
            </div>
            <div class="col-lg-12 wow fadeInUp offset-mg-medium-textarea" data-wow-offset="200">
               
            </div>
            <button class="blue-btn btn-hollow order-btn2" data-form="#form2">Получить идею сценария</button>
            <div class="consult"><img src="/img/consultation.png" alt="">Менеджер перезвонит вам в течение 5 минут</div>
        </form>
    </div>
</div>


<section id="video_for_bis">
    <div class="container">
        <h2 class="wow zoomInUp blue mb-5" data-wow-offset="200">ВИДЕО ИНФОГРАФИКА ДЛЯ БИЗНЕСА</h2>
        <div class="row">
            <div class="col-lg-8 col-md-7 col-xs-12 wow fadeInLeft" data-wow-offset="200">
                <h4>Что такое видео инфографика?</h4>

                <p class="video_for_bis_text mb-5"><span>Видеоинфографика - это один из самых современных и эффективных методов привлечения клиентов.</span> наличие ярких образов и персонажей повышает доверие аудитории, а прекрасная фоновая музыка в комплексе с четким дикторским слогом помогает более точно воспринять всю суть видеоролика.</p>
                

                <p class="video_for_bis_text mb-5"><span>Видеоинфографика представляет собой эффективный маркетинговый продукт, </span> который может выступать как в роли дополнения, так и в роли самостоятельного основного вида рекламы.Над созданием видеоинфографики в Алматы работает группа профессионалов из различных областей деятельности: маркетологи, сценаристы, копирайтеры, дизайнеры и художники, главной целью которых является создание не только красивого, но и продающего видео.</p>

                <p class="video_for_bis_text mb-5"><span>Видеоинфографика - это самый оптимальный и быстродействующий компонент маркетинговой стратегии, </span>который позволяет за 60 секунд не только рассказать, но и наглядно продемонстрировать всю суть освещаемой в видеоролике темы.</p>

                <p class="video_for_bis_text"><span>Видео инфографика бизнеса – прекрасный инструмент демонстрации потенциала,</span> например, не так давно перед нами стояла задача осветить строительство нового объекта, но показывать строительную площадку, да еще и зимой – не очень. И здесь на помощь пришла видеоинфографика с помощью которой мы легко воспроизвели будущий объект.
                    Безграничные средства инфографики помогут решить любую поставленную бизнес задачу, но стоит тщательно продумать визуальные образы и смыслы до момента разработки видеоинфографики, от этого будет зависеть виральность продукта.</p>
            </div>
            <div class="col-lg-4 col-md-5 col-xs-12  wow fadeInRight" data-wow-offset="200">
                <img src="/img/lamp.png" class="lamp" alt="">
            </div>
        </div>
    </div>
</section>

<div class="fourth-block">
    <h2 class="wow zoomInUp" data-wow-offset="200">Компании, которые работают с нами</h2>
    <div class="container">
        <div class="logos-slider">
            <img src="/img/logos1.png" alt="">
            <img src="/img/logos2.png" alt="">

        </div>

    </div>
</div>

<section id="video_for_bis_2">
    <h2 class="wow zoomInUp  mb-5" data-wow-offset="200">Почему видео инфографика - <br>это эффективно?</h2>
    <div class="container">
        <div class="row" align="center">
            <div class="col-lg-4 one-feat col-md-6 col-sm-12">
                <img src="/img/1icon.png" class="wow rubberBand" data-wow-offset="200" alt="">
                <h6 class="feat-name bold upper mt-3">Интеллектуальная <br>реклама</h6>
                <p class="feat-txt">Анимационная видео инфографика — это интеллектуальный вид рекламы,который легок в понимании.</p>
                <img src="/img/after.png" alt="">
            </div>
            <div class="col-lg-4 one-feat col-md-6 col-sm-12">
                <img src="/img/2icon.png" class="wow rubberBand" data-wow-offset="200" alt="">
                <h6 class="feat-name bold upper mt-3">Хорошо <br>запоминается</h6>
                <p class="feat-txt">Когда человек смотрит видеоролик, то информация воспринимается им по нескольким каналам. </p>
                <img src="/img/after.png">
            </div>
            <div class="col-lg-4 one-feat col-md-6 col-sm-12">
                <img src="/img/3icon.png" class="wow rubberBand" data-wow-offset="200" alt="">
               <h6 class="feat-name bold upper mt-3">Вызывает положительные<br>эмоции</h6>
                <p class="feat-txt">Если история рассказана правильно и интересно, то она вызывает положительные эмоции.</p>
                <img src="/img/after.png">
            </div>
            <div class="col-lg-4 one-feat col-md-6 col-sm-12">
                <img src="/img/4icon.png" class="wow rubberBand" data-wow-offset="200" alt="">
                <h6 class="feat-name bold upper mt-3">Рассказывает просто о<br>сложном</h6>
                <p class="feat-txt">Довольно сложные данные и большой объем информации переводятся благодаря видео, в простую визуальную форму.</p>
                <img src="/img/after.png" alt="">
            </div>
            <div class="col-lg-4 one-feat col-md-6 col-sm-12">
                <img src="/img/5icon.png" class="wow rubberBand" data-wow-offset="200" alt="">
                <h6 class="feat-name bold upper mt-3">Имидж</h6>
                <p class="feat-txt">Наличие качественного видеоролика повышает Ваш статус в глазах клиента, а также влияет на лояльность потребителя.</p>
                <img src="/img/after.png">
            </div>
            <div class="col-lg-4 one-feat col-md-6 col-sm-12">
                <img src="/img/6icon.png" class="wow rubberBand" data-wow-offset="200" alt="">
                <h6 class="feat-name bold upper mt-3">Работает 24 ч. <br>в сутки</h6>
                <p class="feat-txt">Видео, созданное один раз долгое время и абсолютно самостоятельно будет приносить клиентов. </p>
                <img src="/img/after.png" alt="">
            </div>
        </div>
        <img src="/img/arrow.png" alt="" class="arrow">
        <p class="smal-last-p">Вот основные причины, по которым профессионально сделанное видео — эффективный инструмент в передаче нужной информации клиенту. Видео действительно хорошо помогает формировать лояльность к бренду и увеличивать продажи.</p>
    </div>

</section>

<section id="video_sec">
    <h2 class="wow zoomInUp mb-5" data-wow-offset="200">примеры видео инфографики, <br><span>разработанной нашей студией</span></h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInLeft" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/dpP5g3e2O3E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInUp" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/WALM5sFeFXA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInRight" data-wow-offset="200">

                <iframe width="100%" height="200" src="https://www.youtube.com/embed/uhEdEfz8IeA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInLeft" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/4p3QyLEEfh0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInDown" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/hhBLyPhvn7o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInRight" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/4DezPr7o2BY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInRight" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/guw42gn5EpU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInRight" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/8b_mWGVdezw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 wow fadeInRight" data-wow-offset="200">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/v2qUF818bRc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>



@endsection
