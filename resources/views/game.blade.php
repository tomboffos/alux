@extends('layouts.main')
@section('content')
<div id="pricing">
    <div class="container">

        <div class="row">
            <div class="col-sm-12 text-left wow bounceInUp center">
                <p>
                    Играй и получай скидку  до 15% на создание и поддержку веб сайта в Алматы и других регионах Казахстана !
                    В честь 13-летнего юбилея компания «A-LUX» предлагает всем клиентам (нынешним и будущим) нашей компании  принять участие  в прохождении легендарной игры 90-х "PACMAN",которую мы сами воссоздали и выиграть 10% или 15% скидку на разработку или поддержку веб сайта. Акция действует до 1 Апреля 2020 г.
                </p>
                <p>
                    Пройдите 2 или 3 уровня и получите разные кодовые слова,которые Вам нужно будет сообщить нашему менеджеру при заказе или продлении услуги.
                </p>
                <div class="content" id="game-content">
                    <!-- main game content -->
                    <div class="game wrapper">
                        <div class="score">Очков:</div>
                        <div class="level">Уровень:</div>
                        <div class="lives">Жизней: </div>
                        <div class="controlSound">
                            <img src="img/audio-icon-mute.png" id="mute">
                        </div>
                        <!-- canvas is splitted into 18x13 fields -->
                        <div id="canvas-container">
                            <div id="canvas-overlay-container">
                                <div id="canvas-overlay-content">
                                    <div id="title">Игра «Pacman»</div>
                                    <div><p id="text">Кликните для начала игры</p></div>
                                </div>
                            </div>
                            <canvas id="myCanvas" width="540" height="390">
                                <p>Canvas не поддерживается</p>
                            </canvas>
                        </div>
                        <div class="controls" id="game-buttons">
                            <div>
                                <span id="up" class="controlButton">&uarr;</span>
                            </div>
                            <div>
                                <span id="left" class="controlButton">&larr;</span>
                                <span id="down" class="controlButton">&darr;</span>
                                <span id="right" class="controlButton">&rarr;</span>
                            </div>

                        </div>
                        <!-- inGame Controls End -->
                        <!-- Game Menu -->
                        <div class="controls" id="menu-buttons">
                            <ul>
                                <li class="button" id="newGame">Новая игра</li>
                                <li class="button" id="instructions">Инструкция</li>
                            </ul>

                        </div>
                        <!-- Game Menu End -->
                    </div>
                    <div class="description nomobile">
												<span id="audio">
													<audio id="theme" preload="auto">
														<source src="wav/theme.wav" type="audio/wav">
														<source src="mp3/theme.mp3" type="audio/mpeg">
													</source></audio>
													<audio id="waka" preload="auto">
														<source src="wav/waka.wav" type="audio/wav">
														<source src="mp3/waka.mp3" type="audio/mpeg">
													</source></audio>
													<audio id="die" preload="auto">
														<sour   ce src="wav/die.wav" type="audio/wav">
														<source src="mp3/die.mp3" type="audio/mpeg">
													</source></audio>
													<audio id="powerpill" preload="auto">
														<source src="wav/powerpill.wav" type="audio/wav">
														<source src="mp3/powerpill.mp3" type="audio/mpeg">
													</source></audio>
												</span>
                    </div>
                </div>
                <link rel="stylesheet" href="pacman-canvas.css?ver=1.0">
                <script src="js/jquery-1.10.2.min.js"></script>
                <script src="js/jquery.hammer.min.js"></script>
                <script src="pacman-canvas.js"></script>
            </div>

        </div>
    </div>
</div>


<style>
    #pricing{
        margin-top: 150px;
    }
</style>

@endsection
