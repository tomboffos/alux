@extends('layouts.main')
@section('content')
    <div class="row header-info" style="background-image: url('{{asset('images/img-00.jpg')}}')">
        <div class="container text-right" style="text-align:right;margin-top: 150px;margin-bottom: 150px;">
            <h1 class="wow fadeIn">{{$post['title']}}</h1>
        </div>
    </div>
<div id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-left wow bounceInUp center">
                {!! $post['text'] !!}
            </div>
        </div>
    </div>
</div>

<style>


</style>
@endsection
