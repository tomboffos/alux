@extends('layouts.main')
@section('content')


<section id="one_sec">
    <div class="container-fluid">
        <div id="carouselExampleIndicators" class="carousel slide slider_s" data-ride="carousel">
            <ol class="carousel-indicators " id="pagination_pos">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-lg-7">
                            <img src="img/slide_1.jpg" class="d-block slider_zoom" alt="...">
                        </div>
                        <div class="col-lg-4 slider_pt">
                            <h3 class="main_home_slider_item_title"><span class="uppercase"><span class="color_blue">@foreach($settings as $setting)
                                        @if($setting['setting_part'] == 'slider_title_blue_1')
                                        {{$setting['content']}}
                                        @endif
                                        @endforeach</span> @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'slider_title_1')
                                    {{$setting['content']}}
                                    @endif
                                    @endforeach </span></h3>
                            <p class="main_home_slider_item_desc main-text-left"> @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'slider_text_1')
                                {{$setting['content']}}
                                @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-lg-7">
                            <img src="img/slide_2.png" class="d-block slider_zoom" alt="...">
                        </div>
                        <div class="col-lg-4 slider_pt">
                            <h3 class="main_home_slider_item_title"><span class="uppercase"><span class="color_blue">@foreach($settings as $setting)
                                        @if($setting['setting_part'] == 'slider_title_blue_2')
                                        {{$setting['content']}}
                                        @endif
                                        @endforeach</span> @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'slider_title_2')
                                    {{$setting['content']}}
                                    @endif
                                    @endforeach </span></h3>
                            <p class="main_home_slider_item_desc main-text-left"> @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'slider_text_2')
                                {{$setting['content']}}
                                @endif
                                @endforeach
                            </p>
                            <div class="main_home_slider_item_btn">
                                <a href="https://www.a-lux.kz/internetmagazin.html" class="btn-hollow main_home_slider_item_btn_link">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item slider_pt" style="background: url('/img/pacman_new.jpg'); background-size:cover;">
                    <div class="row">
                        <div class="col-lg-12" align="center">
                            <h3 class="slider_pacman_text">Играй и получай <span class="t_blue">скидки!</span></h3>
                            <a class="btn btn-md btn_outline_white order-btn mt-3" href="#" tabindex="-1" aria-disabled="true">Начать играть!</a>
                            <img src="img/pacman_new2.jpg" class="d-block" alt="...">
                        </div>

                    </div>
                </div>

                <div class="carousel-item">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-4 slider_pt">
                            <h3 class="main_home_slider_item_title"><span class="uppercase">

                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'slider_title_4')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </span></h3>
                            <p class="main_home_slider_item_desc main-text-left"> @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'slider_text_4')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach</p>
                            <div class="main_home_slider_item_btn">
                                <a href="https://sport-market.kz/landingpage.html" class="btn-hollow main_home_slider_item_btn_link">Подробнее</a>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <img src="img/slide_4.png" class="d-block slider_zoom" alt="...">
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<section id="brands">
    <div class="container brands">
        <div class="row">
            <div class="col">
                <div class="brands_slider_container">
                    <div class="logo_slider"> <a class="slide" href="/portfolio.html">
                            <span class="view_all upper">Просмотреть всех</span></a>
                    </div>
                    <div class="owl-carousel owl-theme brands_slider text-center" align="center">

                        @foreach($logotypes as $logotype)
                        <div class="owl-item">
                            <div class="brands_item d-flex flex-column justify-content-center">
                                <img src="{{asset($logotype['image_path'])}}" alt="">
                            </div>
                        </div>
                        @endforeach

                    </div> <!-- Brands Slider Navigation -->

                    <div class="brands_nav brands_prev"><i class=""><img src="/img/arrow-left.png"></i></div>
                    <div class="brands_nav brands_next"><i class=""><img src="/img/arrow-right.png"></i></div>
                </div>
            </div>
        </div>
    </div>
</section>




<section id="about_us_index">
    <div class="container mr-auto ">
        <div class="row align-items-center">

            <div class="col-lg-12">
                <div class="row justify-content-center ">
                    <div class="col-9">
                        <h1><span class="title_main_text">@foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_title')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach</span></h1>
                        <p class="about_main_text">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'about_description')
                            {!! $setting['content'] !!}
                            @endif
                            @endforeach</p>

                        <div class="row mt-3">
                            <div class="col-lg-2 col-12 text-center">
                                <a href="#" class="btn btn-lg btn_blue about_btn_pos">О нас</a>
                            </div>

                            <div class="col-lg-10 col-12">
                                <p align="right" class="about_text_quote"> @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_citate')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach</p>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<section class="testimonial text-center">
    <div class="container">
        <div class="row">


            <!--
            
            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <div class="carousel-inner" role="listbox">
                    @foreach($reviews as $review)
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">

                            <p>{!! $review['review'] !!}</p>
                            <div align="right"><span class="client_name white">{{$review['author']}}</span></div>
                        </div>
                    </div>
                    
                    <div class="carousel-item">
                        <div class="testimonial4_slide">

                            <p>{!! $review['review'] !!}</p>
                            <div align="right"><span class="client_name white">{{$review['author']}}</span></div>
                        </div>
                    </div>
                    
                     <div class="carousel-item">
                        <div class="testimonial4_slide">

                            <p>{!! $review['review'] !!}</p>
                            <div align="right"><span class="client_name white">{{$review['author']}}</span></div>
                        </div>
                    </div>
                    @endforeach
                 
                    
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <img src="/img/arrow-left.png">
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <img src="/img/arrow-right.png">
                </a>
                
            </div>
-->

            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">

                            <p>С кем и как мы работаем? С Заказчиками, имеющими серьезные планы на свой бизнес и разделяющими точку зрения, что Сайт и Digital-маркетинг - это серьезные инструменты продаж. </p>
                            <div align="right"><span class="client_name white">Иван Востриков - Основатель компании</span></div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <p>Мы иначе смотрим на дизайн. Дизайн - это не то, как продукт выглядит и воспринимается. Дизайн  - это то, как он работает. </p>
                            <div align="right"><span class="client_name white">Елена Груздева - Дизайнер веб-студии</span></div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">

                            <p>Быстро, дешево и хорошо – из этих трех вещей нужно всегда выбирать две. Если быстро и дешево, это никогда не будет хорошо. Если это дешево и хорошо, никогда не получится быстро. А если это хорошо и быстро, никогда не выйдет дешево. Но помни: из трех все равно придется всегда выбирать две.
                                 </p>
                            <div align="right"><span class="client_name white">Джим Джармуш, кинорежиссёр, сценарист</span></div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <img src="/img/arrow-left.png">
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <img src="/img/arrow-right.png">
                </a>
            </div>
        </div>
    </div>
</section>





<div class="benefits-main-block parallax-window" data-parallax="scroll" data-image-src="img/benefits.jpg">
    <div class="features-slider-big">
        <div class="container">
            <div class="row">
                <div class="benefits-item scrollin">
                    <div class="col-md-2 benefits-item-tracker"><span>1</span></div>
                    <div class="col-md-10 benefits-item-content">
                        <div class="benefits-item-arrow"></div>
                        <div class="benefits-item-icon">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'icon_1')
                            <img loading="lazy" src="{{asset($setting['content'])}}" alt="">
                            @endif
                            @endforeach
                        </div>
                        <div class="benefits-item-text">
                            <h3>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_title_1')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </h3>
                            <p>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_text_1')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="benefits-item scrollin">
                    <div class="col-md-2 benefits-item-tracker"><span>2</span></div>
                    <div class="col-md-10 benefits-item-content">
                        <div class="benefits-item-arrow"></div>
                        <div class="benefits-item-icon">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'icon_2')
                            <img loading="lazy" src="{{asset($setting['content'])}}" alt="">
                            @endif
                            @endforeach
                        </div>
                        <div class="benefits-item-text">
                            <span style="font-weight: 500;
                    font-size: 14px;
                    text-transform: uppercase;">
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_title_2')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </span>
                            <p>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_text_2')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="benefits-item scrollin">
                    <div class="col-md-2 benefits-item-tracker"><span>3</span></div>
                    <div class="col-md-10 benefits-item-content">
                        <div class="benefits-item-arrow"></div>
                        <div class="benefits-item-icon">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'icon_3')
                            <img loading="lazy" src="{{asset($setting['content'])}}" alt="">
                            @endif
                            @endforeach
                        </div>
                        <div class="benefits-item-text">
                            <h3>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_title_3')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </h3>
                            <p>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_text_3')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="benefits-item scrollin">
                    <div class="col-md-2 benefits-item-tracker"><span>4</span></div>
                    <div class="col-md-10 benefits-item-content">
                        <div class="benefits-item-arrow"></div>
                        <div class="benefits-item-icon">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'icon_4')
                            <img loading="lazy" src="{{asset($setting['content'])}}" alt="">
                            @endif
                            @endforeach
                        </div>
                        <div class="benefits-item-text">
                            <span style="font-weight: 500;
                    font-size: 14px;
                    text-transform: uppercase;">
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_title_4')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </span>
                            <p>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_text_4')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="benefits-item scrollin">
                    <div class="col-md-2 benefits-item-tracker"><span>5</span></div>
                    <div class="col-md-10 benefits-item-content">
                        <div class="benefits-item-arrow"></div>
                        <div class="benefits-item-icon">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'icon_5')
                            <img loading="lazy" src="{{asset($setting['content'])}}" alt="">
                            @endif
                            @endforeach
                        </div>
                        <div class="benefits-item-text">
                            <h3>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_title_5')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </h3>
                            <p>
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'about_text_5')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="benefits-item benefits-item-end scrollin">
                    <div class="col-md-2 benefits-item-tracker-end"><span></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="features-slider">
        <div class="container">
            <div class="row">
                <div class="features-slider-1">
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 benefits-item-tracker"><span>1</span></div>
                        <div class="col-md-10 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img loading="lazy" src="img/b1.png" alt="иконка компьютера"></div>
                            <div class="benefits-item-text">
                                <h3>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_title_1')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </h3>
                                <p>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_text_1')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 benefits-item-tracker"><span>2</span></div>
                        <div class="col-md-10 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img loading="lazy" src="img/b2.png" alt="иконка клавиатуры"></div>
                            <div class="benefits-item-text">
                                <h3>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_title_2')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </h3>
                                <p>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_text_2')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 benefits-item-tracker"><span>3</span></div>
                        <div class="col-md-10 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img loading="lazy" src="img/b3.png" alt="иконка часов"></div>
                            <div class="benefits-item-text">
                                <h3>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_title_3')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </h3>
                                <p>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_text_3')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 benefits-item-tracker"><span>4</span></div>
                        <div class="col-md-10 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img loading="lazy" src="img/b4.png" alt="иконка браузера"></div>
                            <div class="benefits-item-text">
                                <h3>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_title_4')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </h3>
                                <p>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_text_4')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="benefits-item scrollin">
                        <div class="col-md-2 benefits-item-tracker"><span>5</span></div>
                        <div class="col-md-10 benefits-item-content">
                            <div class="benefits-item-arrow"></div>
                            <div class="benefits-item-icon"><img loading="lazy" src="img/b5.png" alt="иконка кошелька"></div>
                            <div class="benefits-item-text">
                                <h3>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_title_5')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </h3>
                                <p>
                                    @foreach($settings as $setting)
                                    @if($setting['setting_part'] == 'about_text_5')
                                    {!! $setting['content'] !!}
                                    @endif
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container-fluid services-main">
        <div class="row">
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s1.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-1">Создание <br> сайтов любой <br> сложности </h4>
                        <div class="services-item-desc">
                            @foreach($settings as $setting)
                            @if($setting['setting_part'] == 'services_1')
                            {!! $setting['content'] !!}
                            @endif
                            @endforeach</div><a class="services-item-btn" href="/sozdaniesaitovvkazahstane.html">Создание сайтов</a>
                    </figcaption>
                </figure>
            </div>
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s2.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-2">Разработка Мобильных <br> Приложений</h4>
                        <div class="services-item-desc">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'services_2')
                            {!! $setting['content'] !!}
                            @endif
                            @endforeach
                        </div><a class="services-item-btn" href="/razrabotkamobilnihprilozheniy.html">Разработка приложений</a>
                    </figcaption>
                </figure>
            </div>
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s3.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-1">Брендинг</h4>
                        <div class="services-item-desc">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'services_3')
                            {!! $setting['content'] !!}
                            @endif
                            @endforeach</div><a class="services-item-btn" href="/razrabotkabrandbook.html">Брендинг</a>
                    </figcaption>
                </figure>
            </div>
            <div class="services-item col-lg-3 col-md-6 col-sm-3 col">
                <figure class="">
                    <div class="services-item-icon"><img src="img/s4.png"></div>
                    <figcaption>
                        <h4 class="services-item-title upper mt-1">Digital <br> Marketing</h4>
                        <div class="services-item-desc">@foreach($settings as $setting)
                            @if($setting['setting_part'] == 'services_4')
                            {!! $setting['content'] !!}
                            @endif
                            @endforeach<a class="services-item-btn" href="#">Интернет-маркетинг</a></div>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
</section>

<!--
<div class="news-main-block">
    <div class="container">
        <div class="news-main-title scrollin">
            <span style="">Статьи</span>
        </div>
        <div class="carousel news-carousel scrollin">
            <div class="carousel-arrows side">
                <div class="carousel-arrow left"><i></i></div>
                <div class="carousel-arrow right"><i></i></div>
            </div>
            <div class="carousel-inner">
                @foreach($posts as $post)
                <div class="carousel-item">
                    <div class="news-item">

                        <a href="{{route('PostPage',$post['id'])}}" title="{{$post['title']}}">
                            <strong style="display: block; font-size: 18px;">
                                {{$post['title']}}
                            </strong>
                        </a>
                        <time class="news-item-time">
                            {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.
                        </time>
                        <p>
                            @if(strlen($post['text']) > 157)
                            {!! mb_substr(strip_tags($post['text']),0,157).'...' !!}
                            @else
                            {!! mb_substr(strip_tags($post['text']),0,157) !!}

                            @endif
                        </p>
                    </div>

                </div>
                @endforeach

            </div>
        </div>
        <div class="news-main-link"><a class="btn-primary" href="{{route('Posts')}}" title="статьи о создании и продвижении сайтов">Все статьи</a></div>
    </div>
</div>
-->








<!--
<div class="contacts-main-block block-text">
<div class="container" style="position:relative;">
<div class="row">
<div class="col-md-12 block-text-blue">
    <div class="col-md-8">
        @foreach($settings as $setting)
            @if($setting['setting_part'] == 'steps')
                {!! $setting['content'] !!}
            @endif
        @endforeach
    </div><div class="col-md-4">
    <h2>Веб-студия №1</h2>
    
    {{-- <ul> --}}
        <ul>
            <li> Перед тем, как обратиться к специалисту, определите цели и задачи проекта - лучше Вас о них не знает никто.

            </li>
            <li> Начальные данные можно оформить в список или в виде брифа (опросника) и отправить нам на электронную почту.
            </li>
            <li>
                Обсуждение проекта происходит по телефону или при личной встрече с исполнителями.

            </li>
            <li> Далее создание сайта подразумевает постоянный контакт и участие в работе, что позволяет добиться максимального соответствия поставленным задачам и качества готового продукта. Наша веб-студия работает именно по данной схеме.

            </li>
        </ul>
    {{-- <li>@foreach($settings as $setting)
            @if($setting['setting_part'] == 'step_1')
                {!! $setting['content'] !!}
            @endif
        @endforeach</li>
    <li>@foreach($settings as $setting)
            @if($setting['setting_part'] == 'step_2')
                {!! $setting['content'] !!}
            @endif
        @endforeach</li>
    <li>
        @foreach($settings as $setting)
            @if($setting['setting_part'] == 'step_3')
                {!! $setting['content'] !!}
            @endif
        @endforeach
    </li>
    <li>@foreach($settings as $setting)
            @if($setting['setting_part'] == 'step_4')
                {!! $setting['content'] !!}
            @endif
        @endforeach</li> --}}
    {{-- </ul> --}}
    </div>
</div>
</div>
</div>
</div>
-->

<section id="news_blog_slider">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-6">
                <img src="img/news.svg">
            </div>
            <div class="col-lg-6 col-6 mt-5 pt-3" align="right">
                <a href="{{route('Posts')}}" class="btn btn-lg btn_blue about_btn_pos news_btn_text">Посмотреть все</a>
            </div>
        </div>
        <div class="row mt-4">
            <div id="news_slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row mt-4">
                            <div class="col-lg-4 col-md-4 col-12 mt-4">
                                <img src="/img/news_1.jpg">
                                <p class="date_news mt-2"> {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.</p>
                                <a href="#">
                                    <h4 class="heading_news_slider mt-2">
                                        <a href="{{route('PostPage',$post['id'])}}" title="{{$post['title']}}">
                                            {{$post['title']}}

                                        </a>
                                    </h4>
                                </a>
                            </div>

                            <div class="col-lg-4 col-md-4 col-12 mt-4">
                                <img src="/img/news_2.jpg">
                                <p class="date_news mt-2"> {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.</p>
                                <a href="#">
                                    <h4 class="heading_news_slider mt-2">Удобный для пользователя интерфейс влияет на выдачу в топ в поисковой системе Google </h4>
                                </a>
                            </div>

                            <div class="col-lg-4 col-md-4 col-12 mt-4">
                                <img src="/img/news_3.jpg">
                                <p class="date_news mt-2"> {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.</p>
                                <a href="#">
                                    <h4 class="heading_news_slider mt-2">Удобный для пользователя интерфейс влияет на выдачу в топ в поисковой системе Google </h4>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row mt-4">
                            <div class="col-lg-4 col-md-4 col-12">
                                <img src="/img/news_1.jpg">
                                <p class="date_news mt-2"> {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.</p>
                                <a href="#">
                                    <h4 class="heading_news_slider mt-2">Реалии европейского рынка труда 2020 - 2021</h4>
                                </a>
                            </div>

                            <div class="col-lg-4 col-md-4 col-12">
                                <img src="/img/news_2.jpg">
                                <p class="date_news mt-2"> {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.</p>
                                <a href="#">
                                    <h4 class="heading_news_slider mt-2">Удобный для пользователя интерфейс влияет на выдачу в топ в поисковой системе Google </h4>
                                </a>
                            </div>

                            <div class="col-lg-4 col-md-4 col-12">
                                <img src="/img/news_3.jpg">
                                <p class="date_news mt-2"> {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.</p>
                                <a href="#">
                                    <h4 class="heading_news_slider mt-2">Удобный для пользователя интерфейс влияет на выдачу в топ в поисковой системе Google </h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev news_arr_left" href="#news_slider" role="button" data-slide="prev">
                    <img src="/img/news_arr_left.svg">
                </a>
                <a class="carousel-control-next news_arr_right" href="#news_slider" role="button" data-slide="next">
                    <img src="/img/news_arr_right.svg">
                </a>
            </div>
        </div>


    </div>
</section>


<section id="webstudio_block">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12 block-text-blue">

                <div class="row">
                    <div class="col-lg-8 col-12">

                        <h3 class="bold">Разработка сайтов в Алматы</h3>
                        <p style="text-align: justify; ">При проектировании и развитии наших ключевых сервисов мы старались не брать за основу распространенные решения от коллег, а с нуля проработали базовые принципы взаимодействия как с клиентом, так и со средствами разработки . Выделим ключевые из них:
                        </p>
                        <ul>
                            <li>Персональный подход и погружение в каждый проект позволяет закрыть конкретные потребности и создать уникальное решение;</li>
                            <li>Прямая связь со специалистом обеспечивает качественный контакт, который в других компаниях теряет в качестве из-за включения в цепочку посредника (например, оператора на телефоне);</li>
                            <li>В технологическом плане мы обеспечиваем максимальную простоту в управлении, скорость и надежность работы веб-сайта, безопасность и жизнеспособность;</li>
                            <li>В современных условиях для того, чтобы быть победителем, необходимо уже на стадии проектирования прототипов закладывать продающие возможности для достижения максимальной конверсии, то есть коммерческой эффективности продукта.</li>
                        </ul>

                        <p>Ко всему этому стоит добавить стремление компании «А-Люкс» избавляться от второстепенных расходов на сотрудников, задачи которых можно делегировать без потери качества: секретари, тестировщики, call-центр. Таким образом, заказчик получает превосходный код, упакованный в премиум-дизайн за достойные для рынка деньги. Мы занимаемся созданием сайтов в Алматы уже на протяжении 14 лет.</p>
                        <br>
                    </div>
                    <div class="col-lg-4 col-12">

                        <h2 class="bold white">Веб-студия №1</h2>
                        <ul>
                            <li> Перед тем, как обратиться к специалисту, определите цели и задачи проекта - лучше Вас о них не знает никто.

                            </li>
                            <li> Начальные данные можно оформить в список или в виде брифа (опросника) и отправить нам на электронную почту.
                            </li>
                            <li>
                                Обсуждение проекта происходит по телефону или при личной встрече с исполнителями.

                            </li>
                            <li> Далее создание сайта подразумевает постоянный контакт и участие в работе, что позволяет добиться максимального соответствия поставленным задачам и качества готового продукта. Наша веб-студия работает именно по данной схеме.

                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>


<section id="contacts_index">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="row justify-content-center ">
                    <div class="col-lg-4 col-12">
                        <div class="row">
                            <div class="col-lg-1 col-1 text-right">
                                <img src="img/c1.png" class="mt-2">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                @foreach($settings as $setting)
                                @if($setting['setting_part'] == 'address')
                                {!! $setting['content'] !!}
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="row">
                            <div class="col-lg-1 col-1 text-righ">
                                <img src="img/c2.png" class="mt-2">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p><a href="tel:+77273171698" target="_blank">
                                        @foreach($settings as $setting)
                                        @if($setting['setting_part'] == 'phone_1')
                                        {!! $setting['content'] !!}
                                        @endif
                                        @endforeach</a></p>
                                <p><a href="tel:+77054503916" target="_blank">
                                        @foreach($settings as $setting)
                                        @if($setting['setting_part'] == 'phone_2')
                                        {!! $setting['content'] !!}
                                        @endif
                                        @endforeach</a></p>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-1 col-1 text-righ">
                                <img src="img/c3.png">
                            </div>
                            <div class="col-lg-11 col-11 text-left">
                                <p><a href="mailto:info@a-lux.kz">
                                        @foreach($settings as $setting)
                                        @if($setting['setting_part'] == 'mail')
                                        {!! $setting['content'] !!}
                                        @endif
                                        @endforeach</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="map" class="container-fluid">
    <div class="row">
        <div class="col-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d726.6011801227112!2d76.89139882923801!3d43.24293897563113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3883691455da550d%3A0x1959a4a17fe8981a!2z0YPQu9C40YbQsCDQqNC10LLRh9C10L3QutC-IDE2NWIsINCQ0LvQvNCw0YLRiyAwNTAwMjY!5e0!3m2!1sru!2skz!4v1593773285688!5m2!1sru!2skz" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
</section>





@endsection
