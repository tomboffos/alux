@extends('layouts.admin')
@section('content')

    <div class="col-lg-12">
        <div class="card w-100 p-3 mb-5">
            <h2>
                Добавить логотип
            </h2>
            <form action="{{route('admin.AddLogotype')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="file" name="image_path" id="" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg">
                    <input type="submit" value="Добавить" class="btn btn-primary mt-2">
                </div>

            </form>
        </div>
    </div>
    @foreach($logotypes as $logotype)
    <div class="col-lg-4">

        <div class="card p-5 w-100">

            <img src="{{asset($logotype['image_path'])}}" alt="" class="w-100">
        </div>
        <button onclick="window.location.href='{{route('admin.DeleteLogotype',$logotype['id'])}}'"  class="btn btn-danger btn_main">Удалить</button>
    </div>



@endforeach
<div class="col-lg-12">
    {{$logotypes->links()}}
</div>
<style>
    .btn_main{
        position: absolute;
        top: 10px;
        right: 20px;
    }

</style>

@endsection
