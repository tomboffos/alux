@extends('layouts.admin')
@section('content')
    <style></style>

    <link rel="stylesheet" href="{{asset('portfolio/css/style.css')}}">




                <div class="content_works col-lg-12">
                    {{$projects->links()}}
                    <div class="row">



                    @foreach($projects as $project)
                            <div class="modal fade" id="modal_delete_{{$project['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Вы уверены что хотите удалить?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                            <button type="button" class="btn btn-danger" onclick="window.location.href='{{route('admin.DeleteProject',$project['id'])}}'">Удалить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="col-lg-6   pr-0" >
                            <button class="btn btn-primary btn_edit" onclick="window.location.href='{{route('admin.EditProject',$project['id'])}}'">Редактировать</button>
                            <button class="ml-auto btn btn-danger btn_delete" data-toggle="modal" data-target="#modal_delete_{{$project['id']}}">Удалить</button>
                            <div class="card">
                                <div class="logo_image_part "   style="background-image: url({{asset($project->background_path)}})">
                                    <div class="d-flex w-100 justify-content-between align-items-center" style="background: linear-gradient(90deg, #FFFFFF 0.88%, rgba(255, 255, 255, 0.47) 100%);">
                                        <img src="{{asset($project['logo_path'])}}" alt="" >
                                    </div>

                                </div>
                                <div class="go_to_page" onclick="window.location.href='{{route('Work', $project['link'])}}'">
                                    <img src="{{asset('images/arrow_right.svg')}}" alt="">
                                </div>

                                <div class="text_of_work d-flex justify-content-between">
                                    <div class="title">

                                        {{$project['title']}}
                                    </div>
                                    <div class="properties d-flex">
                                        @if($project['web'] == 1)
                                            <div class="property">
                                                web
                                            </div>
                                        @endif
                                        @if($project['mobile'] == 1)
                                            <div class="property">
                                                mobile
                                            </div>
                                        @endif
                                        @if($project['branding'] == 1 )
                                            <div class="property">
                                                branding
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>


                </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script>
        $(window).ready(function(){
            $('.go_to_page').fadeOut()
        })
        $(".col-lg-6").hover(function(){
            $('.go_to_page').eq($(this).index()).addClass('active')
        }, function(){
            $('.go_to_page.active').removeClass('active')
        });
    </script>
    <style>
        .logo_image_part img{
            width: 100px;
            height: auto;


        }
        .logo_image_part>div{
            align-items: center !important;
            height: 100%;
            padding-top: 40px;
        }
        .logo_image_part{
            background-size: cover;
            background-position: center;
        }
        .content_works .col-lg-6:hover{
            cursor: pointer;
        }
        .go_to_page.active{
            display: flex;
        }
        .go_to_page{
            transition: 0.5s ease;
        }
        .content_menu a.active{
            color: #000;
            padding-bottom: 7px;
            border-bottom: 2px #000 solid;
        }
        .btn_delete{
            position: absolute;
            z-index: 10;
            right: 10px;
            top: 10px;
        }
        .btn_edit{
            position: absolute;
            left: 20px;
            top: 10px;
            z-index: 10;
        }
    </style>
@endsection
