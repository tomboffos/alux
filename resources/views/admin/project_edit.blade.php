@extends('layouts.admin')
@section('content')

    <div class="col-lg-12">
        <div class="card d-flex justify-content-center align-items-center p-4">
            <form action="{{route('admin.Update')}}" class="w-100" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <input type="hidden" name="project_id" value="{{$project['id']}}">
                <div class="d-flex justify-content-center">
                    <h3>
                        {{$project['title']}}
                    </h3>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" value="{{$project['title']}}"  placeholder="Название проекта" name="title">

                </div>
                <div class="form-group">
                    <label for="">Укажите порядок</label>
                    <input type="number" name="order" value="{{$project['order']}}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="logo_file" class="">Загрузите логотип</label>
                    <br>

                    <input type="file" name="logo_path" id="logo_file" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                    @if($project['logo_path'] != null)
                        <a target="_blank" href="{{$project['logo_path']}}">
                            <div class="uploadImage">
                                <img src="{{$project['logo_path']}}" alt="" class="web_image">
                            </div>
                        </a>
                    @endif
                </div>
                <div class="form-group">
                    <label for="background_file">Выберите файл для заднего фона</label>
                    <br>
                    <input type="file" name="background_path" id="background_file" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                    @if($project['background_path'] != null)
                        <a target="_blank" href="{{$project['background_path']}}">
                            <div class="uploadImage">
                                <img src="{{$project['background_path']}}" class="web_image" alt="">
                            </div>
                        </a>
                    @endif
                </div>
                <div class="form-group ">
                    <label for="main_image">Загрузите основное фото </label>
                    <br>
                    <input type="file" name="main_image" id="main_image" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                    @if($project['main_image'] != null)
                        <a target="_blank" href="{{$project['main_image']}}">
                            <div class="uploadImage">
                                <img src="{{$project['main_image']}}" class="web_image" alt="">
                            </div>
                        </a>
                    @endif
                </div>
                <div class="form-group form-inline">
                    <label for="web">Веб</label>
                    <input type="checkbox" onchange="webImage()" name="web" id="web" @if($project['web'] == 1) checked @endif>
                    <label for="mobile">Мобильное приложение</label>
                    <input type="checkbox" onchange="mobileImage()" name="mobile" id="mobile" @if($project['mobile'] == 1) checked @endif>
                    <label for="branding">Продвижение</label>
                    <input type="checkbox" name="branding" id="branding" @if($project['branding'] == 1) checked @endif>
                </div>

                @if($project['web'] == 1)
                <div class="form-group" id="web_image_page">
                    <label for="web_image">Загрузите страницу сайта</label>
                    <br>
                    <input type="file" name="web_image[]" multiple id="web_image" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                    @if($project['web_image'] != null)
                        @foreach(json_decode($project['web_image']) as $key=>$web_image)
                        <a target="_blank" href="{{$web_image}}">
                            <div class="uploadImage">
                                <img src="{{$web_image}}" class="web_image" alt="">
                                <div class="delete">
                                    <a href="{{url('admin/delete/WebDelete/'.$project['id'].'/'.$key)}}">Удалить</a>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    @endif
                </div>
                @else
                    <div class="form-group d-none" id="web_image_page">
                        <label for="web_image">Загрузите страницу сайта</label>
                        <br>
                        <input type="file" name="web_image[]" multiple id="web_image" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                        @if($project['web_image'] != null)
                            <a target="_blank" href="{{json_decode($project['web_image'])[0]}}">Картинка</a>
                        @endif
                    </div>
                @endif

                <div class="form-group d-none" id="mobile_image_page">
                    <label for="mobile">Загрузите изображения приложения</label>
                    <br>
                    <input type="file" name="mobile_images[]" multiple id="mobile_image" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                </div>

                <div class="form-group">
                    <input type="text" name="link" id="" class="form-control" value="{{$project['link']}}" placeholder="Введите url страницу для портфолио">
                </div>
                <div class="form-group" id="web_page_link">
                    <input type="text" name="link_to_page" class="form-control" placeholder="Введите url страницу сайта" value="{{$project['link_to_page']}}">
                </div>
                <div class="form-group">
                    <label for="letter">Загрузите благодарственное письмо</label> <br>

                    <input type="file" id="letter" name="letter_path" class="form-control-file form-control p-1" accept="application/pdf,application/vnd.ms-excel multipart/form-data" >
                </div>
                <div class="form-group">
                    <label for="summernote">Напишите отзыв клиента</label> <br>

                    <textarea name="client_review" class="form-control" id="summernote" cols="30" rows="5"  placeholder="Напишите отзыв клиента">
                        {!!$project['client_review']!!}
                    </textarea>
                </div>
                <div class="form-group">
                    <input type="text" name="technologies" class="form-control" id="" placeholder="Напишите стек технологий" value="{{$project['technologies']}}">
                </div>
                <div class="form-group">

                    <label for="summernote1">Описание проекта</label> <br>

                    <textarea name="description" id="summernote1" class="form-control" cols="30" rows="5" placeholder="Описание работы" >
                        {!! $project['description']!!}
                    </textarea>
                </div>
                <div class="form-group d-flex justify-content-center">
                    <input type="submit" value="Сохранить" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <style>
        .web_image {
            width: 100px;
            height: 105px;
        }
        .uploadImage {
            margin-top: 5px;
            text-align: center;
            vertical-align: middle;
            display: inline-block;
            width: 115px;
            height: 130px;
            margin: 10px 5px 5px 5px;
            -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            text-align: center;
        }
        .delete a {
            font-size: 12px;
        }
    </style>


@endsection
