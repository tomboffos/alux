@extends('layouts.admin')
@section('content')

    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <div class="col-lg-12">
        <div class="card p-3">
            <form action="{{route('admin.UpdatePost')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="hidden" name="post_id" value="{{$post['id']}}">
                    <input type="text" class="form-control mb-5" name="title" placeholder="Название" value="{{$post['title']}}">
                    <textarea name="text" id="summernote" class="form-control mt-4" cols="30" rows="5">{!! $post['text'] !!}</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Добавить" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>

@endsection
