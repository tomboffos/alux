@extends('layouts.admin')
@section('content')
<div class="col-lg-12">

    <div class="card p-3">
        <h2>
            Добавить благодарственное письмо
        </h2>
        <form action="{{route('admin.AddCertificate')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="">Загрузите фото благодарственного письма</label>
                <input type="file" name="certificate" id="" class="form-control p-1">
            </div>
            <div class="form-group">
                <label for="">Загрузите фото логотипа</label>
                <input type="file" name="logo" id="" class="form-control p-1">
            </div>
            <div class="form-group">
                <input type="text" name="link" id="" class="form-control" placeholder="Введите ссылку на сайт">

            </div>
            <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Введите имя партнера">
            </div>
            <div class="form-group">
                <input type="submit" value="Добавить" class="btn btn-primary">
            </div>

        </form>
    </div>
</div>
<div class="col-lg-12">
    {{$certificates->links()}}
</div>
@foreach($certificates as $certificate)

    <div class="col-lg-4 mt-5">
        <div class="card p-3">
            <img src="{{asset($certificate['image_link'])}}" alt="" class="w-100">
            <div class="d-flex mt-5">
                <img src="{{asset($certificate['mini_logo'])}}" alt="" class="mini_logo">
                <a href="{{$certificate['link']}}">{{$certificate['title']}}</a>
            </div>
            <button class=" mt-5 btn btn-danger" onclick="window.location.href='{{route('admin.DeleteCertificate',$certificate['id'])}}'">Удалить</button>
        </div>

    </div>
@endforeach

<style>
    .mini_logo{
        width: 100px;
    }
</style>
@endsection
