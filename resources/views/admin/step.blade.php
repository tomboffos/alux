@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <h3 class="text-center">
                        {{$step->question}}

                    </h3>
                    <h5>
                        Добавить ответ
                    </h5>
                    <form action="{{route('admin.createAnswer')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" name="name" placeholder="Моб приложения">
                        </div>
                        <p><select class="admin__select" name="category">
                            <option selected disabled>Следующий вопрос</option>
                            @foreach (App\TestStep::get() as $answer)
                            <option value="{{ $answer->id }}">{{ $answer->question }}</option>
                            @endforeach
                            </select>
                            </p>
                            <label for="input-text">Текстовое поле</label>
                            <input type="checkbox" name="input-text" id="input-text">
                        <input type="hidden" name="test_step_id" value="{{$step->id}}">
                        <div class="form-group text-center">
                            <a href='{{route('admin.CustomAnswer',$step->id)}}'
                             class='btn btn-primary'>Кастомные ответы</a>
                            <input type="submit" class="btn btn-primary">
                        </div>
                    </form>
                </div>

            </div>
            @foreach($answers as $answer)
                <div class="col-lg-4">
                    <div class="card card_step">
                        <h5>
                            {{$answer->answer}}
                        </h5>
                        <h5>
                            {{$answer->counter}}
                        </h5>
                        <a href="{{route('admin.DeleteAnswer',$answer->id)}}" class="btn btn-danger">
                            Удалить
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <style>
        .card_step{
            display: flex;
            justify-content: space-between;
            flex-direction: row;
            margin-top: 10px;
            margin-bottom: 10px;
            padding-top: 15px;
            padding-bottom: 15px;
            padding-left: 10px;
            padding-right: 10px;
            vertical-align: middle;

        }
        .col-12 .card
        {
            padding:20px 15px;
        }
        .admin__select {
            outline: none;
            display: block;
            width: 100%;
            height: calc(1.5em + .75rem + 2px);
            padding: .375rem .75rem;
            font-size: 1rem;
            font-weight: 400;
            color: #6e707e;
            background-color: #fff;
            border: 1px solid #d1d3e2;
            border-radius: .35rem;
            margin-top: 10px;
            cursor: pointer;
        }
    </style>
@endsection
