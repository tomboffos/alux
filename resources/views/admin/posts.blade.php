@extends('layouts.admin')
@section('content')
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<div class="col-lg-12">
    <div class="card p-3">
        <form action="{{route('admin.CreatePost')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <input type="text" class="form-control mb-5" name="title" placeholder="Название">
                <textarea name="text" id="summernote" class="form-control mt-4" cols="30" rows="5">Пост</textarea>
            </div>
            <div class="form-group">
                <input type="submit" value="Добавить" class="btn btn-primary">
            </div>
        </form>
    </div>
</div>
    @foreach($posts as $post)
        <div class="col-lg-4 mt-5">
            <div class="card p-3">
                <h5>
                    <b>{{$post['title']}}</b>
                </h5>

                <p>
                    {!! $post['text'] !!}
                </p>
                <button class="btn btn-primary mt-2" onclick="window.location.href='{{route('admin.EditPost',$post['id'])}}'">Редактировать</button>
                <button onclick="window.location.href='{{route('admin.DeletePost',$post['id'])}}'" class="btn btn-danger mt-2">Удалить</button>
            </div>
        </div>
    @endforeach
    <div class="col-lg-12">
        {{$posts->links()}}
    </div>


    <script>
    </script>
@endsection
