@extends('layouts.admin')
@section('content')

    <div class="col-lg-12">
        <div class="card d-flex justify-content-center align-items-center p-4">
            <form action="{{route('admin.Upload')}}" class="w-100" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="d-flex justify-content-center">
                    <h3>
                        Добавить работу для страницы портфолио
                    </h3>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Название проекта" name="title" value="{{ old('title') ?? $project->title ?? '' }}">

                </div>
                <div class="form-group">
                    <label for="logo_file" class="">Загрузите логотип</label>
                    <br>

                    <input type="file" name="logo_path" id="logo_file" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                </div>
                <div class="form-group">
                    <label for="background_file">Выберите файл для заднего фона</label>
                    <br>
                    <input type="file" name="background_path" id="background_file" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                </div>
                <div class="form-group ">
                    <label for="main_image">Загрузите основное фото </label>
                    <br>
                    <input type="file" name="main_image" id="main_image" value="{{ old('main_image') ?? $project->main_image ?? '' }}" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                </div>
                <div class="form-group form-inline">
                    <label for="web">Веб</label>
                    <input type="checkbox" onchange="webImage()" name="web" id="web">
                    <label for="mobile">Мобильное приложение</label>
                    <input type="checkbox" onchange="mobileImage()" name="mobile" id="mobile">
                    <label for="branding">Продвижение</label>
                    <input type="checkbox" name="branding" id="branding">

                </div>
                <div class="form-group d-none" id="web_image_page">
                    <label for="web_image">Загрузите страницу сайта</label>
                    <br>
                    <input type="file" name="web_image[]" multiple id="web_image" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                </div>
                <div class="form-group d-none" id="mobile_image_page">
                    <label for="mobile">Загрузите изображения приложения</label>
                    <br>
                    <input type="file" name="mobile_images[]" multiple id="mobile_image" class="form-control-file form-control p-1" accept="image/x-png,image/gif,image/jpeg" >
                </div>
                <div class="form-group">
                    <input type="text" name="link" id="" class="form-control" value="{{ old('link') ?? $project->link ?? '' }}" placeholder="Введите url страницу для портфолио">
                </div>
                <div class="form-group d-none" id="web_page_link">
                    <input type="text" name="link_to_page" class="form-control" placeholder="Введите url страницу сайта">
                </div>
                <div class="form-group">
                    <label for="letter">Загрузите благодарственное письмо</label> <br>

                    <input type="file" id="letter" name="letter_path" class="form-control-file form-control p-1" accept="application/pdf,application/vnd.ms-excel" >
                </div>
                <div class="form-group">
                <label for="summernote">Напишите отзыв клиента</label> <br>

<<<<<<< HEAD
                    <textarea name="review"  class="form-control" id="summernote" cols="30" rows="5" placeholder="Напишите отзыв клиента"></textarea>
=======
                    <textarea name="review"  class="form-control" id="summernote" cols="30" rows="5" placeholder="Напишите отзыв клиента">{{ old('review') ?? $project->review ?? '' }}</textarea>
>>>>>>> d4d3a4a54e4df97afd186f6017d7b03e6c71e6e7
                </div>
                <div class="form-group">
                    <input type="text" name="technologies" class="form-control" id="" value="{{ old('technologies') ?? $project->technologies ?? '' }}" placeholder="Напишите стек технологий">
                </div>
                <div class="form-group">
                    <textarea name="description" id="summernote1" class="form-control" cols="30" rows="5" placeholder="Описание работы">{{ old('description') ?? $project->description ?? '' }}</textarea>
                </div>
                <div class="form-group d-flex justify-content-center">
                    <input type="submit" value="Создать" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote1').summernote();
        });
    </script>


@endsection
