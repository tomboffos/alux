@extends('layouts.admin')

@section('content')
<div class='container'>
    <div class='row'>
        <div class="col-12">
            <div class="card card-padding">
                <h3 class="text-center">
                    Кастомные ответы
                </h3>
    </div>
</div>
            @foreach($custom as $answer)
                <div class="col-lg-4">
                    <div class="card card_step">
                        <h5>
                            {{$answer->title}}
                        </h5>
                        <a href="{{route('admin.DeleteCustom',$answer->id)}}" class="btn btn-danger">
                            Удалить
                        </a>
                    </div>
                </div>
            @endforeach
<style>
    .card-padding {
        padding: 20px 15px;
    }
    .card_step {
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    margin-top: 10px;
    margin-bottom: 10px;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 10px;
    padding-right: 10px;
    vertical-align: middle;
    }
</style>
@endsection
