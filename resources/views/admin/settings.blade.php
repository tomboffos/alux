@extends('layouts.admin')
@section('content')


    <!-- include summernote css/js -->
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/froala-design-blocks/2.0.0/css/froala_blocks.min.css">


    <div class="col-lg-12">
        <div class="card p-3">
            @foreach($settings as $setting)

                <form action="{{route('admin.SettingEdit')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="{{$setting['id']}}">
                            {{$setting['description']}}
                        </label>
                        @if ($setting['setting_part']  == 'icon_1' || $setting['setting_part']  == 'icon_2' ||
                $setting['setting_part']  == 'icon_3' || $setting['setting_part']  == 'icon_4')
                            <input type="file" class="form-control form-control-file p-1">
                            <div class="bg-primary p-1 justify-content-center align-items-center" style="width: 10%;">
                                <img src="{{asset($setting['content'])}}" alt="" class="ml-auto mr-auto">
                            </div>
                        @elseif($setting['setting_part'] == 'steps')
                            <textarea name="content" id="summernote" class="form-control" cols="30" rows="10">
                                {!! $setting['content'] !!}
                            </textarea>
                        @else
                            <textarea name="content"  cols="30" class="form-control" rows="4">{!! $setting['content'] !!}</textarea>
                        @endif
                        <input type="hidden" name="id" value="{{$setting['id']}}">

                        <input type="submit" value="Сохранить" class="btn btn-primary mt-2">
                    </div>
                </form>
            @endforeach
            {{$settings->links()}}
        </div>
    </div>

@endsection
