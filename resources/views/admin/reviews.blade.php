@extends('layouts.admin')
@section('content')

<div class="col-lg-12 mb-4">
    <div class="card p-3">
        <form action="{{route('admin.CreateReview')}}" method="post">
            {{csrf_field()}}
            <input type="text" class="form-control" name="author" placeholder="Автор">
            <div class="form-group mt-2">
                <textarea  name="review" class="form-control" id="summernote" cols="30" rows="10">Отзыв</textarea>
            </div>
            <input type="submit" class="btn btn-primary">
        </form>
    </div>
</div>
    @foreach($reviews as $review)
        <div class="col-lg-4">
            <div class="card p-3">
                <h5>
                    <b>{{$review['author']}}</b>
                </h5>
                <p>
                    {!! $review['review'] !!}
                </p>
                <button class="btn btn-danger" onclick="window.location.href='{{route('admin.DeleteReview',$review['id'])}}'">Удалить</button>
            </div>

        </div>

    @endforeach

    <div class="col-lg-12">
        {{$reviews->links()}}
    </div>



@endsection
