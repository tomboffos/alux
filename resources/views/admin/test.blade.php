@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <h3 class="text-center">
                        {{$test->name}}

                    </h3>
                    <h5>
                        Добавить вопрос
                    </h5>
                    <form action="{{route('admin.createStep')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" name="name" placeholder="Например какие сайты вам нравятся?">
                        </div>
                        <input type="hidden" name="test_id" value="{{$test->id}}">
                        <div class="form-group text-center">
                            <input type="submit" class="btn btn-primary">
                        </div>
                    </form>
                </div>

            </div>
            @foreach($steps as $step)
                <div class="col-lg-4">
                    <div class="card card_step">
                        <h5>
                            {{$step->question}}
                        </h5>
                        <a href="{{route('admin.DeleteQuestion',$step->id)}}" class="btn btn-danger">
                            Удалить
                        </a>
                        <a href="{{route('admin.Step',$step->id)}}" class="btn btn-primary">
                            Перейти
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <style>
        .card_step{
            align-items: center;
            display: flex;
            justify-content: space-between;
            flex-direction: row;
            margin-top: 10px;
            margin-bottom: 10px;
            padding-top: 15px;
            padding-bottom: 15px;
            padding-left: 10px;
            padding-right: 10px;
            vertical-align: middle;

        }
        .col-12 .card
        {
            padding:20px 15px;
        }
        .admin__select {
            outline: none;
            display: block;
            width: 100%;
            height: calc(1.5em + .75rem + 2px);
            padding: .375rem .75rem;
            font-size: 1rem;
            font-weight: 400;
            color: #6e707e;
            background-color: #fff;
            border: 1px solid #d1d3e2;
            border-radius: .35rem;
            margin-top: 10px;
            cursor: pointer;
        }
    </style>
@endsection
