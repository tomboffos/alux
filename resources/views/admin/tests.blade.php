@extends('layouts.admin')
@section('content')
    <div class="col-12">
        <div class="card">
            <h5 class="text-center">
                Создать опрос
            </h5>
            <form action="{{route('admin.createTest')}}" method="post" class="">
                {{csrf_field()}}
                <div class="form-group">
                    <h6 class="text-center">Название вашего опроса</h6>
                    <input type="text" class="form-control" placeholder="Например ваши пожелания к сайтам" name="name">

                </div>

                <div class="form-group d-flex justify-content-center">
                    <input type="submit" value="Создать" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    @foreach($tests as $test)
        <div class="col-lg-4">
            <div class="card d-flex justify-content-between test_card">
                <h4>
                    {{$test->name}}
                </h4>
                <a href="{{route('admin.Test',$test->id)}}" class="btn btn-primary">Перейти</a>
            </div>
        </div>
    @endforeach


    <style>
        .test_card{
            margin-top: 20px;
            display: flex;
            flex-direction: row;
        }
        .card{
            padding: 15px;
        }
    </style>

@endsection
