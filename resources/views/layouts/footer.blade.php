<footer id="footer">
    <div class="container">
        <div class="row">

            <div class="col-lg-5 col-md-4 col" align="left">
                <img src="/img/logo_alux.svg" width="150px" class="footer_pb">
                <p class="white">COPYRIGHT © 2007-{{ date('Y') }} ALMATY<br>LUXURY WEB DESIGN STUDIO </p>

                <div class="footer_pt">
                    <a href="#" target="_blank"><img src="img/fb.svg"></a>
                    <a href="https://www.instagram.com/aluxkz/" class="ml-4 mr-4" target="_blank"><img src="img/inst.svg"></a>
                    <a href="#" target="_blank"><img src="img/yt.svg"></a>
                </div>
            </div>

            <div class="col-lg-7 col-md-8 col" align="center">

               
                <nav id="header_menu" class="navbar navbar-expand-md d-none d-lg-block">
                    <div class="container">
                        <div>
                            <h5 class="white upper footer_pb20">Навигация</h5>
                        </div>
                    </div>
                    <div class="container footer_pb50">

                        <div class=" navbar-collapse" id="navbarsExampleDefault">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav_color nav_size" href="/">Главная <span class="sr-only">(current)</span></a>
                                </li>

                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="about_us.php" tabindex="-1" aria-disabled="true">О нас</a>
                                </li>


                                <li class="nav-item dropdown nav_mar">
                                    <a class="nav_color nav_size" href="services.php" target="_blank" id="dropdown01" aria-haspopup="true" aria-haspopup="true" aria-expanded="false">Наши услуги</a>

                                </li>
                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="works.php" tabindex="-1" aria-disabled="true">Портфолио</a>
                                </li>

                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="contacts.php" tabindex="-1" aria-disabled="true">Контакты</a>
                                </li>
                            </ul>

                        </div>


                    </div>
                    <div class="container ">
                        <div>
                            <h5 class="white upper footer_pb20">Контакты</h5>
                        </div>
                    </div>
                    <div class="container">

                        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav_color nav_size" href="/">+7 (727) 317-16-98</a>
                                </li>

                                <li class="nav-item nav_mar">
                                    <a class="nav_color nav_size" href="about_us.php" tabindex="-1" aria-disabled="true">+7 (705) 450-39-16</a>
                                </li>



                            </ul>

                        </div>


                    </div>
                </nav>
            </div>
        </div>
    </div>

</footer>
