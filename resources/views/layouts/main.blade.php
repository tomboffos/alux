<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('icon.png')}}" sizes="42x42">
    <title>
        {!! $title['content'] !!}
    </title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="keywords" content="{!!$meta_tag_keywords['content']!!}">
    <meta name="description" content="{!! $meta_tag_description['content'] ?? 0 !!}" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('n_css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('n_css/style.css')}}">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

   
</head>

<body>

    <nav id="header_menu" class="navbar navbar-expand-md fixed-top d-none d-lg-block">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="img/logo_alux.svg" width="100px"></a>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault" style="position:relative; top:-7px">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item nav_color @if($active=='main') active @else default @endif" data-hover="index">
                        <a class="nav-link nav_size nav_mar_top" href="{{route('Index')}}">Главная <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item nav_color @if($active=='about') active @else default @endif" data-hover="about">
                        <a class="nav-link nav_size nav_mar_top" href="{{route('AboutUs')}}" tabindex="-1" aria-disabled="true">О нас</a>
                    </li>


                    <li class="nav-item dropdown nav_color @if($active=='services') active @else default @endif" data-hover="services">
                        <a class="nav-link nav_size nav_mar_top" href="{{route('Services')}}" target="_blank" id="dropdown01" aria-haspopup="true" aria-haspopup="true" aria-expanded="false">Наши услуги</a>
                        <div class="dropdown-menu hidden_menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item nav_color nav_size" href="internetmagazin.html">Интернет магазин</a>
                            <a class="dropdown-item nav_color nav_size" href="landingpage.html">Лендинг пейдж</a>
                            <a class="dropdown-item nav_color nav_size" href="korporativniysite.html">Корпоративный сайт</a>
                            <a class="dropdown-item nav_color nav_size" href="razrabotkabrandbook.html">Фирменный стиль</a>
                            <a class="dropdown-item nav_color nav_size" href="videoinfografika.html">Видеографика</a>
                            <a class="dropdown-item nav_color nav_size" href="razrabotkamobilnihprilozheniy.html">Мобильные приложения</a>
                            <a class="dropdown-item nav_color nav_size" href="smm.html">SMM продвижение</a>
                        </div>
                    </li>
                    <li class="nav-item nav_color @if($active=='projects') active @else default @endif" data-hover="portfolio">
                        <a class="nav-link nav_size nav_mar_top" href="{{route('Projects')}}" tabindex="-1" aria-disabled="true">Портфолио</a>
                    </li>

                    <li class="nav-item nav_color @if($active=='contacts') active @else default @endif'" data-hover="contacts">
                        <a class="nav-link nav_size nav_mar_top" href="{{route('Contacts')}}" tabindex="-1" aria-disabled="true">Контакты</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="btn btn-md btn_outline_white order-btn mt-3" href="#" tabindex="-1" aria-disabled="true">Заказать услугу</a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <!-- mobile -->
    <nav id="header_menu" class="navbar navbar-expand-md fixed-top  d-lg-none d-xs-block">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{asset('img/logo_alux.svg')}}" width="100px" class="logo__img"></a>


            <content class="mob-view">

                <input type="checkbox" id="hamburger" class="hamburger ">
                <label for="hamburger" class="hamburger">
                    <i></i>
                    <text style="display: none"></text>
                </label>

                <section class="drawer-list">
                    <ul>
                        <li>
                            <a href="index.html" data-id="1">Главная</a>
                        </li>
                        <li class="">
                            <a href="about.html" data-id="2">О нас</a>
                        </li>
                        <li class="dropdown">
                            <label for="dropdown">
                                <input type="hidden" class="ourService" value="{{route('Services')}}">
                                <a data-id="3">
                                    <span style="display: inline" onclick="window.location.href = document.querySelector('.ourService').value">
                                        Наши услуги
                                    </span>
                                </a>
                            </label>
                            <input type="checkbox" id="dropdown">
                            <ul>
                                <li>
                                    <a href="e-commerceagazin.html">Интернет магазин</a>
                                </li>
                                <li>
                                    <a href="landingpage.html">Лендинг пейдж</a>
                                </li>
                                <li>
                                    <a href="korporativniysite.html">Корпоративный сайт</a>
                                </li>
                                <li>
                                    <a href="razrabotkabrandbook.html">Фирменный стиль</a>
                                </li>
                                <li><a href="videoinfografika.html">Видеоинфографика</a></li>
                                <li><a href="razrabotkamobilnihprilozheniy.html">Мобильные приложения</a></li>
                                <li><a href="smm.html">SMM продвижение</a></li>
                            </ul>
                        </li>
                        <li class="@if($active=='projects') active @else default @endif" data-hover="portfolio">
                            <a href="portfolio.html" data-id="4">Портфолио</a>
                        </li>
                        <li>
                            <a href="otzyvy.html" data-id="5" target="_blank">Отзывы</a>
                        </li>
                        <li class="@if($active=='contacts') active @else default @endif'" data-hover="contacts">
                            <a href="contacts.html" data-id="6">Контакты</a>
                        </li>
                        <li>
                            <a href="#" class="btn-hollow order-btn">Заказать
                                услугу</a>
                        </li>

                    </ul>
                </section>
            </content>

        </div>
    </nav>




    <body>

        <!--
<body class="mobile">
    <link rel="stylesheet" href="{{asset('css/new_css.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl-carousel-owl-theme-bootstrap-slick-fancybox-sweetalert-style-21.min.css')}}">


    <link rel="stylesheet" href="{{asset('fonts/Sf_pro/style.css')}}">

    <div class="contect">
        <div class="top-navbar-wrapper">
            <div class="top-navbar-start"></div>
            <div class="top-navbar hidden-xs collect-in">
                <div class="container">
                    <div class="row">
                        <div class="top-flex" itemscope itemtype="http://schema.org/WPHeader">
                            <a class="header_logo" href="/">
                                <img src="{{asset('/img/logo_alux.svg')}}" itemprop="image" alt="A-Lux">
                            </a>
                            <nav class="top-menu">
                                <ul>
                                    <li class="hover @if($active=='main') active @else default @endif"
                                        data-hover="index">
                                        <a href="{{route('Index')}}">Главная</a>
                                    </li>
                                    <li class="hover @if($active=='about') active @else default @endif "
                                        data-hover="about'"> <a href="{{route('AboutUs')}}">О нас</a></li>
                                    <li class="hover @if($active=='services') active @else default @endif"
                                        data-hover="services"><a href="{{route('Services')}}">Наши услуги</a></li>
                                    <li class="hover @if($active=='projects') active @else default @endif"
                                        data-hover="portfolio"><a href="{{route('Projects')}}">Наши работы</a></li>
                                    <li class="hover @if($active=='reviews') active @else default @endif"
                                        data-hover="otzyvy"><a href="{{route('CustomerReviews')}}">Отзывы</a></li>
                                    <li class="hover @if($active=='contacts') active @else default @endif'"
                                        data-hover="contacts"><a href="{{route('Contacts')}}">Контакты</a></li>
                                </ul>
                                <ul class="hidden-menu" id="dropdown-services">
                                    <li><a href="internetmagazin.html">Интернет магазин</a></li>
                                    <li><a href="landingpage.html">Лендинг пейдж</a></li>
                                    <li><a href="korporativniysite.html">Корпоративный сайт</a></li>
                                    <li><a href="razrabotkabrandbook.html">Фирменный стиль</a></li>
                                    <li><a href="videoinfografika.html">Видеоинфографика</a></li>
                                    <li><a href="razrabotkamobilnihprilozheniy.html">Мобильные приложения</a></li>
                                    <li><a href="smm.html">SMM продвижение</a></li>
                                </ul>
                            </nav>
                            <a class="btn-hollow order-btn" href="#">заказать услугу</a>
                            <div class="block-phones">
                                <label style="color:#fff;clear:both;display:block;"><i class="f f-phone"></i><span>Наши
                                        телефоны:</span></label>
                                <label style="color:#fff;clear:both;display:block;"><a href="tel:+77273171698">+7
                                        <span>(727)</span> 317-16-98</a></label>
                                <label style="color:#fff;clear:both;display:block;"><a href="tel:+77273171698">+7
                                        <span>(705)</span> 450-39-16</a></label>
                            </div>
                            <div class="right-menu " style="display: none"><img class=""
                                    style="height: 40px; width: auto;" src="{{asset('img/phone-call.png')}}" alt="">
                            </div>
                            <div class="mobile-menu-right ">
                                <p>Наши телефоны:</p>
                                <a href="tel:+77273171698">+7 (727) 317-16-98</a>
                                <a href="tel:+77054503916">+7 (705) 450-39-16</a>
                                 <div class="triangle-tooltip"></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->
        @php $test = \App\Test::find(1); @endphp
        @yield('content')

        @include('layouts.footer')
       <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script async src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
        </script>
        
<a href="https://api.whatsapp.com/send?phone=+77054503916&amp;text=Здравствуйте, меня интересует разработка веб сайта." target="_blank" id="wa__toggle">
    <div class="circlephone"></div>
    <div class="circle-fill"></div>
    <div class="img-circle">
        <img src="../img/whatsapp_icon.png">
    </div>
</a>

    
        <button type="button" class="btn btn-price btn_test btn-primary" data-toggle="modal" data-target="#exampleModalLong">
            Узнать стоимость сайта
        </button>

        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="horizontal-position-modal-item">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{$test->name}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background: transparent; outline: none; color: #FFFFFF;">
                                <span style="outline: none; font-size: 4rem; font-weight: 300;" aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-subtitle-window">
                            Доброго времени суток, уважаемый посетитель сайта a-lux.kz. Мы подготовили для Вас короткий
                            опрос, который поможет нам в расчете стоимости необходимой Вам услуги.
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            @foreach($test->steps as $step)

                            <div class="col-lg-12 test_block  @if($loop->index == 0) active @endif" data-test-step="{{ $step->id }}" id="test_block-{{$step->id}}">
                                <div class="range_block">

                                    <div class="range">

                                    </div>

                                </div>
                                <h3>
                                    {{$step->question}}
                                </h3>

                                <div class="choose_content">
                                    @foreach($step->answers as $answer)
                                    <button data-answer='{{$answer->id}}' data-next-question='{{$answer->next_step_id}}' data-step='{{$step->id}}' class="choose_button choose_{{$step->id}}">
                                        {{$answer->answer}}
                                    </button>
                                    @endforeach

                                    <form action="{{route('admin.createAnswer')}}" method="post" class="style-form-width">
                                        {{csrf_field()}}
                                        <div class="form-group mt-2">
                                            <input data-answer='{{$answer->id}}' data-next-question='{{$answer->next_step_id}}' data-step='{{$step->id}}' type="text" id="answer_type_{{ $step->id }}" class="form-control answer-input choose_button choose_{{$step->id}}" value="" name="answer-input" placeholder="Свой вариант ответа">
                                        </div>
                                    </form>
                                </div>

                            </div>
                            @endforeach
                            <div class="col-lg-12 main_setting">
                                <div class="d-flex align-items-center" style="
										  display: flex;
										  justify-content: flex-end;
										  align-items: center;
									 ">
                                    <button class="previous_arrow" id="previous">
                                        <span9>Назад</span>
                                    </button>


                                    <button disabled class="next_button" id="next_button">
                                        Вперед
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <input type="hidden" id="steps_count" value="{{count($test->steps)}}">
        <script>
            let nextStep = 0;
            var counter = $("#steps_count").val(),
                step = 1,
                main = "",
                step_percent = 0,
                percent_one = 100 / counter;
            step_percent += percent_one, $(document).ready(function() {
                    console.log("hi");
                    var e = 100 - Math.ceil(step_percent);
                    $(".range").css("background", "linear-gradient(to right, #406BFF " + Math.ceil(step_percent /
                            2) +
                        +",#406BFF " + Math.ceil(step_percent / 2) +
                        "%, #E3E3E3 " + e + "%)")
                }), $("#previous").on("click", function() {
                    (step -= 1) > 0 ? ($(".test_block.active").removeClass("active"), $(".test_block:nth-child(" +
                        step + ")").addClass("active")) : step += 1
                }),
                $("#next_button").on("click", function() {
                    $(this).attr('data-next-step', nextStep)
                    let currentStepId = $('.test_block.active').attr('data-test-step')
                    console.log(currentStepId)
                    console.log(document.getElementById(`answer_type_${currentStepId}`).value)
                    if ($(`.answer_type_${currentStepId}`).val !== '') {
                        $.ajax({
                            type: "POST",
                            url: "/api/request/test/custom/answer",
                            data: {
                                title: document.getElementById(`answer_type_${currentStepId}`).value,
                                step_id: currentStepId
                            },
                            // success: function(e) {
                            //     alert(e.msg)
                            //     $('.modal-content, .modal-backdrop, .btn_test').hide(),
                            //     $('.modal-backdrop').removeClass('show')
                            // }
                        })
                    }
                    if (nextStep === '') console.log("ok"), $.ajax({
                        type: "POST",
                        url: "/api/request/test",
                        data: {
                            answers: JSON.stringify(answers)
                        },
                        success: function(e) {
                            alert(e.msg)
                            $('.modal-content, .modal-backdrop, .btn_test').hide(),
                                $('.modal-backdrop').removeClass('show')
                        }
                    });
                    else {
                        step += 1;
                        var e = 100 - (step_percent += percent_one);
                        console.log(step_percent),
                            $(".test_block.active").removeClass("active"),
                            $(".range").css("background", "linear-gradient(to right, #406BFF " + step_percent +
                                "%, #E3E3E3 " + e + "%)")
                        console.log(`test_block-${nextStep}`)
                        $(`#test_block-${nextStep}`).addClass("active")
                    }
                });
            var answers = [];
            $(".choose_button").on("click", function() {
                $('.next_button').prop("disabled", false);
                $('.next_button').css({
                    'background': 'linear-gradient(90deg, #456FFF -15.22%, #003AFF 150%), #3A66FF',
                    'color': '#fff'
                })
                var e = $(this).attr("data-step");
                nextStep = $(this).attr('data-next-question')
                console.log($(this).attr('data-next-question'))
                parseInt(e) === parseInt(step) && $(".choose_" + e + ".active").removeClass("active"), $(this)
                    .toggleClass("active"), main = $(this).attr("data-answer");
                $(this).siblings().removeClass('active');

                var t = {
                        step_id: parseInt(e),
                        answer_id: parseInt(main)
                    },
                    s = 0;
                for (let t of Object.values(answers)) t.step_id === parseInt(e) && (t.answer_id = parseInt(
                    main), s = 1);
                0 === s && answers.push(t), console.log(answers)
                $('.answer-input').on('click', function() {
                    $('.choose_button').removeClass('active')
                });
            });

        </script>




        
        <script src="{{asset('n_js/scripts.js')}}" type="application/javascript"></script>
        <script async src="{{asset('../n_js/modal-script.js')}}"></script>
        <script src="{{asset('n_js/sweetalert.min.js')}}" type="application/javascript"></script>
        
        <script async src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
        </script>
        <script async src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.rawgit.com/tonystar/bootstrap-hover-tabs/v3.1.1/bootstrap-hover-tabs.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
        <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'GA_MEASUREMENT_ID');

        </script>
    </body>

</html>
<!--
    <script async src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script async type="text/javascript" src="{{asset('js/map.js')}}"></script>
    <script async src="{{asset('js/slick/slick.min.js')}}"></script>
    <script async src="{{asset('js/jquery.fancybox.min.js')}}"></script>


    {{--<script async src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>--}}
    {{--<script async src="{{asset('js/vendor/slick.min.js')}}" ></script>--}}
    <script async src="{{asset('js/vendor/parallax.min.js')}}"></script>
    {{-- <script src="{{asset('js/vendor/jquery.fancybox.pack.js')}}"></script> --}}
    <script async src="{{asset('js/vendor/sweetalert.min.js')}}"></script>
    <script async src="{{asset('js/script.js')}}"></script>

    <script async src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js" async></script>
    <script async src="{{asset('js/dropdown.js')}}"></script>
    <script async src="{{asset('js/hamburger.js')}}">

    </script>
     <script async src="{{asset('js/landscript.js')}}"></script>
    <script async src="{{asset('js/js.js')}}"></script>

    {{--<script async type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVEiVvApqSD4TgfTYyNpsUpxhgfVDoRIQ"></script>--}}
-->
