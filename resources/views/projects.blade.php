@extends('layouts.main')
@section('content')

<section id="about_page_sec_6">

    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 text-center ">
                <h3 class="portfolio_title" align="left">Наше портфолио</h3>


                <ul class="dropwdown_navlink nav mt-4" role="tablist">
                    <li>
                        <a href="#all" role="tab" data-toggle="tab" class="active tabs_btns_portfolio">Все</a>
                    </li>
                    <li>
                        <a href="#websites" role="tab" data-toggle="tab" class="tabs_btns_portfolio ml-4">Веб-сайты</a>
                    </li>
                    <li>
                        <a href="#apps" role="tab" data-toggle="tab" class="tabs_btns_portfolio ml-4">Мобильные приложения</a>
                    </li>
                    <li>
                        <a href="#design" role="tab" data-toggle="tab" class="tabs_btns_portfolio ml-4">Дизайн</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="pills-all-tab">
                    <div class="row">
                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#project-one-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#project-one-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class=" col"><a href="#project-one-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well">
                                <div class="tab-pane active" id="project-one-1"><img src="img/portfolio/one_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Дирекция спортивных сооружений г. Алматы</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="project-one-2">

                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Дирекция спортивных сооружений г. Алматы</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="project-one-3">

                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Дирекция спортивных сооружений г. Алматы</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#project-two-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#project-two-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class=" col"><a href="#project-two-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="project-two-1">
                                    <img src="img/portfolio/two_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title black">BAIZAAR</p>
                                        <p class="image_minititle black">Веб-сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="project-two-2">
                                    <img src="img/portfolio/1-3.jpg" width="100%">

                                    <div class="project__name">
                                        <p class="image_title black">BAIZAAR</p>
                                        <p class="image_minititle black">Веб-сайт</p>
                                    </div>
                                </div>

                                <div class="tab-pane" id="project-two-3">
                                    <img src="img/portfolio/1-3.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title black">BAIZAAR</p>
                                        <p class="image_minititle black">Веб-сайт</p>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#project-three-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#project-three-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#project-three-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="project-three-1">
                                    <img src="img/portfolio/three_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="project-three-2">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="project-three-3">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#project-four-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#project-four-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class=" col"><a href="#project-four-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="project-four-1">
                                    <img src="img/portfolio/four_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Жаровня</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="project-four-2">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Жаровня</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="project-four-3">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Жаровня</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tab-pane fade" id="websites" role="tabpanel" aria-labelledby="pills-websites-tab">
                    <div class="row">
                        
                         <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#web-one-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#web-one-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class=" col"><a href="#web-one-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="web-one-1"><img src="img/portfolio/one_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Дирекция спортивных сооружений г. Алматы</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="web-one-2">

                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Дирекция спортивных сооружений г. Алматы</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="web-one-3">

                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">Дирекция спортивных сооружений г. Алматы</p>
                                        <p class="image_minititle white">Корпоративный сайт</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#web-two-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#web-two-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class=" col"><a href="#web-two-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="web-two-1">
                                    <img src="img/portfolio/two_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title black">BAIZAAR</p>
                                        <p class="image_minititle black">Веб-сайт</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="web-two-2">
                                    <img src="img/portfolio/1-3.jpg" width="100%">

                                    <div class="project__name">
                                        <p class="image_title black">BAIZAAR</p>
                                        <p class="image_minititle black">Веб-сайт</p>
                                    </div>
                                </div>

                                <div class="tab-pane" id="web-two-3">
                                    <img src="img/portfolio/1-3.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title black">BAIZAAR</p>
                                        <p class="image_minititle black">Веб-сайт</p>
                                    </div>

                                </div>
                            </div>
                        </div>

                       
                    </div>
                </div>
                <div class="tab-pane fade" id="apps" role="tabpanel" aria-labelledby="pills-apps-tab">
                    <div class="row">
                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#apps-one-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#apps-one-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#apps-one-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="apps-one-1">
                                    <img src="img/portfolio/three_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">App</p>
                                        <p class="image_minititle white">Мобильное приложение</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="apps-one-2">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">App</p>
                                        <p class="image_minititle white">Мобильное приложение</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="apps-one-3">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">App</p>
                                        <p class="image_minititle white">Мобильное приложение</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#apps-two-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#apps-two-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#app-two-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="apps-two-1">
                                    <img src="img/portfolio/three_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">App</p>
                                        <p class="image_minititle white">Мобильное приложение</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="apps-two-2">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">App</p>
                                        <p class="image_minititle white">Мобильное приложение</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="apps-two-3">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">App</p>
                                        <p class="image_minititle white">Мобильное приложение</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="design" role="tabpanel" aria-labelledby="pills-design-tab">
                    <div class="row">
                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#design-one-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#design-one-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#design-one-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="design-one-1">
                                    <img src="img/portfolio/three_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="design-one-2">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="design-one-3">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mt-4 ">
                            <ul class="nav nav-pills rel_block hover_nav_position">
                                <li class="active col"><a href="#design-two-1" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#design-two-2" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                                <li class="col"><a href="#design-two-3" data-toggle="tab">
                                        <div class="line_btn_portfolio"></div>
                                    </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content well border_portfolio">
                                <div class="tab-pane active" id="design-two-1">
                                    <img src="img/portfolio/three_image.png" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="design-two-2">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="design-two-3">
                                    <img src="img/portfolio/1-2.jpg" width="100%">
                                    <div class="project__name">
                                        <p class="image_title white">BI GROUP</p>
                                        <p class="image_minititle white">Дизайн сайта</p>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


@endsection