@extends('layouts.main')
@section('content')
<link rel="stylesheet" href="/css/owl-carousel-owl-theme-bootstrap-slick-fancybox-sweetalert-style-21.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

<div class="new-topbar hidden-md hidden-lg navbar-fixed-top">
    <div></div>
    <a href="#"><img style="height: 45px; width: auto;" src="/img/logo.png" alt=""></a>
    <div class="right-menu"><img class="" style="height: 40px; width: auto;" src="/images/phone-call.png" alt=""></div>
</div>




<div class="mobile-menu-right">
    <p>Наши телефоны:</p>
    <a href="tel:+77273171698">+7 (727) 317-16-98</a>
    <a href="tel:+77054503916">+7 (705) 450-39-16</a>
    <!-- <div class="triangle-tooltip"></div> -->
</div>
<header class="top">
    <div class="owl-carousel owl-theme main_home_slider">
        <div class="main_home_slider_item">
            <div class="main_home_slider_item_bg item-first" style="background-image: url(img/slide-banner.png); background-position-x: 50%;"></div>
            <div class="container item-first-text">
                <div class="main_home_slider_item_inner right">
                    <div class="main_home_slider_item_content">
                        <!--												<img src="img/logo.png" class="mobile-logo-alux">-->
                        <div class="main_home_slider_item_title" style="line-height: 1;margin-bottom: 30px;">
                            <span style="font-size: 2.2rem;margin-bottom: 35px;" class="color_blue">Создание уникальных корпоративных веб-сайтов в Казахстане.</span><br>
                        </div>

                        <div class="main_home_slider_item_title" style="margin-bottom: 30px;">
                            <span style="font-size: 2.7rem;margin-bottom: 50px;" class="uppercase">s<span class="color_blue uppercase">mart.</span> s<span class="color_blue uppercase">imple.</span> s<span class="color_blue uppercase">uperawesome</span></span>
                        </div>
                        <div class="main_home_slider_item_desc">
                            На протяжении 13 лет мы занимаемся успешным созданием уникальных веб-сайтов класса LUX. Благодаря комплексному подходу и качественной разработке, желаемый результат гарантируется. Веб-студия «A-Lux» занимается созданием сайтов различной сложности, начиная от landing page, и заканчивая крупными корпоративными проектами.

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <style>
            .main_home_slider .owl-item.active .main_home_slider_item_bg {
                animation: none;
            }

        </style>
    </div>
</header>



<div class="col-md-12 col-sm-12 blockk1">
    <script type="application/ld+json">
        {
            "@context": "http://www.schema.org",
            "@type": "Organization",
            "name": "A-Lux",
            "url": "https://www.a-lux.kz/",
            "logo": "https://www.a-lux.kz/img/logo.png",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Ул. Шевченко 165 Б, офис. 511",
                "addressLocality": "Алматы",
                "addressRegion": "Алматинская область",
                "postalCode": "050009",
                "addressCountry": "Казахстан"
            },
            "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "+77273171698",
                "email": "info@a-lux.kz",
                "contactType": "customer service",
                "availableLanguage": "Russian"
            }
        }

    </script>

    <div class="conwrap">
        <h1 class="text-uppercase">МЫ ВЛИВАЕМСЯ В СФЕРУ ФУНЦИОНИРОВАНИЯ БИЗНЕСА НАШИХ КЛИЕНТОВ</h1>
        <p class="italict">ОБЕСПЕЧИВАЕМ КАЧЕСТВЕННЫЙ И ИНДИВИДУАЛЬНЫЙ ПОДХОД</p>
        <div class="col-md-8 utp">
            <div class="row">
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(1).png alt="">
                    <p>Проводим детальной анализ конкурентоспособной среды.</p>
                </div>
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(2).png alt="">
                    <p>Тщательно исследуем целевую аудиторию в целях правильной расстановки приоритетов.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(3).png alt="">
                    <p>Мы работаем до полного утверждения сайта заказчиком. Количество макетов неограниченно.</p>
                </div>
                <div class="col-md-6 col-sm-12 utp-item">
                    <img src=img/target-(4).png alt="">
                    <p>Даем полную гарантию на бесперебойное и надежное функционирование сайта.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 utp-list">
            <h2 class="text-uppercase">цели и задачи <br> корпоративного сайта</h2>
            <div class="utp-list-text">
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Значительное увеличение клиентов</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Повышение уровня вашего бизнеса в глазах конкурентов</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Формирование серьезного и солидного имиджа</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Предоставление информации обо всех направлениях деятельности компании</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Возможность обратной связи и консультация в режиме онлайн</div>
                </div>
                <div class="utp-list-text-item">
                    <div class="for-img">
                        <img src="img/success.png" alt="">
                    </div>
                    <div class="for-text">Повышение лояльности посетителей</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-sm-12 block2">
    <div class="conwrap">
        <div class="brif-content col-md-5 col-sm-12">
            <h1 class=" text-uppercase">Бриф на разработку корпоративного сайта</h1>
            <p>Заполненный опросник, именуемый брифом, окажет значительное влияние на скорость и получение желаемого результата, ведь благодаря четкому пониманию требований и пожеланий, процесс разработки дизайна будет осуществляться гораздо быстрее. </p>
            <label>
                <a download href="Бриф на дизайн (format).rtf">
                    <span>Скачать бриф</span>

                </a></label>
        </div>
        <div class="col-md-7">
        </div>
    </div>
</div>
<div class="col-md-12 col-sm-12 block3 mb-5">
    <div class="conwrap">
        <div class="row">
            <h1 class="text-uppercase">Примеры корпоративных сайтов,</h1>
            <p class="italict">созданных нашей веб студией</p>
            <img src="img/line-tran.png" alt="" style="margin: auto;display: block;">
            <div class="col-md-4 col-sm-12 utp-item1">
                <img src=img/1200px-VTB_Logo_2018.png alt=""><br>
                <div class="block3-itext" style="padding: 19px 10px;line-height: 1;">ВТБ Банк</div>
            </div>
            <div class="col-md-4 col-sm-12 utp-item2">
                <img src=img/logo_aziakredit.png alt="">
                <div class="block3-itext">AsiaCredit Bank</div>
            </div>

            <div class="col-md-4 col-sm-12 utp-item3">
                <img src=img/LG-lifes-good-gray-lettering.png alt="">
                <div class="block3-itext">LG</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12 utp-item4">
                <img src=img/logo_small.png alt=""><br>
                <div class="block3-itext">iPoint</div>
            </div>
            <div class="col-md-4 col-sm-12 utp-item5">
                <img src=img/asialife-logo.png alt="">
                <div class="block3-itext">Asia Life</div>
            </div>

            <div class="col-md-4 col-sm-12 utp-item6">
                <img src=img/payment-method-logo-homebank.png alt="">
                <div class="block3-itext">HOMEBANK.</div>
            </div>
        </div>
        <br><br>
        <span><a href="/portfolio.html">Все работы</a></span>
        <br><br>
    </div>
    <div class="row">
        <div class="form-wrapp">
            <h2>заявка на разработку сайта</h2>
            <form action="" id="btn-hollow1">
                <input type="text" name="name" placeholder="Представьтесь, пожалуйста">
                <input type="text" name="phone" placeholder="Как с Вами связаться?"><br><br>
                <textarea name="msg" id="" cols="97" rows="3" placeholder="Опишите задачу" style="margin-bottom:20px;"></textarea>
                 </form>
             <a class="btn-hollow order-btn" href="#" style="padding: 14px 53px!important; color: #4490c3;">заказать</a>
        </div>
    </div>
    <div class="row">
        <div class="conwrap">
            <h1 class="text-uppercase">Над вашим сайтом будут работать</h1>
            <p class="italict">Лучшие профессионалы своего дела</p>
            <div class="col-md-12 col-sm-12 utp">
                <div class="row">
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/secretary.png alt="">
                        <p class="text-uppercase">Личный менеджер проекта</p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/programmer.png alt="">
                        <p class="text-uppercase">верстальщики</p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/training.png alt="">
                        <p class="text-uppercase">seo-оптимизаторы</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/paint-palette.png alt="">
                        <p class="text-uppercase"> ui / ux дизайнеры </p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/learning.png alt="">
                        <p class="text-uppercase"> программисты </p>
                    </div>
                    <div class="col-md-4 col-sm-12 utp-item">
                        <img src=img/writer.png alt="">
                        <p class="text-uppercase">рекламные копирайтеры</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
</div>
<!--	<div class="footer col-md-12 col-sm-12">
            <div class="conwrap">
                <div class="soc-wrap">

                    <div class="soc-img">
                       <a href=""><img src="./img/vk.png" alt=""></a>
                    </div>
                    <div class="soc-img">
                        <a href=""><img src="./img/fb.png" alt=""></a>
                    </div>
                    <div class="soc-img">
                       <a href=""><img src="./img/in.png" alt=""></a>
                    </div>




                        <a class="ft-socs-item ft-socs-vk" href="https://vk.com/club6291800"> </a>
    <a class="ft-socs-item ft-socs-fb" href="#"> </a>
    <a class="ft-socs-item ft-socs-inst" href="#"> </a>

                    <div class="foot_hr"></div>
                </div>
                <h3>COPYRIGHT © 2001-2020 ALMATYLUXURY WEB DESIGN STUDIO</h3>
                <div class="spft">Создаём и продвигаем эффективные веб сайты в Алматы и других городах Казахстана уже более 13 лет.<br>карта сайта/ sitemap</div>
            </div>
        </div>		-->



<div class="whatsapp">
    <a href="https://api.whatsapp.com/send?phone=+77054503916&text=Здравствуйте, меня интересует разработка веб сайта."></a>
    <div class="circlephone" style="transform-origin: center;"></div>
    <div class="circle-fill" style="transform-origin: center;"></div>
</div>
<style>
    .whatsapp {
        background: url(/images/whatsapp.png) center center no-repeat;
        background-size: 100%;
        width: 50px;
        height: 50px;
        position: fixed;
        bottom: 10px;
        right: 10px;
        z-index: 999;
    }

    .whatsapp a {
        display: block;
        width: 100%;
        height: 100%;
        position: relative;
        z-index: 999;
    }

    .circlephone {
        box-sizing: content-box;
        -webkit-box-sizing: content-box;
        border: 2px solid #44bb6e;
        width: 80px;
        height: 80px;
        bottom: -15px;
        left: -15px;
        position: absolute;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        opacity: .5;
        -webkit-animation: circle-anim 2.4s infinite ease-in-out !important;
        -moz-animation: circle-anim 2.4s infinite ease-in-out !important;
        -ms-animation: circle-anim 2.4s infinite ease-in-out !important;
        -o-animation: circle-anim 2.4s infinite ease-in-out !important;
        animation: circle-anim 2.4s infinite ease-in-out !important;
        -webkit-transition: all .5s;
        -moz-transition: all .5s;
        -o-transition: all .5s;
        transition: all 0.5s;
    }

    .circle-fill {
        box-sizing: content-box;
        -webkit-box-sizing: content-box;
        background-color: #44bb6e;
        width: 50px;
        height: 50px;
        bottom: -2px;
        left: -2px;
        position: absolute;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        border: 2px solid transparent;
        -webkit-animation: circle-fill-anim 2.3s infinite ease-in-out;
        -moz-animation: circle-fill-anim 2.3s infinite ease-in-out;
        -ms-animation: circle-fill-anim 2.3s infinite ease-in-out;
        -o-animation: circle-fill-anim 2.3s infinite ease-in-out;
        animation: circle-fill-anim 2.3s infinite ease-in-out;
        -webkit-transition: all .5s;
        -moz-transition: all .5s;
        -o-transition: all .5s;
        transition: all 0.5s;
    }

    @keyframes pulse {
        0% {
            transform: scale(0.9);
            opacity: 1;
        }

        50% {
            transform: scale(1);
            opacity: 1;
        }

        100% {
            transform: scale(0.9);
            opacity: 1;
        }
    }

    @-webkit-keyframes pulse {
        0% {
            -webkit-transform: scale(0.95);
            opacity: 1;
        }

        50% {
            -webkit-transform: scale(1);
            opacity: 1;
        }

        100% {
            -webkit-transform: scale(0.95);
            opacity: 1;
        }
    }

    @keyframes tossing {
        0% {
            transform: rotate(-8deg);
        }

        50% {
            transform: rotate(8deg);
        }

        100% {
            transform: rotate(-8deg);
        }
    }

    @-webkit-keyframes tossing {
        0% {
            -webkit-transform: rotate(-8deg);
        }

        50% {
            -webkit-transform: rotate(8deg);
        }

        100% {
            -webkit-transform: rotate(-8deg);
        }
    }

    @-moz-keyframes circle-anim {
        0% {
            -moz-transform: rotate(0deg) scale(0.5) skew(1deg);
            opacity: .1;
            -moz-opacity: .1;
            -webkit-opacity: .1;
            -o-opacity: .1;
        }

        30% {
            -moz-transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .5;
            -moz-opacity: .5;
            -webkit-opacity: .5;
            -o-opacity: .5;
        }

        100% {
            -moz-transform: rotate(0deg) scale(1) skew(1deg);
            opacity: .6;
            -moz-opacity: .6;
            -webkit-opacity: .6;
            -o-opacity: .1;
        }
    }

    @-webkit-keyframes circle-anim {
        0% {
            -webkit-transform: rotate(0deg) scale(0.5) skew(1deg);
            -webkit-opacity: .1;
        }

        30% {
            -webkit-transform: rotate(0deg) scale(0.7) skew(1deg);
            -webkit-opacity: .5;
        }

        100% {
            -webkit-transform: rotate(0deg) scale(1) skew(1deg);
            -webkit-opacity: .1;
        }
    }

    @-o-keyframes circle-anim {
        0% {
            -o-transform: rotate(0deg) kscale(0.5) skew(1deg);
            -o-opacity: .1;
        }

        30% {
            -o-transform: rotate(0deg) scale(0.7) skew(1deg);
            -o-opacity: .5;
        }

        100% {
            -o-transform: rotate(0deg) scale(1) skew(1deg);
            -o-opacity: .1;
        }
    }

    @keyframes circle-anim {
        0% {
            transform: rotate(0deg) scale(0.5) skew(1deg);
            opacity: .1;
        }

        30% {
            transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .5;
        }

        100% {
            transform: rotate(0deg) scale(1) skew(1deg);
            opacity: .1;
        }
    }

    @-moz-keyframes circle-fill-anim {
        0% {
            -moz-transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }

        50% {
            -moz-transform: rotate(0deg) -moz-scale(1) skew(1deg);
            opacity: .2;
        }

        100% {
            -moz-transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }
    }

    @-webkit-keyframes circle-fill-anim {
        0% {
            -webkit-transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }

        50% {
            -webkit-transform: rotate(0deg) scale(1) skew(1deg);
            opacity: .2;
        }

        100% {
            -webkit-transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }
    }

    @-o-keyframes circle-fill-anim {
        0% {
            -o-transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }

        50% {
            -o-transform: rotate(0deg) scale(1) skew(1deg);
            opacity: .2;
        }

        100% {
            -o-transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }
    }

    @keyframes circle-fill-anim {
        0% {
            transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }

        50% {
            transform: rotate(0deg) scale(1) skew(1deg);
            opacity: .2;
        }

        100% {
            transform: rotate(0deg) scale(0.7) skew(1deg);
            opacity: .2;
        }
    }

</style>
<style>
    .conwrap {
        font-family: 'Open Sans', sans-serif;
        text-align: center;
    }

</style>
@endsection
