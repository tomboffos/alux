@extends('layouts.main')
@section('content')
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->


    <link rel="stylesheet" href="../css/new_css.css">
    <link rel="stylesheet" href="/css/internet-magazin-main.css">
    <link rel="stylesheet" href="/css/internet-magazin-media.css">
	 <link rel="stylesheet" href="../css/bootstrap-fontello-fontello-embedded-slick-slick-theme-fotorama-animate-style-shop.min.css" rel="stylesheet">
	 <link rel="stylesheet" href="css/owl-carousel-owl-theme-bootstrap-slick-fancybox-sweetalert-style-21.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZDDHQG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<style>

</style>
<div class="header_slider">
    <div class="header_slider_items">
        <div class="container">
            <div class="row">
                <div class="col-md-6 head">
                    <div class="header_slider_btn_wrap">
                        <a class="header_slider_btn order-internet-magazin" href="#">Хотите максимально повысить продажи?</a>
                    </div>
                </div>
                <div class="col-md-6 header_slider_descr">
                    <h3 class="header_slider_text_white">Займитесь бизнесом!</h3>
                    <h3 class="header_slider_text_blue">Техническую часть и маркетинг </h3>
                    <h3 class="header_slider_text_white">мы возьмём на себя!</h3>
                    <p class="header_slider_p">Для вас мы разработаем качественный интернет-магазин с удобной системой управления под ключ.
                        Проанализируем конкурентов и бесплатно настроим максимально эффективную рекламную компанию.
                        Обеспечим бесперебойную работу магазина и окажем бесплатную техническую поддержку сроком на 12 месяцев.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="header_slider_items">
        <div class="container">
            <div class="row">
                <div class="col-md-6 head">
                    <div class="header_slider_btn_wrap">
                        <a class="header_slider_btn order-internet-magazin" href="#">Хотите максимально повысить продажи?</a>
                    </div>
                </div>
                <div class="col-md-6 header_slider_descr">
                    <h3 class="header_slider_text_white">Займитесь бизнесом!</h3>
                    <h3 class="header_slider_text_blue">Техническую часть и маркетинг </h3>
                    <h3 class="header_slider_text_white">мы возьмём на себя!</h3>
                    <p class="header_slider_p">для вас мы разработаем качественный интернет магазин с удобной системой управления под ключ. проанализируем конкурентов и бесплатно настроим максимально эффективную рекламную компанию. обеспечим бесперебойную работу магазина и окажем бесплатную техническую поддержку сроком на 12 месяцев</p>
                </div>
            </div>
        </div>
    </div>
    <div class="header_slider_items">
        <div class="container">
            <div class="row">
                <div class="col-md-6 head">
                    <div class="header_slider_btn_wrap">
                        <a class="header_slider_btn order-internet-magazin" href="#">Хотите максимально повысить продажи?</a>
                    </div>
                </div>
                <div class="col-md-6 header_slider_descr">
                    <h3 class="header_slider_text_white">Займитесь бизнесом!</h3>
                    <h3 class="header_slider_text_blue">Техническую часть и маркетинг </h3>
                    <h3 class="header_slider_text_white">мы возьмём на себя!</h3>
                    <p class="header_slider_p">для вас мы разработаем качественный интернет магазин с удобной системой управления под ключ. проанализируем конкурентов и бесплатно настроим максимально эффективную рекламную компанию. обеспечим бесперебойную работу магазина и окажем бесплатную техническую поддержку сроком на 12 месяцев</p>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="about_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <h1 class="h1_descr">создание интернет-магазина в Алматы</h1>
                <div class="about_head_descr">
                    <div class="about_head_descr_h3">
                        <h2>НЕМНОГО</h2>
                    </div>
                    <div class="about_head_descr_h2">
                        <h2>О НАС</h2>
                    </div>
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="about_icon_descr">
                    <div class="col-md-3 col-lg-3 col-xs-12 col-sm-6">
                        <div class="about-head_icon_descr">
                            <img src="/img/internet-magazin/idea.png" alt="idea">
                            <p>Мы разрабатываем и развиваем только эффективные интернет-магазины, которые действительно продают,
                                используя комплексный подход к вашему бизнесу.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-12 col-sm-6">
                        <div class="about-head_icon_descr">
                            <img src="/img/internet-magazin/badge.png" alt="badge">
                            <p>За 13 лет опыта в сфере IT мы наработали эффективный инструментарий, который позволяет запустить интернет-магазин под ключ в кратчайшие сроки.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-12 col-sm-6">
                        <div class="about-head_icon_descr">
                            <img src="/img/internet-magazin/settings.png" alt="settings">
                            <p>Наши специалисты выполнят интеграцию с любыми внешними системами, например, интеграцию с 1С, системой «Мой Склад», различными CRM и др.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-12 col-sm-6">
                        <div class="about-head_icon_descr">
                            <img src="/img/internet-magazin/smartphone.png" alt="smartphone">
                            <p>В пакет TOP SALES, кроме интернет-магазина входит аналогичное мобильное приложение и видеоинфографика,что позволяет охватить и привлечь новых клиентов.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about_includes">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="col-md-3 col-lg-3 col-sm-3"></div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <div class="about_include_h2">
                        <h2>Разработка интернет-магазина в Алматы включает в себя:</h2>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3"></div>
            </div>
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="about_includes_1">
                    <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                        <img src="/img/internet-magazin/creativity.png" alt="">
                        <p class="abouts_descr_p">Яркий и современный дизайн</p>
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                    <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                        <img src="/img/internet-magazin/ecommerce.png" alt="">
                        <p class="abouts_descr_p">Удобная витрина и формы захвата пользователя</p>
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                    <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                        <img src="/img/internet-magazin/creative.png" alt="">
                        <p class="abouts_descr_p">Грамотно продуманное юзабилити</p>
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                    <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                        <img src="/img/internet-magazin/responsive-design.png" alt="">
                        <p class="abouts_descr_p">Мобильная и планшетные версии сайта</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 about_includes_2">

                <div class="col-md-2 col-lg-2 width_pos_about_2"></div>
                <div class="col-md-2 col-sm-4 col-xs-12 col-lg-2">
                    <img src="/img/internet-magazin/analytics.png" alt="">
                    <p class="abouts_descr_p">Удобная CMS для управления магазином и приложением</p>
                </div>
                <div class="col-md-1 col-lg-1"></div>
                <div class="col-md-2 col-sm-4 col-xs-12 col-lg-2">
                    <img src="/img/internet-magazin/worldwide.png" alt="">
                    <p class="abouts_descr_p">Интеграция с внешними сервисами</p>
                </div>
                <div class="col-md-1 col-lg-1"></div>
                <div class="col-md-2 col-sm-4 col-xs-12 col-lg-2">
                    <img src="/img/internet-magazin/spam.png" alt="">
                    <p class="abouts_descr_p">Настороенная рекламная компания под ключ</p>
                </div>
                <div class="col-md-2 col-lg-2"></div>

            </div>
        </div>
    </div>
</section>
<section class="tarif_plan">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="tarif_plan_h2_descr">
                    <h2>тарифные планы</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="tarif_plan_blocks">
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 tarif_media_block">
                        <div class="fast_start">
                            <div class="plans_descr">
                                <div class="plans_descr_h3">
                                    <h3>быстрый <span class="black_descr">старт</span></h3>
                                </div>
                                <div class="circle"></div>
                                <h2>450.000 <span><img src="/img/internet-magazin/Tenge_symbol.png" alt=""></span></h2>
                                <p class="text_zapusk">Запуск за 5 рабочих дней</p>
                                <p class="text_zapusk_bottom">Не секрет что компания A-LUX - это одна из группы 4 Digital наших компаний. Имея большой опыт в разрабокте мобильных приложений и производства видеоинфографики, предлагаем Вашему
                                    внииманию наш топовый пакет,который разработан, чтобы привлечь максимум клиентов и посетителей в ваш интернет магазин. </p>
                                <div class="plans_list">
                                    <ul>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Шаблон премиум-класса (Продающий стильный дизайн)</li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Удобная система управления (Open Cart или Woocommertz)</li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Интуитивная структура</li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Корзина,простое оформление заказа</li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Личный кабинет пользователя</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="start_plan_btn_links">
                                <div class="link_plans">
                                    <a href="#">смотреть подробности</a>
                                </div>
                                <div class="btn_plans">
                                    <a href="#" class="order-btn">Заказать</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 tarif_media_block">
                        <div class="basic">
                            <div class="plans_descr">
                                <div class="plans_descr_h3">
                                    <h3>пакет <span class="black_descr">базовый</span></h3>
                                </div>
                                <div class="circle"></div>
                                <h2>750.000 <span><img src="/img/internet-magazin/Tenge_symbol.png" alt=""></span></h2>
                                <p class="text_zapusk">Запуск за 5 рабочих дней</p>
                                <p class="text_zapusk_bottom">Не секрет что компания A-LUX - это одна из группы 4 Digital наших компаний. Имея большой опыт в разрабокте мобильных приложений и производства видеоинфографики, предлагаем Вашему
                                    внииманию наш топовый пакет,который разработан, чтобы привлечь максимум клиентов и посетителей в ваш интернет магазин. </p>
                                <div class="plans_list">
                                    <ul>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Оригинальный дизайн всех страниц, разработанный специально под Вашу компанию (Работаем до полного утверждения, кол-во макетов не ограничено)</li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Интуитивная структура</li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Корзина, простое оформление заказа</li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Личный кабинет пользователя</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="basic_plan_btn_links">
                                <div class="link_plans">
                                    <a href="#">смотреть подробности</a>
                                </div>
                                <div class="btn_plans">
                                    <a href="#" class="order-btn">Заказать</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 tarif_media_block">
                        <div class="pro">
                            <div class="plans_descr">
                                <div class="plans_descr_h3">
                                    <h3>пакет <span class="black_descr">pro</span></h3>
                                </div>
                                <div class="circle"></div>
                                <h2>1 350.000 <span><img src="/img/internet-magazin/Tenge_symbol.png" alt=""></span></h2>
                                <p class="text_zapusk">Запуск за 40 рабочих дней</p>
                                <p class="text_zapusk_bottom">Не секрет что компания A-LUX - это одна из группы 4 Digital наших компаний. Имея большой опыт в разрабокте мобильных приложений и производства видеоинфографики, предлагаем Вашему
                                    внииманию наш топовый пакет,который разработан, чтобы привлечь максимум клиентов и посетителей в ваш интернет магазин. </p>
                                <div class="plans_list">
                                    <ul>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Разработка мобильного приложения </li>
                                        <li><img src="/img/internet-magazin/check-mark.png" alt=""> Оригинальный дизайн всех страниц, специально разработанный под вашу компанию (Работаем до полного утверждения, количество макетов неограниченно)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pro_plan_btn_links">
                                <div class="link_plans">
                                    <a href="#">смотреть подробности</a>
                                </div>
                                <div class="btn_plans">
                                    <a href="#" class="order-btn">Заказать</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="comand">
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 comand_about_left">
        <div class="comand_about">
            <h2>ЗАКАЗЫВАЯ ИНТЕРНЕТ-МАГАЗИН У НАС <br><span class="bonus_h2">ВЫ ПОЛУЧАЕТЕ ПРИЯТНЫЕ БОНУСЫ<span><br>в подарок!</h2>
        </div>
        <div class="col-md-11 col-xs-8 col-lg-11 col-sm-10 comand_about_list_wrap">
            <div class="comand_about_list">
                <p>Личного проект-менеджера, прикрепленного к проекту для консультаций и технической поддержки сроком на 1 год.</p>
                <p>Техническая поддержка на 1 год.</p>
                <p>Установка и настройка Google Analitycs.</p>
                <p>Настройка и запуск высокоэффективной рекламной компании в поисковых системах или соц. сетях.</p>
            </div>
        </div>
        <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1">
            <div class="comand_about_list_img">
                <ul>
                    <li><img src="/img/internet-magazin/telemarketing-phone-operato.png" alt=""></li>
                    <li><img src="/img/internet-magazin/settings_comn.png" alt=""></li>
                    <li><img src="/img/internet-magazin/analysis.png" alt=""></li>
                    <li><img src="/img/internet-magazin/adwords.png" alt=""></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
        <div id="background-slide1" class="background-slider"></div>
        <div id="background-slide2" class="background-slider"></div>
        <div id="background-slide0" class="background-slider"></div>
    </div>
</section>
<section class="profit">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="profit_descr">
                    <h3>почему с нами</h3>
                    <h2>выгодно?</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="logo_alux">
                    <!-- <img src="/img/internet-magazin/alux.png" alt=""> -->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 alux_profit_img_p">
                    <img src="/img/internet-magazin/megaphone.png" alt="">
                    <p>Профессионалная настройка и<br /> запуск рекламы</p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 alux_profit_img_p">
                    <img src="/img/internet-magazin/discount.png" alt="">
                    <p>Снижение стоимости<br /> одного клиента</p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 alux_profit_img_p">
                    <img src="/img/internet-magazin/group.png" alt="">
                    <p>Команда опытных<br /> специалистов</p>
                </div>
            </div>
        </div> --}}
        <div class="comand_about_row">
            <div class="comand_about_column">
                <img src="/img/internet-magazin/megaphone.png" alt="">
                <p>Профессионалная настройка и<br /> запуск рекламы</p>
            </div>
            <div class="comand_about_column">
                <img src="/img/internet-magazin/discount.png" alt="">
                    <p>Снижение стоимости<br /> одного клиента</p>
            </div>
            <div class="comand_about_column">
                <img src="/img/internet-magazin/group.png" alt="">
                <p>Команда опытных<br /> специалистов</p>
            </div>
            <div class="comand_about_column">
                <img src="/img/internet-magazin/stopwatch.png" alt="">
                <p>Оперативность<br /> решений</p>
            </div>
            <div class="comand_about_column">
                <img src="/img/internet-magazin/shining.png" alt="">
                <p>Прозрачность работы<br /> с клиентом</p>
            </div>
            </div>
        </div>
    </div>
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="col-md-2 col-lg-2"></div>
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 alux_profit_img_p">
                    <img src="/img/internet-magazin/stopwatch.png" alt="">
                    <p>Оперативность<br /> решений</p>
                </div>
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 alux_profit_img_p">
                    <img src="/img/internet-magazin/shining.png" alt="">
                    <p>Прозрачность работы<br /> с клиентом</p>
                </div>
                <div class="col-md-2 col-lg-2"></div>
            </div>
        </div> --}}
    </div>
</section>
<section class="alux_cart">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="alux_cart_descr">
                    <h2>Примеры некоторых наших интернет-магазинов</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="multiple-items">
                    <div class="slider_wrapper">
                        <!-- <h3>Название</h3> -->
                        <img src="../uploads/images/6b97d682e5239c3a659f0cfe10335a9e.jpg" id="img1" alt="">
                        <a class="eye" href="../uploads/images/6b97d682e5239c3a659f0cfe10335a9e.jpg"><img src="/img/internet-magazin/view.png" alt=""></a>
                    </div>
                    <div class="slider_wrapper">
                        <!-- <h3>Название</h3> -->
                        <img src="../uploads/images/ef4a425fd9ad5480458c61128691a209.jpg" alt="">
                        <a class="eye" href="../uploads/images/ef4a425fd9ad5480458c61128691a209.jpg"><img src="/img/internet-magazin/view.png" alt=""></a>
                    </div>
                    <div class="slider_wrapper">
                        <!-- <h3>Название</h3> -->
                        <img src="../uploads/images/dccd70430a3e63c987ede4d33bc0b22d.jpg" alt="">
                        <a class="eye" href="../uploads/images/dccd70430a3e63c987ede4d33bc0b22d.jpg"><img src="/img/internet-magazin/view.png" alt=""></a>
                    </div>
                    <div class="slider_wrapper">
                        <!-- <h3>Название</h3> -->
                        <img src="../uploads/images/708041d47dd5d60e802bc6e0ebc80762.png" alt="">
                        <a class="eye" href="../uploads/images/708041d47dd5d60e802bc6e0ebc80762.png"><img src="/img/internet-magazin/view.png" alt=""></a>
                    </div>
                    <div class="slider_wrapper">
                        <!-- <h3>Название</h3> -->
                        <img src="../uploads/images/4f11e8aebc26b2fbba2f6e0c2c767439.jpg" alt="">
                        <a class="eye" href="../uploads/images/4f11e8aebc26b2fbba2f6e0c2c767439.jpg"><img src="/img/internet-magazin/view.png" alt=""></a>
                    </div>
                    <div class="slider_wrapper">
                        <!-- <h3>Название</h3> -->
                        <img src="../uploads/images/8c0030950b216fb12aa91ea50748df8e.jpg" alt="">
                        <a class="eye" href="../uploads/images/8c0030950b216fb12aa91ea50748df8e.jpg"><img src="/img/internet-magazin/view.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="spektr">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="spektr_descr">
                    <h2>Занимайтесь Бизнесом ! Техническую часть и маркетинг мы берем на себя !</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container spektr_items">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 spektr_item">
                    <img src="/img/internet-magazin/mobile-app-developing.png" alt="">
                    <p>Разработка мобильных приложений под Android и iOS</p>
                    <div class="arrow_hover">
                        <!-- <img src="/img/internet-magazin/arrowh.png" alt=""> -->
                        <a href="#">Подробнее</a>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 spektr_item">
                    <img src="/img/internet-magazin/paint-palette.png" alt="">
                    <p>Создание логотипа и фирменного </br>стиля</p>
                    <div class="arrow_hover">
                        <!-- <img src="/img/internet-magazin/arrowh.png" alt=""> -->
                        <a href="#">Подробнее</a>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 spektr_item">
                    <img src="/img/internet-magazin/spider-chart.png" alt="">
                    <p>Разработка анимированной видео рекламы (Видеоинфографика)</p>
                    <div class="arrow_hover">
                        <!-- <img src="/img/internet-magazin/arrowh.png" alt=""> -->
                        <a href="#">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 spektr_item">
                    <img src="/img/internet-magazin/online-shopping.png" alt="">
                    <p>Интернет-маркетинг</p>
                    <div class="arrow_hover" >
                        <!-- <img src="/img/internet-magazin/arrowh.png" alt=""> -->
                        <a href="#">Подробнее</a>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 spektr_item">
                    <img src="/img/internet-magazin/market.png" alt="">
                    <p>Создание сайтов любой сложности</p>
                    <div class="arrow_hover">
                        <!-- <img src="/img/internet-magazin/arrowh.png" alt=""> -->
                        <a href="#">Подробнее</a>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 spektr_item">
                    <img src="/img/internet-magazin/technical-support.png" alt="">
                    <p>Техническая поддержка проектов</p>
                    <div class="arrow_hover">
                        <!-- <img src="/img/internet-magazin/arrowh.png" alt=""> -->
                        <a href="#">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


<div class="hidden"></div>

<div class="loader">
    <div class="loader_inner"></div>
</div>

    <style>
        .spektr_descr>h2, .header_slider_p, .plans_descr_h3>h3, .spektr_descr, .plans_descr{
            font-family: 'Open Sans', sans-serif !important;
            font-weight: 700;
        }
        .header_slider_btn{
            font-family: 'Lato', sans-serif;
            font-weight: 900;

        }
        .h1_descr{
            font-family: 'Lato', sans-serif;
            font-weight: 900;

        }
        .about_head_descr_h3 h2, .comand_about>h2{
            font-family: 'Lato', sans-serif !important;
            font-weight: 900 !important;

        }

        .about_head_descr_h2 h2, .tarif_plan_h2_descr>h2 ,.alux_cart_descr>h2{
            font-family: 'Lato', sans-serif !important;
            font-weight: 900 !important;

        }

        .about_include_h2 h2 , .profit_descr>h2, .profit_descr>h3{
            font-family: 'Lato', sans-serif !important;
            font-weight: 900 !important;

		  }

		  @media(max-width: 360px){
			  .comand_about_left{
				padding-bottom: 2rem;
			  }
		  }

    </style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- <script src="/js/js.js"></script>
<script src="/js/vendor/slick.min.js"></script>
<script src="/js/vendor/parallax.min.js"></script>
<script src="/js/vendor/jquery.fancybox.pack.js"></script>
-->

<script src="/js/script.js"></script>
<script src="/js/vendor/sweetalert.min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="/slick.min.js"></script>
<script src="/js/internet-magazin-common.js"></script>

<style>
        @media screen and (max-width: 991px){
            .header_slider .header_slider_items{
                background-image: none !important
            }
        }
        .plans_list{
            padding-top: 45px;
            z-index: 1003;
        }
</style>
@endsection
