@extends('layouts.main')
@section('content')
    <style>
        .top-menu ul li.active{
            width: auto !important;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.theme.green.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css" />
    <link rel="stylesheet" href="css/landstyle.css">
    <div class="new-topbar hidden-md hidden-lg navbar-fixed-top">
        <div></div>
        <a href="/"><img loading="lazy" style="height: 45px; width: auto;" src="/img/logo.png" alt=""></a>
        <div class="right-menu"><img loading="lazy" class="" style="height: 40px; width: auto;" src="/images/green-phone.png" alt=""></div>
    </div>

    <style>
        .footer{
            display: none;
        }
        .new-topbar { display: flex; justify-content: space-between; align-items: center; position:fixed; height:90px; width: 100vw; top: 0;left: 0; background-color: black; z-index:30; padding: 10px 10px 0 4rem; }
        @media only screen and (min-width : 1300px) {
            .new-topbar, .mobile-menu-right { display:none; }
        }
        @media (min-width: 767px) {
            .new-topbar {
                display: none!important;
            }
        }
        @media only screen and (max-width: 767px) {
            .mobile-logo-alux { display: none !important; }
            .main_home_slider_item_btn,
            .item-second,
            .item-first { background: none!important; }
            .main_home_slider_item_bg_new.item-first {
                background-image: url(img/main_slider_img1.jpg)!important;
                width: 300px;
                height: 300px;
                background-size: 250%!important;
                top: 100px;
                background-position: -120px center!important;
                margin: 30px auto -240px;
            }
            .main_home_slider_item_bg_new.item-second {
                background-image: url(img/Magicien.png)!important;
                background-color: #000;
                width: 300px;
                height: 300px;
                background-size: 50%!important;
                top: 100px;
                background-position: -350px center!important;
                margin: 30px auto -170px;
            }
            .main_home_slider_item_title {
                font-size: 20px;
                margin-bottom: 25px;
            }
        }
    </style>


    <div class="mobile-menu-right">
        <p>Наши телефоны:</p>
        <a href="tel:+77273171698">+7 (727) 317-16-98</a>
        <a href="tel:+77054503916">+7 (705) 450-39-16</a>
        <!-- <div class="triangle-tooltip"></div> -->
    </div>
    <link rel="stylesheet" href="css/my-animate.css">



    <div itemscope itemtype="http://schema.org/CreativeWork">
        <meta itemprop="headline" content="Создание Landing Page в Алматы. Разработка крутых лэндинг-пейдж под ключ">
        <meta itemprop="description" content="Создание лендингов под ключ в Алматы. Разработка уникальных Landing Page для любых задач с индивидуальным дизайном не дорого.">
        <div itemprop="author">
            <span itemprop="name" style="display: none;">Иван Востриков</span>
        </div>
        <div style="display: none;" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            <meta itemprop="bestRating" content="5">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="ratingValue" content="5">
            <meta itemprop="ratingCount" content="1">
        </div>
    </div>

    <div class="list-obert">
        <img loading="lazy" src="img/list-1.png" class="list list-1 rellax wow bounceInRight" data-rellax-speed="-9" data-wow-offset="100"alt="">
        <img loading="lazy" src="img/list-2.png" class="list list-2 rellax wow bounceInDown" data-rellax-speed="-9" data-wow-offset="100" alt="">
        <img loading="lazy" src="img/list-3.png" class="list list-3 rellax wow bounceInLeft" data-wow-offset="100" data-rellax-speed="-9" alt="">
    </div>

    <div class="main">


        <div class="owl-carousel owl-theme">
            <div class="main-slide-first main-slide">
                <!-- <button class="modal-open" id="modal-open" onclick="rightOpen()" style="border-radius: 50% 50% 0 0 / 100% 100% 0 0;"><i class="icon-up"></i></button> -->
                <div class="container">

                    <div class="col-lg-6 col-xl-6 col-md-4">

                        <img loading="lazy" src="img/tree-lamp.png" class="tree-lamp wow bounceInUp" data-wow-offset="100">
                    </div>
                    <div class="main-text-block col-lg-6 col-xl-6 col-md-6 wow zoomInDown" data-wow-offset="100">
                        <h1 class="main-capt">
                            Разработка крутых <br>Landing Page
                        </h1>
                        <div class="main-fst-p main-p">
                            <p>
                                Уникальный авторский дизайн и высокая конверсия от ведущей веб-студии Казахстана.
                            </p>
                            <p>
                                Создадим стильный, продающий Landing Page и выведим его на первые строчки в Google и Yandex.
                            </p>
                        </div>
                        <button class="down-arrow"><a href="#tarif"><img loading="lazy" src="img/scroll-arrow.png" alt=""></a></button>

                    </div>
                </div>
            </div>
            <div class="main-slide main-slide-card">
                <div class="container">
                    <div class="col-lg-5 col-md-4">
                        <h2 class="main-capt card-capt fadeInDown wow" data-wow-offset="100">
                            13 лет опыта в IT бизнесе, более 300 клиентов, довольных результатами
                        </h2>
                        <div class="main-card-p main-p fadeInDown wow" data-wow-offset="100">
                            <p>
                                Комплексный подход для максимальной отдачи<br>
                                Лучшее ценовое предложение на создание уникального Landing Page и эффективной рекламной компании в одном пакете.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4">
                        <img loading="lazy" src="img/Magicien.png" class="main-slide-cards" alt="Разработка Landing Page в Алматы">
                    </div>
                </div>
            </div>
            <div class="main-slide main-slide-scd">
                <div class="container">
                    <div class="col-lg-5">
                        <h2 class="main-capt sc-capt">
                            Мы работаем до полного утверждения клиентом
                        </h2>
                        <div class="main-sec-p main-p">
                            <p>
                                Количество вариантов и правок по дизайну не ограничено, мы работаем на результат!
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img loading="lazy" src="img/glasses.png" class="main-glasses" alt="Создание лендингов в Алматы">
                    </div>
                </div>
            </div>
            <div class="main-slide-discount main-slide">
                <div class="container">
                    <div class="col-lg-7 col-lg-offset-0">
                        <img loading="lazy" src="img/arms.png" class="arms">
                    </div>
                    <div class="col-lg-5 sl4-text">
                        <h2>Получите 15.000 тенге в подарок</h2>
                        <p>на рекламу в Yandex.kz от сертифицированной ведущей веб-студии Казахстана ALUX!</p>
                    </div>

                </div>
            </div>
        </div>

        <div class="right-modal">
            <div class="container">
                <div class="right-modal-content col-lg-offset-1 col-lg-11">
                    <div class="right-inside">
                        <div class="col-lg-1">
                            <button class="minus-right" onclick="rightClose()"><i class="icon-right"></i></button>
                        </div>
                        <div class="col-lg-8">
                            <img loading="lazy" src="img/tree.png" class="tree-img" alt="">
                            <div class="main-text-block col-lg-11 col-xl-11 col-md-11">
                                <h2 class="main-capt right-modal-capt">
                                    This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh
                                </h2>
                                <div class="right-modal-p">
                                    <p>
                                        This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                    </p>
                                </div>
                                <div class="right-modal-buttons">
                                    <button class="multiland">мультилендинг</button>
                                    <button class="tarif-right">тарифные планы</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="tarif">
        <div class="container">
        <div class="row">

        <div class="col-lg-4"><h2 class="text-center wow rubberBand" data-wow-offset="100">Тарифные планы</h2> </div>
        <div class="col-lg-8"><p class="tarif-p">Команда веб-студии ALUX разработала совершенно новый продукт на рынке интернет-маркетинга:</p> </div>
        </div>
            
            
            <div class="tarifs">
                <div class="row">
                    <div class="tarifs-block tb1 col-lg-4 col-xl-4 col-md-4 bounceInLeft wow" data-wow-offset="100">
                        <div class="tarifs-all">
                            <div class="tarifs-capt-block">
                                <h3 class="tarifs-capt">Быстрый Старт</h3>
                            </div>
                            <p class="tarifs-text">
                                Оптимальный выбор, если нужно в кратчайший срок заявить о себе и получать заявки от клиентов с первого дня работы сайта.
                            </p>
                            <img loading="lazy" src="img/question.png" alt="" class="quest-img">
                            <ul class="tarifs-plan">
                                <li>Создание уникального продуманного продающего дизайна</li>
                                <li>Анимация на сайте</li>
                                <li>Адаптивная верстка - Мобильная версия</li>
                                <li>Рекламная компания</li>
                                <li>Анализ и прозвон конкурентов</li>
                            </ul>
                            <div class="tarifs-cena">
                                <span class="tarif-cena-text">250.000<img loading="lazy" class="tarif-tenge" src="img/white-tenge.png" alt=""></span><span class="break-line">|</span><span class="moment">5 дней с момента <br>утверждения дизайна</span>
                            </div>
                        </div>
                        <button class="order-btn tarif-btn">заказать</button>
                    </div>
                    <div class="tarifs-block tb2 col-lg-4 col-lg-4 col-xl-4 col-md-4 bounceInDown wow" data-wow-offset="100">
                        <div class="tarifs-all">
                            <div class="tarifs-capt-block">
                                <h3 class="tarifs-capt">Мультилендинг</h3>
                            </div>
                            <p class="tarifs-text">
                                Увеличьте продажи в 2 раза за 30 дней  с помощью нашей технологии мультилендинга и контекстной<br> рекламы
                            </p>
                            <img loading="lazy" src="img/question.png" alt="" class="quest-img">
                            <ul class="tarifs-plan">
                                <li>Создание уникального продуманного продающего дизайна</li>
                                <li>
                                    Данный пакет включает в себя все, что включает  "Быстрый старт" +  удобная система управления сайтом + уникальный скрипт продаж, разработанный нашей веб-студией.<br>
                                    Этот инструмент позволяет значительно повысить показатели конверсии за счет адаптации контента для удобства посетителей.
                                </li>
                            </ul>
                            <div class="tarifs-cena">
                                <span class="tarif-cena-text">350.000<img loading="lazy" class="tarif-tenge" src="img/white-tenge.png" alt=""></span><span class="break-line">|</span><span class="moment">10 дней с момента <br>утверждения дизайна</span>
                            </div>
                        </div>
                        <div class="tarif-btn-block">
                            <button class="tarif-btn-1 tarif-btn"><a href="#multiland" class="more-href">подробнее</a></button>
                            <button class="order-btn tarif-btn">заказать</button>
                        </div>
                    </div>
                    <div class="tarifs-block tb3 col-lg-4 col-lg-4 col-xl-4 col-md-4 bounceInRight wow" data-wow-offset="100">
                        <img loading="lazy" src="img/butterfly.png" class="butterfly" alt="">
                        <div class="tarifs-all">
                            <div class="tarifs-capt-block">

                                <h3 class="tarifs-capt">TOP SALES</h3>
                            </div>
                            <p class="tarifs-text">
                                Полноценная система <br>привлечения клиентов под ключ с алгоритмами мультилендинга и видеоинфографикой.
                            </p>
                            <img loading="lazy" src="img/question.png" alt="" class="quest-img">
                            <ul class="tarifs-plan">
                                <li>Создание уникального продуманного продающего дизайна</li>
                                <li>Разработка видеоинфографики, повышающей конверсию сайта на 40 % (длительность ролика 60 секунд)</li>
                                <li>Адаптивный *Скрипт продаж* мультилендинг</li>
                                <li>Высокоэффективная профессионально настроенная рекламная компания в Google и Yandex</li>
                            </ul>
                            <div class="tarifs-cena">
                                <span class="tarif-cena-text">500.000<img loading="lazy" class="tarif-tenge" src="img/white-tenge.png" alt=""></span><span class="break-line">|</span><span class="moment">30 дней с момента <br>утверждения дизайна</span>
                            </div>
                        </div>
                        <div class="tarif-btn-block">
                            <button class="tarif-btn-1 tarif-btn"><a href="#top-sales" class="more-href">подробнее</a></button>
                            <button class="order-btn tarif-btn">заказать</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="/js/slick/slick.css">
    <link rel="stylesheet" href="/js/slick/slick-theme.css">
    <style>
        .portfolio-desc{
            height: 110px;
        }
    </style>
    <script src="/js/slick/slick.min.js"></script>
    <div class="parallax-window" data-parallax="scroll" data-image-src="img/portfolio-bg.png" id="portfolio" data-position-y="-160px">
        <div class="container portfolio-cont">
            <img loading="lazy" src="img/paint.png" class="paint rellax" data-rellax-speed="6" alt="">
            <h2 class="text-center wow rubberBand" data-wow-offset="100">Мы разработали сайты для:</h2>
            <div class="row portfolio-row">
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInUp wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/oil.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>"Liqui Moly EURASIA" Интернет магазин розничной торговли.</p>
                    </div>
                </div>
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInDown wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/aziakredit.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>"Asia Credit Bank" Разработка интернет банкинга для юр. лиц.</p>
                    </div>
                </div>
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInUp wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/vtb.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>"ВТБ Банк Казахстан" Разработка корпоративного веб сайта.</p>
                    </div>
                </div>
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInDown wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/lg.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>"LG" Создание Лендинга в честь 21 летнего юбилея компаниии в Казахстане.</p>
                    </div>
                </div>
            </div>
            <div class="row sec-row">
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInDown wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/sberbank.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>ДБ АО "Сбербанк" Разработка промо сайта для VIP клиентов "Сбербанк1"</p>
                    </div>
                </div>
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInUp wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/ipoint.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>"Ipoint" Разработка продающего интернет магазина</p>
                    </div>
                </div>
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInDown wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/asialife.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>"ASIA LIFE" Разработка корпоративного веб сайта для Страховой Компании.</p>
                    </div>
                </div>
                <div class="potfolio-block col-lg-3 col-xl-3 col-md-3 fadeInUp wow" data-wow-offset="100">
                    <div class="portfolio-img-block">
                        <img loading="lazy" src="img/homebank.png" class="portfolio-img">
                    </div>
                    <div class="portfolio-desc">
                        <p>"Home Bank" Разработка посадочной страницы для мнговенных переводов с карты на карту</p>
                    </div>
                </div>
            </div>

            <button class="other-works btn-hollow order-btn">
                <a href="/portfolio.html">смотреть все работы</a>
            </button>
        </div>
    </div>
    <div class="multi" id="multiland">
        <div class="container">
            <p class="paket flipInY wow" data-wow-offset="100">Пакет *Мультилендинг* 250 000 тенге</p>
            <div class="row">
                <div class="col-lg-6 col-md-6 slideInUp wow" data-wow-offset="100">
                    <h2>Что такое «Мультилендинг» и почему эта технология увеличивает количество заявок в 2-5 раз?</h2>
                    <p class="user-p">
                        Посетитель попадает на ваш лендинг по запросу с рекламы, а страница, как хамелеон, автоматически подменяет содержание под его запрос, как следствие растет количество заявок.
                    </p>
                </div>
                <div class="col-lg-6 col-md-6">
                    <img loading="lazy" src="img/green-arrow.png" class="big-green-arrow" alt="">
                </div>
            </div>
            <p class="how-work wow flipInX" data-wow-offset="100">Как работает Мультилендинг? Выберите поисковый запрос</p>
            <select name="multiChoice" id="multiChoice" onchange="choiceFun()" class="flipInX wow" data-wow-offset="100">
                <option value="0">Рыбалка в Алматы</option>
                <option value="1">Охота в Алматы</option>
                <option value="2">Подводная рыбалка в Алматы</option>
            </select>
            <img loading="lazy" src="img/rybalka.jpg" id="img0" class="choice-img lightSpeedIn wow" data-wow-offset="100">
            <img loading="lazy" src="img/ohota.jpg" id="img1" class="choice-img lightSpeedIn wow" data-wow-offset="100" alt="">
            <img loading="lazy" src="img/podvodnaya.jpg" id="img2" class="choice-img lightSpeedIn wow" data-wow-offset="100" alt="">
        </div>
    </div>
    <div class="form-all">
        <div class="container">
            <div class="col-lg-6 col-xl-6 col-md-6">
                <h2>Это означает, что:</h2>
                <div class="meaning">
                    <div class="meaning-block">
                        <img loading="lazy" src="img/search.png" class="icon-at-all fadeInLeft wow" data-wow-offset="100" alt="">
                        <p class="fadeInRight wow" data-wow-offset="100">Посетитель внимательно изучает ваше предложение - ведь он нашел именно то, что искал</p>
                    </div>
                    <div class="meaning-block">
                        <img loading="lazy" src="img/thumbs-up.png" class="icon-at-all fadeInLeft wow" data-wow-offset="100" alt="">
                        <p class="fadeInRight wow" data-wow-offset="100">Для разных поисковых запросов мы расставляем акценты на самые актуальные выгоды вашего предложения</p>
                    </div>
                    <div class="meaning-block">
                        <img loading="lazy" src="img/growth.png" class="icon-at-all fadeInLeft wow" data-wow-offset="100"alt="">
                        <p class="fadeInRight wow" data-wow-offset="100">Вероятность заказа и конверсия увеличиваются</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 col-md-6 form-cols jackInTheBox wow" data-wow-offset="100">

                <div class="form-wrap">
                    <div class="form">
                        <p class="form-capt">Заявка на консультацию</p>
                        <p class="form-desc">
                            Если у Вас остались вопросы либо Вы хотите получить консультацию, напишите нам, пожалуйста:)
                        </p>
                        <div class="form-itself">
                            <input type="text" placeholder="Ваше имя" class="form-input itself-text"></input>
                            <input type="phone" placeholder="Ваш телефон" class="form-input itself-phone"></input>
                            <div class="agreement">
                                <input type="checkbox" id="conf"><label for="conf" class="arge-label"><span>Согласие на обработку персональных данных</span></label>
                            </div>
                            <button class="send itself-btn">отправить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info" id="top-sales">
        <div class="container">
            <div class="col-lg-7">
                <img loading="lazy" src="img/buble-1.png" class="buble buble-1 rellax" data-rellax-speed="1" alt="">
                <img loading="lazy" src="img/buble-2.png" class="buble buble-2 rellax" data-rellax-speed="2" alt="">
                <img loading="lazy" src="img/buble-3.png" class="buble buble-3 rellax" data-rellax-speed="-2" alt="">
                <img loading="lazy" src="img/buble-4.png" class="buble buble-4 rellax" data-rellax-speed="-5" alt="">
                <img loading="lazy" src="img/butterfly.png" class="sales-btf fadeInRightBig wow" data-wow-offset="500" alt="">
                <h2 class="bounceInDown wow" data-wow-offset="100">Top Sales 480 000 тенге Мультилендинг + видеоинфографика</h2>
                <div class="main-p info-p">
                    <p class="bounceInDown wow" data-wow-offset="100">
                        Во всем мире с недавних пор видео инфографика широко используется на лендингах и интернет магазинов или любых других компаний, чтобы объяснить процесс продажи или производства товаров, процесс доставки или обслуживания клиентов.
                    </p>
                    <p class="bounceInDown wow" data-wow-offset="100">
                        Одним из самых важных преимуществ инфографики перед другими видами анимации - это ее низкая стоимость. Для сравнения: 1 минута инфографики стоит от 280.000 тенге. Заказывая данную услугу в пакете с мультилендингом Вы получаете скидку на видео инфографику  50 % и повышаете конверсию своего сайта до 20 %.
                    </p>
                    <p class="bounceInDown wow" data-wow-offset="100">
                        Видеомаркетинг должен стать обязательной частью рекламной стратегии, и мы собрали подтверждения этому в новой инфографике.
                    </p>
                </div>
                <div class="percent percent-1 bounceInDown wow" data-wow-offset="100">
                    <img loading="lazy" src="img/60percent.png" alt="">
                    <span>Конверсия Landing Page с минутной видео-инфографикой  на 60 % больше чем без нее - Доказано !</span>
                </div>
                <div class="percent percent-1 bounceInDown wow" data-wow-offset="100">
                    <img loading="lazy" src="img/69percent.png" alt="">
                    <span>69% пользователей смартфонов принимают решение после просмотра рекламных видео</span>
                </div>
                <div class="percent percent-1 bounceInDown wow" data-wow-offset="100">
                    <img loading="lazy" src="img/74percent.png" alt="">
                    <span>На долю видео в 2017 году приходится 74% всего трафика</span>
                </div>
            </div>
            <div class="col-lg-5 info-right-side">
                <img loading="lazy" src="img/lime.png" alt="" class="lime">
                <button class="question order-btn">перезвонить вам?</button>
            </div>
        </div>
    </div>
    <div class="parallax-window" data-parallax="scroll" data-image-src="img/video-bg.png" id="video">
        <div class="container video-cont">
            <h2>Примеры видеоинфографики, созданной нашей студией:</h2>
            <div class="youtube">
                <div class="row">
                    <iframe loading="lazy" width="475" height="280" style="
    position: relative;
    z-index: 9;
    margin: 0 auto;
    display: block;" src="https://www.youtube.com/embed/C8NvXK_r1R4?rel=0&amp;showinfo=0" class="fadeInLeft wow" data-wow-offset="100" frameborder="0" allowfullscreen=""></iframe>
                    <iframe loading="lazy" width="475" height="280" style="
    position: relative;
    z-index: 9;
    margin: 0 auto;
    display: block;" src="https://www.youtube.com/embed/WALM5sFeFXA?rel=0&amp;showinfo=0" frameborder="0" class="fadeInDown wow" data-wow-offset="100" allowfullscreen=""></iframe>
                    <iframe loading="lazy" width="475" height="280" style="
    position: relative;
    z-index: 9;
    margin: 0 auto;
    display: block;" src="https://www.youtube.com/embed/O5YtKQ2h3Rs?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen="" class="fadeInRight wow" data-wow-offset="100"></iframe>
                </div>
                <div class="row">
                    <iframe loading="lazy" width="475" height="280" style="
    position: relative;
    z-index: 9;
    margin: 0 auto;
    display: block;" src="https://www.youtube.com/embed/4p3QyLEEfh0?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen="" class="fadeInLeft wow" data-wow-offset="100"></iframe>
                    <iframe loading="lazy" width="475" height="280" style="
    position: relative;
    z-index: 9;
    margin: 0 auto;
    display: block;" src="https://www.youtube.com/embed/hhBLyPhvn7o?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen="" class="fadeInUp wow" data-wow-offset="100"></iframe>
                    <iframe loading="lazy" width="475" height="280" style="
    position: relative;
    z-index: 9;
    margin: 0 auto;
    display: block;" src="https://www.youtube.com/embed/4DezPr7o2BY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen="" class="fadeInRight wow" data-wow-offset="100"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="array">
        <div class="container">
            <h2>Как мы будем создавать очередь<br> из клиентов?</h2>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="array-img-block">
                        <img loading="lazy" src="img/Technology.png" alt="">
                    </div>
                    <h3>Сбор данных и анализ Вашего бизнеса</h3>
                    <p class="array-p">Перед началом разработки наши специалисты соберут цены,условия преимущества Ваших 5 главных конкурентов,проанализируют их вместе с Вами и предложут оригинальное концептуальное маркетинговое решение в виде уникального,продающего Лендинга.</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="array-img-block">
                        <img loading="lazy" src="img/building.png" alt="">
                    </div>
                    <h3>Разработка стратегии продвижения</h3>
                    <p class="array-p">После проведения детального анализа Вашего бизнеса, опытные маркетологи продумывают максимально эффективный метод продвижения Вашего сайта, для того чтобы Ваш сайт приносил Вам прибыль.</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="array-img-block">
                        <img loading="lazy" src="img/branding.png" alt="">
                    </div>
                    <h3>Запаковка Вашего бизнеса</h3>
                    <p class="array-p">После того, как все цели и задачи определены, мы приступаем к аккумулированию всех ранее произведенных действия для достижения желаемого результата.</p>
                </div>
            </div>
            <div class="row sec-row">
                <div class="col-lg-4 col-md-4">
                    <div class="array-img-block">
                        <img loading="lazy" src="img/search-for.png" alt="">
                    </div>
                    <h3>Изучение конкурентоспособной среды.</h3>
                    <p class="array-p">Прежде чем приступить к разработке дизайна необходимо изучить аналоги будущего веб-сайта, и выделить  их основные плюсы и минусы.</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="array-img-block">
                        <img loading="lazy" src="img/Analysis.png" alt="">
                    </div>
                    <h3>Процесс разработки.</h3>
                    <p class="array-p">Процесс разработки веб-сайта проходит в три этапа: разработка дизайна, верстка и программирование. На каждом этапе проводится детальный контроль и тестирование.</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="array-img-block">
                        <img loading="lazy" src="img/target.png" class="target" alt="">
                    </div>
                    <h3>Запуск Вашего лендинга!</h3>
                    <p class="array-p">После того, как Ваш сайт прошел все три этапа разработки и готов к использованию, необходимо произвести настроку контекстной рекламы, которая позволит продвигать Ваш сайт в таких поисковых системах как Яндекс и Google.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="as-result">
        <div class="container">
            <div class="col-lg-6 col-md-7">
                <h2>В результате Вы получаете:</h2>
                <div class="circle">
                    <div class="circle-left circle-side">
                        <div class="circle-left-top">
                            <p class="p73">73%</p>
                            <p class="t73 circle-text">
                                Страницу с<br> конверсией<br> до 73%*
                            </p>
                        </div>
                        <div class="circle-left-down">
                            <p class="t102 circle-text">До 102 заявок<br> за один день*</p>
                            <p class="record circle-text">* наш рекорд</p>
                            <p class="p102">102</p>
                        </div>
                    </div>
                    <div class="circle-right circle-side">
                        <div class="circle-right-top">
                            <p class="p200">200%</p>
                            <p class="t200 circle-text">
                                Увеличение<br> прибыли в 2 и<br> более раза
                            </p>
                        </div>
                        <div class="circle-right-down">
                            <p class="t30 circle-text">Дней<br> бесплатного<br> гарантийного<br> обслуживания</p>
                            <p class="p30">30</p>
                        </div>
                    </div>
                    <div class="circle-center">
                        <p>
                            Команда студии<br> «ALUX»<br> Преданные фанаты своего дела сделают работу на профессиональном уровне для решения ваших задач
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-5">
                <div class="result-p">
                    <p>98% веб-студий забудут про Вас после сдачи страницы.</p>
                    <p>Но разработка страницы - это лишь 50% успеха. Еще 50% это ведение и улучшение на основе рекламы и аналитики</p>
                    <div class="green-p">
                        <p>
                            Поэтому мы ведем Ваш проект 6 месяцев бесплатно после сдачи основной работы<br>
                            Команда студии «Алматы Люкс»<br>  Преданные фанаты своего дела сделают работу на профессиональном уровне для решения ваших задач.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div id="bottom" class="parallax-window" data-parallax="scroll" data-image-src="img/bottom-bg.png" id="bottom">
        <div class="container bottom-cont">
            <div class="row bottom-row">
                <div class="col-lg-3 col-md-6">
                    <img loading="lazy" src="img/arm.png" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="contacts">
                        <p class="phone">
                            <a href="tel:+77273171698" class="phone-number">+7 (727) 317-16-98</a>
                            <a href="tel:+77054503916" class="phone-number">+7 705 450 39 16</a>
                        </p>
                        <p class="where">
                            Республика Казахстан, г.Алматы Ул. Шевченко 165б,5 этаж, офис 511
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <h3>Договоритесь с нами о встрече, чтобы обсудить перспективы роста и развития вашего бизнеса!</h3>
                    <div class="bottom-form-bg">
                    </div>
                    <div class="form-itself btm-form">
                        <input type="text" placeholder="Укажите Ваше имя" class="form-input itself2-text"></input>
                        <input type="phone" placeholder="Укажите Ваш телефон" class="form-input itself2-phone"></input>
                        <button class="send itself2-btn">отправить</button>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="icons-anchor">
                    <a href="#"><i class="icon-face"></i></a>
                    <a href="#"><i class="icon-vk"></i></a>
                    <a href="#"><i class="icon-insta"></i></a>
                </div>
                <div class="bf-text">
                    <p>COPYRIGHT © 2007-2020 ALMATY LUXURY WEB DESIGN STUDIO</p>
                    <p>Создаём и продвигаем эффективные веб сайты в Алматы и других городах Казахстана уже более 13 лет. карта сайта / sitemap</p>
                </div>
            </div>
        </div>
    </div>
	 </div>
	 
	 <content class="mob-view">
            <input id="hamburger" class="hamburger" type="checkbox">
            <label class="hamburger" for="hamburger">
                <i></i>
                <text style="display:none;">
                    <close>закрыть</close>
                    <open>меню</open>
                </text>
            </label>
            <section class="drawer-list">
                <ul><li>
                        <a data-id="1" href="index.html">Главная</a></li><li>
                        <a data-id="2" href="about.html">О нас</a></li><li class="dropdown"><label for="dropdown">
                            <a data-id="3">Наши услуги</a>
                        </label><input type="checkbox" id="dropdown"/>
                        <ul>
                            <li><a href="internetmagazin.html">Интернет магазин1</a></li>
                            <li><a href="landingpage.html">Лендинг пейдж</a></li>
                            <li><a href="korporativniysite.html">Корпоративный сайт</a></li>
                            <li><a href="razrabotkabrandbook.html">Фирменный стиль</a></li>
                        </ul>
                        <div></div></li><li>
                        <a data-id="4" href="portfolio.html">Наши работы</a></li><li>
                        <a data-id="5" href="otzyvy.html">Отзывы</a></li><li>
                        <a data-id="6" href="contacts.html">Контакты</a></li>
                    <li style="padding:20px;"><a style="font-size:18px;border-color:#fff;" class="btn-hollow order-btn" href="#">заказать услугу</a></li>
                </ul>
            </section>
        </content>

      
    <style>
        .as-result .circle-left-top{
            height: 220px !important;
        }
        .as-result .circle .p73{
            padding-top: 0;
            margin-bottom: 0px;
            margin-top: 40px;
        }
        .as-result .circle .t73{
            margin-top: 30px;
        }
        .owl-carousel .owl-dots .owl-dot{
            padding-top: 7px;
        }
        .owl-theme .owl-dots .owl-dot::after{
            background-color: transparent;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script async src="js/parallax.min.js"></script>
    <script src="js/vendor/jquery-3.1.1.min.js"></script>
    <script src="/js/vendor/slick.min.js"></script>
    <script src="/js/vendor/parallax.min.js"></script>

    <script async src="js/rellax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/owl.carousel.min.js"></script>
    <script src="/js/landscript.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js">
    </script>
    <script>
        var rellax = new Rellax('rellax');

    </script>
    <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=+77054503916&text=Здравствуйте, меня интересует разработка веб сайта."></a>
        <div class="circlephone" style="transform-origin: center;"></div>
        <div class="circle-fill" style="transform-origin: center;"></div>
    </div>
    <style>
        .whatsapp { background:url(/images/whatsapp.png) center center no-repeat; background-size: 100%; width: 50px; height: 50px; position: fixed; bottom: 10px; right: 10px;
            z-index: 999; }
        .whatsapp a { display: block; width: 100%; height: 100%;
            position: relative;
            z-index: 999; }
        .circlephone{box-sizing:content-box;-webkit-box-sizing:content-box;border: 2px solid #44bb6e;width:80px;height:80px;bottom:-15px;left:-15px;position:absolute;-webkit-border-radius:100%;-moz-border-radius: 100%;border-radius: 100%;opacity: .5;-webkit-animation: circle-anim 2.4s infinite ease-in-out !important;-moz-animation: circle-anim 2.4s infinite ease-in-out !important;-ms-animation: circle-anim 2.4s infinite ease-in-out !important;-o-animation: circle-anim 2.4s infinite ease-in-out !important;animation: circle-anim 2.4s infinite ease-in-out !important;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
        .circle-fill{box-sizing:content-box;-webkit-box-sizing:content-box;background-color:#44bb6e;width:50px;height:50px;bottom:-2px;left:-2px;position:absolute;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;border: 2px solid transparent;-webkit-animation: circle-fill-anim 2.3s infinite ease-in-out;-moz-animation: circle-fill-anim 2.3s infinite ease-in-out;-ms-animation: circle-fill-anim 2.3s infinite ease-in-out;-o-animation: circle-fill-anim 2.3s infinite ease-in-out;animation: circle-fill-anim 2.3s infinite ease-in-out;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
        @keyframes pulse {0% {transform: scale(0.9);opacity: 1;}
            50% {transform: scale(1); opacity: 1; }
            100% {transform: scale(0.9);opacity: 1;}}
        @-webkit-keyframes pulse {0% {-webkit-transform: scale(0.95);opacity: 1;}
            50% {-webkit-transform: scale(1);opacity: 1;}
            100% {-webkit-transform: scale(0.95);opacity: 1;}}
        @keyframes tossing {
            0% {transform: rotate(-8deg);}
            50% {transform: rotate(8deg);}
            100% {transform: rotate(-8deg);}}
        @-webkit-keyframes tossing {
            0% {-webkit-transform: rotate(-8deg);}
            50% {-webkit-transform: rotate(8deg);}
            100% {-webkit-transform: rotate(-8deg);}}
        @-moz-keyframes circle-anim {
            0% {-moz-transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;-moz-opacity: .1;-webkit-opacity: .1;-o-opacity: .1;}
            30% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;-moz-opacity: .5;-webkit-opacity: .5;-o-opacity: .5;}
            100% {-moz-transform: rotate(0deg) scale(1) skew(1deg);opacity: .6;-moz-opacity: .6;-webkit-opacity: .6;-o-opacity: .1;}}
        @-webkit-keyframes circle-anim {
            0% {-webkit-transform: rotate(0deg) scale(0.5) skew(1deg);-webkit-opacity: .1;}
            30% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);-webkit-opacity: .5;}
            100% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);-webkit-opacity: .1;}}
        @-o-keyframes circle-anim {
            0% {-o-transform: rotate(0deg) kscale(0.5) skew(1deg);-o-opacity: .1;}
            30% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);-o-opacity: .5;}
            100% {-o-transform: rotate(0deg) scale(1) skew(1deg);-o-opacity: .1;}}
        @keyframes circle-anim {
            0% {transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;}
            30% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;}
            100% {transform: rotate(0deg) scale(1) skew(1deg);
                opacity: .1;}}
        @-moz-keyframes circle-fill-anim {
            0% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {-moz-transform: rotate(0deg) -moz-scale(1) skew(1deg);opacity: .2;}
            100% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @-webkit-keyframes circle-fill-anim {
            0% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;  }
            50% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;  }
            100% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @-o-keyframes circle-fill-anim {
            0% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {-o-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
            100% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @keyframes circle-fill-anim {
            0% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
            100% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
    </style>
    <style>




@endsection
