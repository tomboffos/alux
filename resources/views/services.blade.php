@extends('layouts.main')
@section('content')
  <section id="one_sec_services">
    <div class="container-fluid">
        <div id="carouselExampleIndicators" class="carousel slide slider_s_services" data-ride="carousel">
            <ol class="carousel-indicators " id="pagination_pos">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                       <div class="col-lg-2"></div>
                        <div class="col-lg-4 slider_pt_services">
                            <h3 class="main_home_slider_item_title">
                                Создание уникальных <span class="color_blue">корпоративных</span>
                                <br> веб-сайтов в Казахстане
                                </h3>
                            <p class="main_home_slider_item_desc "> 
                                На протяжении 13 лет наша веб-студия успешно разрабатывает и продвигает уникальные веб сайты класса "LUX" 
                            </p>
                        </div>
                        <div class="col-lg-1"></div>
                         <div class="col-lg-5">
                            <img src="img/slide_1.png" class="d-block w-100" alt="...">
                        </div>
                    </div>
                </div>
                
                 <div class="carousel-item">
                    <div class="row">
                       <div class="col-lg-2"></div>
                        <div class="col-lg-4 slider_pt_services">
                            <h3 class="main_home_slider_item_title">
                                Создание уникальных <span class="color_blue">корпоративных</span>
                                <br> веб-сайтов в Казахстане
                                </h3>
                            <p class="main_home_slider_item_desc "> 
                                На протяжении 13 лет наша веб-студия успешно разрабатывает и продвигает уникальные веб сайты класса "LUX" 
                            </p>
                        </div>
                        <div class="col-lg-1"></div>
                         <div class="col-lg-5">
                            <img src="img/slide_1.png" class="d-block w-100" alt="...">
                        </div>
                    </div>
                </div>
                
                <div class="carousel-item">
                    <div class="row">
                       <div class="col-lg-2"></div>
                        <div class="col-lg-4 slider_pt_services">
                            <h3 class="main_home_slider_item_title">
                                Создание уникальных <span class="color_blue">корпоративных</span>
                                <br> веб-сайтов в Казахстане
                                </h3>
                            <p class="main_home_slider_item_desc "> 
                                На протяжении 13 лет наша веб-студия успешно разрабатывает и продвигает уникальные веб сайты класса "LUX" 
                            </p>
                        </div>
                        <div class="col-lg-1"></div>
                         <div class="col-lg-5">
                            <img src="img/slide_1.png" class="d-block w-100" alt="...">
                        </div>
                    </div>
                </div>
                
                <div class="carousel-item">
                    <div class="row">
                       <div class="col-lg-2"></div>
                        <div class="col-lg-4 slider_pt_services">
                            <h3 class="main_home_slider_item_title">
                                Создание уникальных <span class="color_blue">корпоративных</span>
                                <br> веб-сайтов в Казахстане
                                </h3>
                            <p class="main_home_slider_item_desc "> 
                                На протяжении 13 лет наша веб-студия успешно разрабатывает и продвигает уникальные веб сайты класса "LUX" 
                            </p>
                        </div>
                        <div class="col-lg-1"></div>
                         <div class="col-lg-5">
                            <img src="img/slide_1.png" class="d-block w-100" alt="...">
                        </div>
                    </div>
                </div>
           


            </div>
        </div>
    </div>
</section>
<!--
<section class="second_screen">
    <div class="container">
        <div class="d-flex nav_item_bar">
            <div class="offer_text">
                Посмотрите наши свежие <br> кейсы в различных сферах
            </div>
            <div class="divider">

            </div>
            <div class="content_menu">
                <h5 class="">
                    <a href="{{route('Projects')}}">
                        Все сферы
                    </a>
                </h5>
                <span>

                </span>
                <h5 class="">
                    <a href="{{route('WebProjects')}}">
                        Web
                    </a>
                </h5>
                <span>

                </span>
                <h5>

                    <a href="{{route('MobileProjects')}}">
                        Mobile
                    </a>
                </h5>
                <span>

                </span>
                <h5>
                    <a href="{{route('BrandingProjects')}}">
                        Branding
                    </a>
                </h5>

                <span>

                </span>
                <h5>
                    <a href="{{route('BrandingProjects')}}">
                        SEO/SMM
                    </a>
                </h5>
                <span>

                </span>
                <h5>
                    <a href="{{route('BrandingProjects')}}">
                        SRM
                    </a>
                </h5>


            </div>
        </div>


    </div>
</section>
-->
<section class="third_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="{{asset('images/setting.svg')}}" alt="">
                    <div></div>
                </div>
                <h5>Разработка веб сайтов</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="{{asset('images/cubes.svg')}}" alt="">
                    <div></div>
                </div>
                <h5>Разработка CMS,CRM систем любой сложности</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="{{asset('images/cube.svg')}}" alt="">
                    <div></div>
                </div>
                <h5>Разработка приложений и игр для мобильных устройств</h5>
                <button>Заказать</button>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="{{asset('images/art.svg')}}" alt="">
                    <div></div>
                </div>
                <h5>Разработка брендбука, фирменного стиля, полиграфический дизайн</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="{{asset('images/tools.svg')}}" alt="">
                    <div></div>
                </div>
                <h5>Техническая поддержка</h5>
                <button>Заказать</button>
            </div>
            <div class="col-lg-4 col-md-6 inner">
                <div class="image_block"><img src="{{asset('images/brain.svg')}}" alt="">
                    <div></div>
                </div>
                <h5>SEO, SMM, SERM, контекстная реклама</h5>

                <button>Заказать</button>
            </div>
        </div>
    </div>

</section>
<section class="fourth_screen">
    <div class="container">
        <div class="row stylized">
            <div class="col-lg-6 size-images-about">
                <img src="/img/about-images.jpg" alt="">
            </div>
            <div class="col-lg-6 third_text">
                <h3>
                    Только <span> современные технологии </span> и <br> проекты любого уровня сложности.
                </h3>
                <p>
                    Ваш сайт будет адаптирован под все существующие устройства. Для управления будет использована наша
                    собственная, интуитивно понятная и в то же время функциональная система управления сайтами "LUX
                    CMS",которая вам позволит управлять и получать новые заказы с вашего сайта как через ПК так через
                    мобильный телефон. Мы в состоянии оперативно разработать проект любой сложности под ключ. Беремся за
                    те работы, от которых другие отказались.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="sixth_screen">
    <div class="container">
        {{-- <div class="row">
            <div class="col-lg-6 actions">
                <div class="icon_field">
                    <img src="{{asset('images/arrow.svg')}}" alt="">
                </div>
                <h3>
                    Работаем <span> до полного утверждения </span> Заказчиком
                </h3>
                <p>
                    Заказав разработку веб сайта,логотипа или мобильного приложения у нас Вы можете быть уверены,что
                    <span> 100 % достигните нужного результата.</span> Мы работаем до Вашего полного утверждения и
                    готовы удовлетворить даже самых требовательных клиентов
                </p>
            </div> --}}
            <div class="sixth_screen_row">
                <div class="sixth_screen_column">
                    <div class="icon_field">
                        <img src="{{asset('images/arrow.svg')}}" alt="">
                    </div>
                    <h3>
                        Работаем <span> до полного утверждения </span> Заказчиком
                    </h3>
                    <p>
                        Заказав разработку веб сайта,логотипа или мобильного приложения у нас Вы можете быть уверены,что
                        <span> 100 % достигните нужного результата.</span> Мы работаем до Вашего полного утверждения и
                        готовы удовлетворить даже самых требовательных клиентов
                    </p>
                </div>
                <div class="sixth_screen_column">
                    <div class="icon_field">
                        <img src="{{asset('images/frame.svg')}}" alt="">
                    </div>
                    <h3>
                        <span>Бесплатная</span> техническая поддержка
                    </h3>
                    <p>
                        Мы предоставляем бесплатную техническую поддержку сроком на <span> 6 месяцев </span> на все веб
                        сайты разработанные нашей веб студией. Автоматическое бекапирование данных, антивирусная система и
                        еще много приятных бонусов, которые позволят Вашему веб сайту всегда находиться в боевом состоянии и
                        быть доступным для Ваших клиентов 24/7
                    </p>
                </div>
            </div>
            {{-- <div class="col-lg-6 actions">
                <div class="icon_field">
                    <img src="{{asset('images/frame.svg')}}" alt="">
                </div>
                <h3>
                    <span>Бесплатная</span> техническая поддержка
                </h3>
                <p>
                    Мы предоставляем бесплатную техническую поддержку сроком на <span> 6 месяцев </span> на все веб
                    сайты разработанные нашей веб студией. Автоматическое бекапирование данных, антивирусная система и
                    еще много приятных бонусов, которые позволят Вашему веб сайту всегда находиться в боевом состоянии и
                    быть доступным для Ваших клиентов 24/7
                </p>
            </div> --}}
            <div class="col-lg-12">
                <hr>
            </div>
        </div>
    </div>
</section>
<section class="seventh_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 title_seven">
                <h1>
                    LUX<span>CMS</span>
                </h1>
            </div>
            <div class="col-lg-6 text_seven">
                <p>
                    Модуль создания и редактирования <br> разделов и подразделов сайта <br> <span> (Неограниченное
                        количество страниц любой вложенности)</span>
                </p>
            </div>
        </div>
    </div>
</section>
<div class="eighth_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="d-flex pointer">
                    <div class="props_guide d-flex"><img src="{{asset('images/vector.svg')}}" alt="">
                        <p>
                            Возможность управления структурой сайта <br> "в один клик"
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="{{asset('images/image.svg')}}" alt="">

                        <p>
                            Работа с графикой <br> (автоматическое масштабирование <br> изображений)
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="{{asset('images/loop.svg')}}" alt="">
                        <p>Поиск по сайту</p>
                    </div>
                    <div class="props_guide d-flex"><img src="{{asset('images/card.svg')}}" alt="">
                        <p>
                            Форма обратной связи <br> (неограниченное количество)
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="{{asset('images/figures.svg')}}" alt="">
                        <p>
                            Модуль новостей с выведением всех новостей списком и постраничного выведения каждой новости
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="{{asset('images/loop_second.svg')}}" alt="">
                        <p>
                            Статистика посещений, курсы валют,погода,время
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="{{asset('images/document.svg')}}" alt="">
                        <p>
                            Визуальный редактор сайта <br> (аналог Microsoft Word)
                        </p>
                    </div>
                    <div class="props_guide d-flex"><img src="{{asset('images/setting_page.svg')}}" alt="">
                        <p>
                            Использование технологии AJAX
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 screen_card">
                <div class="card">
                    <h5>
                        Модуль <br> для самостоятельной <br> оптимизации сайта <br> под поисковые системы
                    </h5>
                    <p>
                        Добавление ключевых слов и meta тэгов для каждой страницы в отдельности
                    </p>
                    <p>
                        Автоматически генерируемый файл sitemap для поисковых машин
                    </p>
                    <button>
                        Заказать CMS
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="nineth_screen">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-8">
                <div class="d-flex">
                    <button class="mains">SMM</button>
                    <button class="mains">SEO</button>
                </div>
                <div class="nine_text">
                    <h3>
                        Создание эффективных сайтов
                    </h3>
                    <p>
                        Мы не просто создаем красивые и удобные сайты. <br>
                        Мы максимально эффективно настраиваем и запускаем рекламную компанию для <br>
                        каждого проекта индивидуально. Что позволяет нашим клиентам получать прибыль уже с <br>
                        первых дней работы сайта разработанного нами.
                    </p>
                    <button>
                        Заказать ПРОДВИЖЕНИЕ
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.third_screen .col-lg-4').hover(function () {
        $(this).find('button').addClass('active')

    }, function () {
        $(this).find('button').removeClass('active')
    })

</script>
