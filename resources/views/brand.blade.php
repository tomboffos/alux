@extends('layouts.main')
@section('content')

    <div class="container-fluid" id="main-screen">
        <div class="container containerOne wow fadeInLeft">
            <div class="row justify-content-center header">
                <div class="col-auto">
                    <div class="white-image">
                        <img src="img/design/header-picture.png">
                    </div>
                    <h5><span class="white">● </span><span class="blue">ОТЛИЧАТЬСЯ</span><span class="white"> - ВЫГОДНО!</span></h5>
                </div>
                <div class="col-md-5 col-sm-10 offset-xl-3 offset-lg-1">
                    <div class="mt-5">
                        <h5 class="white">Профессиональный логотип - достойное лицо вашего бизнеса.</h5>
                    </div>
                    <div class="info" >
                        <p>Выходя в свет, вы заботитесь о своей внешности. Выводя в свет свой бизнес, вы наверняка также заботитесь, чтобы он выглядел наиболее выгодно на фоне конкурентов.<br>
                            Внешний вид любого бренда - его фирменный стиль, логотип.<br>
                            Он может быть простым, замысловатым, эксцентричным и эпатирующим, но в любом случае он должен отражать суть вашего изнеса и создавать необходимый образ.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid wow fadeInRight" id="screen">
        <div class="row flex-direct">
            <div class="col-md-6 content-black" >
                <div class="block-for-shadow container">
                    <!-- <div class="shadow"></div> -->
                    <div class="abs col-xl-8 col-lg-10 col-md-11 col-sm-11 col-12  info brandbook-content">
                        <h6 class="xz-style black ">Мы часто задаёмся вопросами: что заставляет новых покупателей обращаться в ту или иную компанию? Как она может стать узнаваемой и наладить продуктивные отношения с партнёрами? Разумеется, благодаря фирменному стилю.</h6>
                        <span style="border-top:3px solid #4996ca;padding-right: 37px;"></span>
                        <p class="xz-style black ">Создание фирменного стиля в Алматы предусматривает разработку визуального имиджа фирмы и формулировку особенностей корпоративной, печатной, рекламной, полиграфической и сувенирной продукции. Концепция фирменного стиля непосредственно зависит от качества и грамотности выполнения, поэтому от компании предоставляющей эту услугу, требуется немалый опыт и знания в области корпоративного дизайна.</p>
                    </div>
                </div>
            </div>
            <div class="col-md col-0 branding">

            </div>
        </div>
    </div>
    <style>
        @media only screen and (max-width: 991px){
            .flex-direct{
                display: flex;
                /* flex-direction: column */
            }
            
        }
    </style>
    <div class="container-fluid" id="why_firm_style">
        <div class="container" align="center">
            <div class="row justify-content-center wow fadeInLeft">
                <div class="col-lg-7 col-11">
                    <h2>Почему фирменный стиль следует заказать именно в a-lux?</h2>
                </div>
            </div>
            <div class="row justify-content-center cards wow fadeInRight">
                <div class="col-lg-3 col-md-4 col-7">
                    <img src="img/design/1.png" >
                    <p class="white mt-5">Мы знаем, как сделать вашу фирму узнаваемой в Казахстане или в любой другой точке мира.</p>
                </div>
                <div class="col-lg-3 col-md-4 col-7 offset-md-1">
                    <img src="img/design/2.png">
                    <p class="white mt-5">Всегда работаем до полного утверждения Заказчиком: Мы работаем на результат. <br>Договор - дороже денег.</p>
                </div>
                <div class="col-lg-3 col-md-4 col-7 offset-lg-1 offset-md-0">
                    <img src="img/design/3.png">
                    <p class="white mt-5">Придерживаемся трех основных принципов: стиль, качество и дедлайн.</p>
                </div>
            </div>
            <div class="row justify-content-center wow fadeInLeft mt-5">
                <div class="col-lg-3 col-md-4 col-7">
                    <img src="img/design/4.png">
                    <p class="white mt-5">Мы не мыслим стереотипами, поэтому вы не получите конвейерный продукт.</p>
                </div>
                <div class="col-lg-3 col-md-4 col-7 offset-md-1">
                    <img src="img/design/5.png">
                    <p class="white mt-5">У нас приемлемые цены.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="do_you_have_questions">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-sm-12 col-11 form wow zoomIn">
                    <div class="col-auto">
                        <h2>У ВАС ЕСТЬ ВОПРОСЫ?</h2>
                        <hr size="1px" width="25px" align="center" color="#000000">
                        <h3>Напишите нашему менеджеру!</h3>
                    </div>
                    <form class="row justify-content-center">
                        <div class="col-md-6 col-12">
                            <input type="text" id="name" name="name" placeholder="Ваше имя">
                        </div>
                        <div class="col-md-6 col-12">
                            <input type="text" name="phone" id="phone" placeholder="Ваш телефон">
                        </div>
                        <div class="col-12 mt-3">
                            <input class="text" type="text" id="text" name="msg" placeholder="Краткая информация о Вашем вопросе">
                        </div>
                        <div class="col-auto">
                            <input type="submit" name="submit" value="Отправить">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="firm_style">
        <div class="container">
            <div class="row justify-content-center wow fadeInUp">
                <div class="col-lg-9 col-sm-11 heading">
                    <h3 class="blue">Из чего состоит фирменный стиль</h3>
                    <hr size="5px" width="50px" color="#6e6e6e">
                    <h5 class="white mb-4">Фирменный стиль должен "проникать" в каждого потребителя, в каждого сотрудника вашей компании и быть узнаваемым брендом чуть ли не на уровне подсознания.</h5>
                    <h5 class="white">Вы можете заказать разработку любого из элементов фирменного стиля отдельно.</h5>
                </div>
            </div>
            <div class="row justify-content-center wow fadeInUp">
                <div class="col-auto card" >
                    <div align="center"><img src="img/design/11.png" width="80px"></div>
                    <h5>Логотип</h5>
                    <p class="text">коммерционные решения лого-блока, размещение на цветных фонах.</p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Логотип">ЗАКАЗАТЬ</button>
                    </div>
                </div>
                <div class="col-auto card">
                    <div align="center"><img src="img/design/22.png" width="80px"></div>
                    <h5>Корпоративные цвета и шрифты</h5>
                    <p class="text"></p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Корпоративные цвета и шрифты">ЗАКАЗАТЬ</button>
                    </div>
                </div>
                <div class="col-auto card">
                    <div align="center"><img src="img/design/33.png" width="80px"></div>
                    <h5>Стилеобразующая графика</h5>
                    <p class="text">(паттерны, узоры, графические элементы)</p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Стилеобразующая графика">ЗАКАЗАТЬ</button>
                    </div>
                </div>
                <div class="col-auto card">
                    <div align="center"><img src="img/design/44.png" width="80px"></div>
                    <h5>Уникальные иконки, пиктограммы, инфографика.</h5>
                    <p class="text"></p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Уникальные иконки, пиктограммы, инфографика.">ЗАКАЗАТЬ</button>
                    </div>
                </div>
                <div class="col-auto card">
                    <div align="center"><img src="img/design/55.png" width="80px"></div>
                    <h5>Корпоративный фотостиль</h5>
                    <p class="text">и особые обработки фотографий</p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Корпоративный фотостиль">ЗАКАЗАТЬ</button>
                    </div>
                </div>
                <div class="col-auto card">
                    <div align="center"><img src="img/design/66.png" width="80px"></div>
                    <h5>Разработка фирменного бланка</h5>
                    <p class="text">и принципы компоновки графических элементов.</p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Разработка фирменного бланка">ЗАКАЗАТЬ</button>
                    </div>
                </div>
                <div class="col-auto card">
                    <div align="center"><img src="img/design/77.png" width="80px"></div>
                    <h5>Уникальный формат</h5>
                    <p class="text">печатных изданий. Особые приёмы типографской печати.</p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Уникальный формат">ЗАКАЗАТЬ</button>
                    </div>
                </div>
                <div class="col-auto card text-center">
                    <div align="center"><img src="img/design/88.png" width="80px"></div>
                    <h5>Ключевые рекламные образы</h5>
                    <p class="text">(Key Visual) и рекламные сообщения (Key Message).</p>
                    <div class="fixed-bottom">
                        <button class="order-btn" data-val="Ключевые рекламные образы">ЗАКАЗАТЬ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block-for-shadow container-fluid wow fadeInLeft" id="razrabotka_brend">
        <div class="shadow-2"></div>
        <div class="abs container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-md-10 col-sm-12 info">
                    <h4>РАЗРАБОТКА БРЭНДБУКА</h4>
                    <span style="border-top:3px solid #4996ca;padding-right: 37px;margin:80px 0px;"></span>
                    <h6>Отличие брендбука от фирменного стиля</h6>
                    <p class="black">Очень часто путают понятия фирменного стиля и брендбука или приравнивают их значения. Давайте разберёмся в определениях. Фирменный стиль - это набор и удачное сочетание графических и шрифтовых решений, являющихся основой исполнения коммерческих и деловых материалов компании, он является составляющей брендбука.</p>
                    <h6 class="black mt-3 mb-3">БРЕНДБУК, ПРЕДЛАГАЕМЫЙ КОМПАНИЕЙ A-LUX - ЭТО РУКОВОДСТВО ПО ФИРМЕННОМУ СТИЛЮ.</h6>
                    <p class="black">Главное различие между фирменным стилем и брендбуком - целевая аудитория. Закрытый документ, создаваемый строго для внутрикорпоративного применения - это брендбук, фирменный стиль же целиком и полностью ориентирован на потребителей, он внекорпоративного использования.</p>
                    <button class="order-btn">ЗАКАЗАТЬ РАЗРАБОТКУ БРЕНДБУКА</button>
                </div>
                <div class="col-xl-6 col-12 cards">
                    <div class="row justify-content-center">
                        <div class="col-sm-6 col-10 card">
                            <div class="steaker">
                                <div class="name_of_brend">
                                    <h4>НАЗВАНИЕ БРЕНДА</h4>
                                </div>
                                <div class="triangle"></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-10 card">
                            <div class="steaker">
                                <div class="name_of_brend">
                                    <h4>НАЗВАНИЕ БРЕНДА</h4>
                                </div>
                                <div class="triangle"></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-10 card">
                            <div class="steaker">
                                <div class="name_of_brend">
                                    <h4>НАЗВАНИЕ БРЕНДА</h4>
                                </div>
                                <div class="triangle"></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-10 card">
                            <div class="steaker">
                                <div class="name_of_brend">
                                    <h4>НАЗВАНИЕ БРЕНДА</h4>
                                </div>
                                <div class="triangle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="zakagite">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-12 heading">
                    <h2>Закажите разработку профессионального брендбука в алматы</h2>
                    <hr size="5px" width="55px" color="#6e6e6e">
                </div>
            </div>
            <div class="row justify-content-center cards" align="center">
                <div class="col-lg-3 col-md-4 col-7 wow fadeInDown">
                    <img src="img/design/111.png">
                    <p class="white mt-5">Мы знаем, как сделать вашу фирму узнаваемой в Казахстане или в любой другой точке мира.</p>
                </div>
                <div class="col-lg-3 col-md-4 col-7 offset-md-1 wow fadeInUp">
                    <img src="img/design/222.png">
                    <p class="white mt-5">Всегда работаем до полного утверждения Заказчиком: Мы работаем на результат. <br>Договор - дороже денег.</p>
                </div>
                <div class="col-lg-3 col-md-4 col-7 offset-lg-1 wow fadeInDown">
                    <img src="img/design/333.png">
                    <p class="white mt-5">Придерживаемся трех основных принципов: стиль, качество и дедлайн.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mb-5 pb-5 mt-5" id="leave_form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-sm-12 col-11 form wow zoomIn">
                    <div class="col-auto">
                        <h2>ОСТАВИТЬ ЗАЯВКУ</h2>
                        <hr size="1px" width="25px" align="center" color="#000000">
                    </div>
                    <form class="row justify-content-center">
                        <div class="col-md-6 col-12">
                            <input type="text" id="name" class="order-name2" name="" placeholder="ваше имя">
                        </div>
                        <div class="col-md-6 col-12">
                            <input type="text" id="phone3" class="order-contacts2" name="" placeholder="ваш телефон">
                        </div>
                        <div class="col-12">
                            <input class="text" id="text" class="order-task2" type="text" name="" placeholder="краткая информация о Вашем вопросе">
                            <input class="text" id="text2" class="order-task2" type="text" name="" placeholder="информация о вопросе">
                        </div>
                        <div class="col-auto">
                            <input type="submit" class="order-btn2" name="" value="ОТПРАВИТЬ">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

  

<!--        <link rel="stylesheet" href="alma-lux.kz/owl-carousel-owl-theme-bootstrap-slick-fancybox-sweetalert-style.min.css">-->

        <script>
            $(document).on('click', function(e) {
                if (!$(e.target).closest("content").length) {
                    if($('input#hamburger').is(":checked")) {

                        $('#hamburger').click();

                    }
                }
                e.stopPropagation();
            });
        </script>
        <script>
            if (window.matchMedia("(max-width: 576px)").matches) {
                console.log('aaaaaaaaaaaaaaaaaaa')
                document.querySelectorAll('.fadeInLeft, .fadeInRight, .wow').forEach(fade => {
                    if (fade.classList.contains('fadeInLeft')) {
                        fade.classList.remove('fadeInLeft');
                    }
                    else if (fade.classList.contains('fadeInRight')) {
                        fade.classList.remove('fadeInRight');
                    }
                    else if (fade.classList.contains('wow')) {
                        fade.classList.remove('wow');
                    }
                });
            }
        </script>
        <style>
           
        </style>


    

@endsection
