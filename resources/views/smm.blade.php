@extends('layouts.main')
@section('content')


<section id="about_us_page_sec_one">
    <div class="container">
        <div class="row ">
            <div class="col-lg-6 col-md-6 col-12 text-center">

                <img src="img/hands.png" width="100%" alt="">

            </div>
            <div class="col-lg-6 col-md-6 col-12 mt-5">
                <h2 class="heading_smm bold">Будь ближе <br> к своим клиентам!</h2>
                <div class="container border-l mt-3">

                    <p class="white">Комлексное продвижение и ведение личных и бизнес аккаунтов в инстаграмм в Алматы и других городах Казахстана от профессионалов с 12 летним стажем. 4 000 000 пользователей интернета только в Казахстане, помогите им вас найти. </p>

                </div>

            </div>
        </div>
    </div>
</section>

    <div class="container-fluid">
        <div class="col-sm-12 first-form" id="free-account">
            <div class="row">
                <div class="col-sm-12 caption-text">
                    <p class="untitle">Бесплатный аудит</p>
                    <h3>Вашего Аккаунта в инстаграме</h3>
                    <hr>
                    <p class="subtitle">Заполните анкету и получите бесплатный разбор Вашего аккаунта от профессионалов</p>
                </div>
                <div class="col-sm-12 form-window wow pulse">
                    <form id="regForm" action="/al2016kz.php" method="POST">
                        <div class="paginfInfo">
                            <span class="step">1</span>
                            <hr class="hline">
                            <span class="step">2</span>
                            <hr class="hline">
                            <!-- <span class="step">3</span>
                            <hr class="hline"> -->
                            <span class="step">3</span>
                        </div>
                        <div class="tab names">
                            <div class="tabtitle"><span></span></div>
                            <div class="input-fields">
                                <input type="hidden" name="task" value="SMM заявка">
                                <input type="hidden" name="event" value="order_task">
                                <input placeholder="Как Вас Зовут ?" oninput="this.className = ''" name="surname">
                                <input placeholder="Ссылка на Ваш аккаунт в соц. сетях" oninput="this.className = 'order-name'" name="name">
                            </div>
                        </div>

                        <div class="tab places col-sm-12">
                            <div class="tabtitle2"><span>По какому региону Вы работаете?</span></div>
                            <div class="input-fields2 col-sm-12 row">
                                <div class="col-sm-6">
                                    <input type="radio" id="allrussia" name="food">
                                    <label for="allrussia">Весь Казахстан</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="radio" name="tobilion" id="tobillion" class="radio-btn">
                                    <label for="tobillion">Города до миллиона</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="radio" name="moscow" id="moscow" class="radio-btn">
                                    <label for="moscow">Алматы/Нурсултан</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="radio" name="frombillion" id="frombillion" class="radio-btn">
                                    <label for="frombillion">Города свыше миллиона</label>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="tab places col-sm-12">
                            <div class="tabtitle2"><span>По какому региону Вы работаете?</span></div>
                            <div class="input-fields2 col-sm-12 row">
                                <div class="col-sm-6">
                                    <input type="radio" name="allrussia" id="allrussia1">
                                    <label for="allrussia1">Вся Россия</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="radio" name="moscow" id="moscow1">
                                    <label for="moscow1">Москва/Санкт-Петербург</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="radio" name="tobilion" id="tobillion1">
                                    <label for="tobillion1">Города до ммиллиона</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="radio" name="frombillion" id="frombillion1">
                                    <label for="frombillion1">Города свыше миллиона</label>
                                </div>
                            </div>
                        </div> -->
                        <div class="tab contacts">
                            <div class="tabtitle"><span>Контакты<span></div>
                            <div class="input-fields">
                                <input placeholder="Номер" oninput="this.className = 'order-contacts'" name="contacts">
                                <input placeholder="E-mail" oninput="this.className = ''" name="email">
                            </div>
                        </div>
                        <div style="overflow:auto;" class="buttons-arrows">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6 col-6">
                                        <div class="wrapper" id="wrapper2">
                                            <button class="inst-btn" type="button" id="prevBtn" onclick="nextPrev(-1)"><img loading="lazy" src="images/backwards.png" alt=""></button>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-6">
                                        <div class="wrapper" id="wrapper1">
                                            <button class="inst-btn" type="button" id="nextBtn" onclick="nextPrev(1)"><img loading="lazy" src="images/forwards.png" alt=""></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper" id="wrapper3">
                                <button class="inst-btn send_smm" type="submit" name="button">
                                    Отправить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <section id="smm_text_slider text-center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 pt-5"><h3 align="center" class="smm_title_text">Не упустите возможность</h3></div>
            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <div class="carousel-inner" role="listbox">
                    
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">
                            <p class="smm_text_slide">В SMM продвижение входит: публикация рекламных объявлений, ведение аккаунтов в социальных сетях, размещение и проведение каких-либо конкурсов, опросов и тп. Подобный комплекс мероприятий позволяет существенно нарастить трафик и посещаемость аккаунта и сайта компании.</p>
                            
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">

                            <p class="smm_text_slide">Комплексное продвижение Ваших товаров и услуг при использовании методов SMM осуществляется в различных социальных сетях, таких как Facebook, Instagram, Pinterest, Twitter, ВКонтакте и пр. На сегодняшний день наибольшую результативность показывает именно Instagram. </p>
                         
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <img src="/img/arrow-left.png">
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <img src="/img/arrow-right.png">
                </a>
            </div>
            <div class="col-12 mt-5" align="center">
                    <a href="#" class="btn btn-lg btn_whiteb order-btn">Узнать больше</a>
                
                <hr class="mt-5">
                </div>
        </div>
    </div>
</section>


        
        
<div class="col-sm-12 white-bg" id="how-works">
    <h3 class="smm_title_text">Как Мы работаем?</h3>
    <div class="cirlces-block container d-flex justify-content-center ">
        <div class="circles circle-cr-account col-sm-4 ">
            <div class="cicle-ellipse wow rotateIn">
                <img src="img/1.png">
            </div>
            <div class="circle-content wow fadeInUp">
                <h6 class="upper">Создание контент-плана и ведение аккаунта в инстаграм</h6>
                <p align="justify">Контент Ваших постов должен обладать двумя значимыми характеристиками, такими как <b>постоянство</b> и <b>актуальность</b>. Важно соблюдать стабильный контент-план, и уметь при этом грамотно выходить за его рамки при обсуждении горячих новостей!</p>
            </div>
        </div>
        <div class="circles circle-cr-account col-sm-4">
            <div class="cicle-ellipse wow rotateIn">
                <img src="img/instagram.png">
            </div>
            <div class="circle-content wow fadeInUp">
                <h6 class="upper">Таргетированная реклама в инстаграм</h6>
                <p align="justify">Один из самых эффективных маркетинговых инструментов, который использует сложные методы и настройки поиска целевой аудитории в соответствие с заданными параметрами.
                    <br>
                    Мы настраиваем рекламу – Вы получаете прибыль!
                </p>
            </div>
        </div>
        <div class="circles circle-cr-account col-sm-4">
            <div class="cicle-ellipse wow rotateIn">
                <img src="img/4.png">
            </div>
            <div class="circle-content wow fadeInUp">
                <h6 class="upper">Реклама в сообществах</h6>
                <p align="justify">При Выборе сообщества - нужно отталкиваться не только от количества подписчиков, но и от его тематики. Чем ближе тематика сообщества к услуге или товару который Вы продвигаете – тем больше вероятность заинтересованности аудитории в Вашем продукте! </p>
            </div>
        </div>
        <div class="circles circle-cr-account col-sm-5">
            <div class="cicle-ellipse wow rotateIn">
                <img src="img/3.png">
            </div>
            <div class="circle-content wow fadeInUp">
                <h6 class="upper">Повышение активности подписчиков в инстаграм</h6>
                <p align="justify">Интерактив – поВышает вовлеченность подписчика в разы. Только полезные и Вызывающие дискуссию посты, опросы и конкурсы могут стать причиной для поВышения активности Ваших подписчиков! </p>
            </div>
        </div>
        <div class="circles circle-cr-account col-sm-5 ">
            <div class="cicle-ellipse wow rotateIn">
                <img src="img/4.png">
            </div>
            <div class="circle-content wow fadeInUp">
                <h6 class="upper">Комплексное ведение и продвижение аккаунта инстаграм</h6>
                <p align="justify">Комплексный подход – всегда отличается особой эффективностью.
                    <br>
                    Мы можем предложить Вам то, от чего Вы не сможете отказаться!
                   <br>
                    Мы работаем. Вы наблюдаете за результатом!
                </p>
            </div>
        </div>
    </div>
    <div class="get-concult">
         <button class="btn-effect order-btn btn-get"><span data-title="получить бесплатную консультацию">получить бесплатную консультацию</span></button>


    </div>

</div>

<div class="col-sm-12 gray-bg" id="whyus">
    <h3 class="smm_title_text">Почему Выбирают нас?</h3>
    <div class="circles-whyus d-flex justify-content-center container">
        <div class="col-sm-4 col-md-6 col-lg-4 whyus-circle-block wow rubberBand">
            <div class="circlesofus  ">
                <div class="inside-circle-text">
                    <p class="top-text">от <span class="bigger"><strong>3</strong></span> <strong>лет</strong></p>
                    <p class="bottom-text">Наши менеджеры имеют опыт<br> работы в соцсетях </p>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-md-6 col-lg-4 whyus-circle-block wow rubberBand">
            <div class="circlesofus purple">

                <div class="inside-circle-text">
                    <p class="top-text"><span class="bigger"><strong>12 </strong></span><strong>лет</strong></p>
                    <p class="bottom-text">На рынке Казахстана в сфере IT </p>
                </div>
            </div>

        </div>
        <div class="whyus-circle-block col-sm-4 col-md-6 col-lg-4 wow rubberBand">
            <div class="circlesofus green">

                <div class="inside-circle-text">
                    <p class="top-text">более <span class="bigger"><strong>10 000</strong></span></p>
                    <p class="bottom-text">Человек поучаствовало в наших<br> конкурсах в Инстаграмм </p>
                </div>

            </div>
        </div>
        <div class="col-sm-4 whyus-circle-block col-md-6 col-lg-4 wow rubberBand">
            <div class="circlesofus green">

                <div class="inside-circle-text">
                    <p class="top-text">более <span class="bigger"><strong>1568</strong></span></p>
                    <p class="bottom-text">Фото и видео было загружено<br> в Инстаграмм за месяц </p>
                </div>
            </div>
        </div>

        <div class="col-sm-4 whyus-circle-block col-md-6 col-lg-4 wow rubberBand">
            <div class="circlesofus yellow">

                <div class="inside-circle-text">
                    <p class="top-text">более <span class="bigger"><strong>1,3</strong></span><strong>млн</strong></p>
                    <p class="bottom-text">Лайков было получено в Инстаграмме</p>
                </div>
            </div>

        </div>
        <div class="col-sm-4 whyus-circle-block col-md-6 col-lg-4 wow rubberBand">
            <div class="circlesofus blue">
                <div class="inside-circle-text">
                    <p class="top-text"><span class="bigger"><strong>10 151</strong></span></p>
                    <p class="bottom-text">Комментариев в Инстаграмме получено </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FIFTH SECTION -->
<div class="col-sm-12 white-bg" id="getfollowers">
    <div class="container caption-text wow fadeInUp">
        <p class="untitle">Получайте любое кол-во </p>
        <h3>лайков и подписчиков!</h3>
        <p class="subtitle">При соблюдении всех необходимых правил настройки и ведения аккаунта в Instagram, Вы заметите, что количество целевых подписчиков и лайков увеличивается на глазах.
            Правильно прописанные #хэштеги, указание Выгодного местоположения, и множество других факторов, влияющих на количество целевых подписчиков помогут Вам реализовать Вашу мечту и сделать Ваш аккаунт более жиВым, более конкурентоспособным, более развитым и полезным.</p>
    </div>
    <div class="our-services container d-flex" style="margin-bottom:0px">
        <div class="protect-content service-content col-sm-4">
            <div class="protect-circle service-circle mb-3 wow heartBeat">
                <img src="img/protect.png" alt="">
            </div>
            <div class="protect-text sevrvice-text wow fadeInUp">
                <p>Абсолютно безопасно. Мы не спрашиваем логин, пароль или другие данные. Только ссылку для накрутки</p>
            </div>
        </div>
        <div class="protect-content service-content col-sm-4">
            <div class="protect-circle service-circle mb-3 wow heartBeat">
                <img src="img/flash.png" alt="">
            </div>
            <div class="protect-text sevrvice-text wow fadeInUp">
                <p>Максимально быстро. Всё в режиме онлайн. Сотни лайков и подписчиков за минуты</p>
            </div>
        </div>
        <div class="protect-content service-content col-sm-4">
            <div class="protect-circle service-circle mb-3 wow heartBeat">
                <img src="img/quality.png" alt="">
            </div>
            <div class="protect-text sevrvice-text wow fadeInUp">
                <p>Высочайшее качество. Нашим сервисом пользуются тысячи довольных пользователей</p>
            </div>
        </div>
    </div>

    <div class="service-button">

        <button class="btn-effect btn-service-get order-btn"><span data-title="Заказать">Заказать</span></button>

    </div>
</div>

<div class="col-sm-12 gray-bg" id="audienceinsta">
    <div class="container">
        <div class="col-sm-12 caption-text">
            <h3>Аудитория Инстаграма </h3>
        </div>
        <div class="audiencestatics col-sm-12 d-flex">
            <div class="col-sm-4 pie-chart-statics wow heartBeat">
                <img src="img/chart.png" class="chart w-80 mt-3 mb-3">
            </div>
            <div class="col-sm-8 audience-content">
                <div class="followers-sets d-flex col-sm-12">
                    <div class="col-lg-6 col-md-12 d-flex audience-chart wow pulse">
                        <img src="img/border.png" alt="">
                        <p class="set-text">Аудитория в возрасте 18—29 лет</p>
                    </div>
                    <div class="col-lg-6 col-md-12 wow pulse">
                        <img src="img/globe.png" alt="">
                        <p class="set-text">Путешественники в Инстаграме</p>
                    </div>
                    <div class="col-lg-6 col-md-12 wow pulse">
                        <img src="img/diamond.png" alt="">
                        <p class="set-text">Платежеспособная аудитория в Инстаграме</p>
                    </div>
                    <div class="col-lg-6 col-md-12 wow pulse">
                        <img src="img/meal.png" alt="">
                        <p class="set-text">Посетители ресторанов в Инстаграме</p>
                    </div>

                </div>
                <div class="col-sm-12 information-content offset-1 wow pulse">
                    <img src="img/information.png" alt="">
                    <p class="set-text">Казахстанцы тратят на Инстаграм и другие соцсети вдвое больше времени, чем жители США и Великобритании</p>
                </div>
            </div>
        </div>
        <div class="audience-button col-sm-12">
            <button class="btn-effect btn-rm order-btn"><span data-title="узнать больше">узнать больше</span></button>

        </div>
    </div>
</div>

<div class="col-sm-12" id="criteries">
    <div class="col-sm-12 caption-text  text-center">
        <p>Привлечение подписчиков </p>
        <h3>по вашим критериям</h3>
    </div>
    <div class="criteries-block container d-flex justify-content-center">
        <div class="col-sm-4 criteris-content">
            <div class="criteries-icon wow flip">
                <img src="img/placeholder_(1).png" alt="">
            </div>
            <div class="criteries-text wow fadeInUp">
                <h6>По гео локации</h6>
                <p>Нужны жители конкретного района? Соберем всех, кто отмечал свои фото ГЕО-отметками в непосредственной близости от вашего офиса</p>
            </div>

        </div>
        <div class="col-sm-4 criteris-content">
            <div class="criteries-icon wow flip">
                <img src="img/love.png" alt="">
            </div>
            <div class="criteries-text wow fadeInUp">
                <h6>По лайкам и комментариям</h6>
                <p style="font-size: 14.1px;">Если вам нужна самая активная аудитория, Мы возьмем только тех, кто ставит лайки и задает вопросы в комментариях на страничках с вашей тематикой. </p>
            </div>

        </div>
        <div class="col-sm-4 criteris-content">
            <div class="criteries-icon wow flip">
                <img src="img/hashtag.png" alt="">
            </div>
            <div class="criteries-text wow fadeInUp">
                <h6>По тегам</h6>
                <p>Ваши клиенты представители определенного рода деятельности? Соберем людей, которые ставят под фото теги #фотограф #дизайнер #шоумен и т.д.</p>
            </div>

        </div>
        <div class="col-sm-4 criteris-content">
            <div class="criteries-icon wow flip">
                <img src="img/value.png" alt="">
            </div>
            <div class="criteries-text wow fadeInUp">
                <h6>По подписчикам конкурентов</h6>
                <p>Расскажем о вас тем, кто уже пользуется вашим продуктом или услугой и переманим к вам!</p>
            </div>

        </div>
        <div class="col-sm-4 criteris-content">
            <div class="criteries-icon wow flip">
                <img src="img/gender.png" alt="">
            </div>
            <div class="criteries-text wow fadeInUp">
                <h6>По полу</h6>
                <p>Нужны только девушки? Пройдемся по подписчикам тематических аккаунтов для женщин.</p>
            </div>

        </div>
        <div class="col-sm-4 criteris-content">
            <div class="criteries-icon wow flip">
                <img src="img/objective.png" alt="">
            </div>
            <div class="criteries-text wow fadeInUp">
                <h6>По интересам</h6>
                <p>Брендовая одежда? Салон красоты? Свадебное агенство? Скомбинируем несколько способов поиска заинтересованных людей для максимального охвата.</p>
            </div>

        </div>

    </div>
</div>

<div class="col-sm-12 gray-bg" id="ourpartners">
    <div class="col-sm-12 caption-text  text-center">
        <h3>с нами работают</h3>
    </div>

            <!-- mobile -->
            <div class="col-12 d-md-none d-xs-block text-center">
                <img src="img/brands_mobb.png" alt="">
            </div>
            <!-- end mobile -->

            <!-- desktop -->
            <div class="col-12 d-none d-md-block text-center">
                <img src="img/brands_desktopb.png" class="w-80" alt="">
            </div>
            <!-- end desktop -->

</div>
    <script type="text/javascript" src="n_js/main-smm.js"></script>


@endsection
