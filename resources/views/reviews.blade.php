@extends('layouts.main')
@section('content')
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/site.css">
    <link rel="stylesheet" href="css/customer-reviews.css">
<main>


    <section class="intro">
        <div class="container">
            <img src="images/intro-heart.png" alt="сердце с дымком" class="intro-heart">
            <div class="col">
                <div class="intro-text">
                    <h1 class="page-title">Что говорят клиенты A-Lux</h1>
                    <p class="page-subtitle">Мы создаем мощные инструменты, которые помогают вашему бизнесу занять
                        лидирующие позиции</p>
                </div>
                <img class="intro-shining" src="images/shining-black.png" alt="северное сияние">
            </div>
        </div>
    </section>

    <section class="reviews">
        <div class="container">
            <h2 class="reviews-title">Что думают о нас наши клиенты</h2>
            <ul id="reviews">
                @foreach($reviews as $certificate)
                <li class="review">
                    <a href="{{$certificate['image_link']}}" data-fancybox data-caption="Caption for single image">
                    <img src="{{$certificate['image_link']}}" alt="отзыв" class="review-image">
                </a>
                    <div class="review-customer">
                        <img class="customer-logo" src="{{$certificate['mini_logo']}}" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">{{$certificate['title']}}</a>
                    </div>
                </li>
                @endforeach
                <li class="review">
                    <a href="images/review-1.jpg" data-fancybox='Тест'>
                    <img src="images/review-1.jpg" alt="отзыв" class="review-image">
                </a>
                    <div class="review-customer">
                        <a href="images/airport-logo.png" data-fancybox='Тест'>
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                    </a>
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>



                <li class="review">
                    <img src="images/review-1.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-3.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>



                <!-- Second page -->

                <li class="review">
                    <img src="images/review-1.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-3.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-1.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-3.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-1.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-3.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <img class="customer-logo" src="images/airport-logo.png" alt="логотип клиента">
                        <a href="https://airportalmaty.kz" class="customer-link">Алматы Аэропорт</a>
                    </div>
                </li>

                <li class="review">
                    <img src="images/review-2.jpg" alt="отзыв" class="review-image">
                    <div class="review-customer">
                        <a href="http://supplygroup.asia/#/login" class="customer-link">supply group</a>
                    </div>
                </li>

            </ul>



            <div class="pagination" style="margin-top: 5%">
                <button class="previous"><img src="images/icons/arrow-left.png" alt="предидущий отзыв"></button>
                <button class="next"><img src="images/icons/arrow-right.png" alt="следующий отзыв"></button>
            </div>
        </div>
    </section>
</main>
    <script src="{{asset('scripts/reviews-pagination.js')}}"></script>


    <style>
        .intro{
            margin-top: 75px !important;
        }
        .founder{
            max-width: 100% !important ;
        }
        .stat-number{
            font-size: 5rem !important;
        }
        .intro-text{
            font-size: 1.5rem !important;
        }
        .our-attributes{
            padding-top: 6em;
        }
        button.previous , button.next{
            /* background-image: none !important; */
            /* border: 2px solid #4996CA; */
        }
        button i{
            color: #4996CA;
        }
        .founder-image{
            width: 140px !important;
            height: 140px !important;
        }
        body{

        }
        .reviews-title{
            font-size: 1.5rem;
        }
        .page-subtitle{
            font-size: 1.7rem !important;
        }
        .page-title{
            font-size: 2.4rem !important;
        }
        .review{
            width: 286px;
            height: 390px;
            margin-bottom: 20px;
        }

    </style>

@endsection
