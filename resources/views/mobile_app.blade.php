@extends('layouts.main')
@section('content')
    
<div class="main_text">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center">
                <img src="img/hands.png" alt="hands" class="hands">
                <img src="img/left-phone.png" alt="phone" class="phone_main">
                <div class="main_ios">
                    <img src="img/ios.png" alt="" class="main_ios">
                </div>

            </div>
            <div class="col-lg-6 mt-4">
                <h2 class="main-title">
                    Мобильные приложения для
                    Вашего бизнеса
                </h2>
                <p class="main">
                    Мы знаем, как создать мобильное приложение высшего качества, которое будет приносить максимум выгоды своему владельцу. Если вам нужна разработка мобильных приложений в Алматы, обращайтесь в A-LUX, и вы получите не только продукт высокого уровня, вы откроете перед своим бизнесом новые горизонты, расширите охват потенциальных клиентов, а также значительно повысите уровень прибыльности своей компании.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="second_section">
    <div class="">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="blue_title">
                    РЕШЕНИЯ ДЛЯ БИЗНЕСА <br>
                    С BLUETOOTH МАЕЧКАМИ IBEACON
                </h1>
            </div>
            <div class="col-lg-6 image_second">
                <img src="img/second.jpg" alt="">
            </div>

            <div class="col-lg-6">
                <div class="gray_card">
                    <p>
                        Хотите узнать, как применить iBeacon в Вашем бизнесе? Представляем Вам примеры
                        наиболее популярных кейсов с iBeacon. Эти идеи успешно работают на практике и могут стать Вашей реальностью уже сегодня.
                    </p>
                    <a href="" class="btn-hollow">
                        Узнать подробнее
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="third_section">
    <div class="container">
        <div class="row" align="center">
            <div class="col-lg-12">
                <h1 class="black_title bold upper mb-2">
                    Какому бизнесу подойдет?
                </h1>

            </div>
            <div class="col-sm-3 col-6 mt-4 pos-col-md">
                <div class="circle_image"><img src="img/mobile.png" alt=""></div>
                <h5 class="mt-3 bold">Интернет-шоппинг</h5>
            </div>
            <div class="col-sm-3 col-6 mt-4 pos-col-md">
                <div class="circle_image"><img src="img/support.png" alt=""></div>
                <h5 class="mt-3 bold">Сфера услуг</h5>
            </div>
            <div class="col-sm-3 col-6 mt-4 pos-col-md">
                <div class="circle_image"><img src="img/networking.png" alt=""></div>
                <h5 class="mt-3 bold">Онлайн-СМИ</h5>
            </div>
            <div class="col-sm-3 col-6 mt-4 pos-col-md">
                <div class="circle_image"><img src="img/report.png" alt=""></div>
                <h5 class="mt-3 bold">Издательствам</h5>
            </div>
        </div>
    </div>
</div>
<div class="fourth_section">
    <div class="container">
        <div class="row" align="center">
            <div class="col-lg-12">
                <h1 class="black_title">
                    С нами работают
                </h1>
            </div>
            
            <!-- mobile -->
            <div class="col-12 d-md-none d-xs-block">
                <img src="img/brands_mobb.png" alt="">
            </div>
            <!-- end mobile -->

            <!-- desktop -->
            <div class="col-12 d-none d-md-block">
                <img src="img/brands_desktopb.png" class="w-80" alt="">
            </div>
            <!-- end desktop -->

        </div>
    </div>
</div>
<div class="fifth_section">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="black_title ">О НАС</h1>
            <h3 class="blue_text">
                ФАКТЫ В ЦИФРАХ

            </h3>
        </div>
        <div class="col-lg-6">
            <div class="props">СОТРУДНИКОВ<div>17</div>
            </div>
            <div class="props">ЛЕТ НА РЫНКЕ<div>13</div>
            </div>
            <div class="props last-props">ВЫПОЛНЕННЫЙ ПРОЕКТ<div>400</div>
            </div>

        </div>
        <div class="col-lg-6">
            <img src="img/about-bg.png" style="width: 100%;" alt="">
        </div>
    </div>
</div>
<div class="sixth">
    <div class="container">
        <h3 class="text-center wow zoomInDown sixth-title">Для вашего бизнеса</h3>
        <h3 class="text-center wow zoomInDown sixth-subtitle" data-wow-delay="0.2s">Преимущества мобильных приложений</h3>
        <p class="text-center sixth-text wow rubberBand" data-wow-delay="0.4s">это мощный маркетинговый инструмент, который позволяет не только продвигать Ваши услуги в интернете, но и создавать максимально комфортные условия приобретения и использования Ваших услуг.</p>
        <div class="row">
            <div class="col-md-4">
                <div class="features-slider-item text-center" data-wow-delay="0s">
                    <img src="img/graph.png" class="wow rubberBand" alt="graph">
                    <h4>Повышение продаж</h4>
                    <p>С помощью мобильного приложения Ваши клиенты смогут принимать активное участие в различных программах лояльности, получать бонусы, а следовательно - становиться Вашими постоянными покупателями, и охотнее совершать покупки.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="features-slider-item text-center" data-wow-delay="0.1s">
                    <img src="img/investment.png" class="wow rubberBand" alt="investment">
                    <h4>Активности</h4>
                    <p>Мобильное приложение позволяет качественно и безошибочно проводить статистику, а также отслеживать показатели эффективности: количество клиентов, продажи, затраты, прибыль.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="features-slider-item text-center" data-wow-delay="0.2s">
                    <img src="img/cost.png" class="wow rubberBand" alt="cost">
                    <h4>Заработок на установке</h4>
                    <p>Каждый владелец мобильного приложения может обеспечить себя пассивным доходом, который будет напрямую зависеть от количества скачиваний.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="features-slider-item text-center" data-wow-delay="0.3s">
                    <img src="img/shop.png" class="wow rubberBand" alt="shop">
                    <h4>Продажи</h4>
                    <p>Ключевым активом любого бизнеса обычно считается высоко оптимизированный и хорошо продуманный интернет-магазин.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="features-slider-item text-center" data-wow-delay="0.4s">
                    <img src="img/audience.png" class="wow rubberBand" alt="aaudience">
                    <h4>Повышение охвата</h4>
                    <p>Быстро растущий рынок мобильной коммерции значительно влияет на укрепление связи с клиентами и повышение охвата.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="features-slider-item text-center" data-wow-delay="0.5s">
                    <img src="img/idea.png" class="wow rubberBand" alt="idea">
                    <h4>Рекламный канал</h4>
                    <p>Постоянное взаимодействие с пользователем. Установленное на смартфон мобильное приложение – является постоянным источником взаимодействия с клиентом.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="seventh" id="seventh">
    <div class="container">
        <h2 class="text-center">Статистика мобильного интернета</h2>
        <h3 class="text-center">Статистика пользователей мобильного интернета в Казахстане на 2020 год.</h3>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12 text-center wow fadeInLeft">
                <div class="pie p1"></div>
                <div class="pin p3"></div>
                <div class="seventh-block seventh-block-boy text-center"><img src="img/boy.png" alt="boy"><br>
                    <span>75,5%</span>
                </div>
                <h4>Популярность мобильного интернета</h4>
                <p>Набирает активные обороты. Количество пользователей мобильных устройств с каждым днем становится все больше и больше. Удобство и компактность смартфона обеспечиваем максимально эффективный интернет-серфинг.</p>
            </div>
            <div class="col-lg-6 col-md-12 col-12  text-center wow fadeInRight" >
                <div class="pie p2"></div>
                <div class="pit p4"></div>
                <div class="seventh-block text-center" ><img src="img/search.png" alt="search"><br>
                    <span>83,4%</span>
                </div>
                <h4>Каждый второй пользователь смартфона</h4>
                <p>Использует мобильные приложения в 5 раз чаще, чем сайты. Постоянный доступ к приложению, даже при отсутствии интернета является его существенным преимуществом.</p>
            </div>
        </div>
    </div>
</div>
   


@endsection
