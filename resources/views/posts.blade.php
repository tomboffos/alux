@extends('layouts.main')
@section('content')
    <div class="row header-info" style="background-image: url('{{asset('images/img-00.jpg')}}')">
        <div class="container text-right" style="text-align:right;margin-top: 150px;margin-bottom: 150px;">

        </div>
    </div>
    <div id="pricing">
        <div class="container">
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-sm-4 article-item clearfix">
                        <p class="datew">
                            {{$post->created_at->day}}.{{$post->created_at->month}}.{{$post->created_at->year}} г.
                        </p>
                        <h3>
                            {{$post['title']}}
                        </h3>
                        <p style="word-break: break-all">
                            @if(strlen($post['text']) > 150)
                                {!! substr(strip_tags($post['text']),0,150).'...' !!}
                            @else
                                {!! substr(strip_tags($post['text']),0,150) !!}

                            @endif
                        </p>
                        <a href="{{route('PostPage',$post['id'])}}">Читать далее</a>
                    </div>
                @endforeach
            </div>
            <div class="col-lg-12">
                {{$posts->links()}}
            </div>
        </div>
    </div>

    <style>


    </style>
@endsection
