<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('logo_path');
            $table->text('background_path');
            $table->boolean('web')->nullable();
            $table->boolean('mobile')->nullable();
            $table->boolean('branding')->nullable();
            $table->text('web_image');

            $table->text('link');
            $table->text('link_to_page')->nullable();
            $table->string('technologies');
            $table->text('description');

            $table->text('letter_path');
            $table->text('client_review');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
