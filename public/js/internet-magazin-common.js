$(document).ready(function () {
    $('.header_slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
    });
    $('.multiple-items').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        prevArrow: '<div class="prev_btn"> <div class="btn_pos"><</div> </div>',
        nextArrow: '<div class="next_btn"> <div class="btn_pos">></div> </div>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});





$(document).ready(function () {

    (function changeBg() {
        var img_array = ["/img/internet-magazin/about_1.png",
            "/img/internet-magazin/about_2.png",
            "/img/internet-magazin/about_1.png"
        ],
            _nxtIndex = 0,
            _curIndex = 0,
            interval = 15000;
        function nextIndex() { _nxtIndex = (_nxtIndex + 1) % img_array.length; return _nxtIndex; };

        function shiftIndexes() {
            _curIndex = _nxtIndex;
            nextIndex();
        }

        function assignBackgrounds() {
            for (var i = 0; i < img_array.length; i++) {

                $('#background-slide' + i).css('backgroundImage', function () {
                    return 'url(' + img_array[i] + ')';
                });
                if (i == 0) {
                    $('#background-slide' + i).css('opacity', 1);
                }
                else {
                    $('#background-slide' + i).css('opacity', 0);
                }
            }
        }

        function startBackgroundOpacityToggle() {
            //console.log("in startBackgroundOpacityToggle. _curIndex = "+_curIndex);
            elem = $('#background-slide' + _curIndex);
            elem.animate({
                opacity: (elem.css('opacity') == 0) ? 1 : 0
            }, {
                duration: 5000,
                start: finishBackgroundOpacityToggle
            });
        };

        function finishBackgroundOpacityToggle() {
            //console.log("in finishBackgroundOpacity. _nxtIndex = "+_nxtIndex);
            elem = $('#background-slide' + _nxtIndex);
            elem.animate({
                opacity: (elem.css('opacity') == 0) ? 1 : 0
            }, {
                duration: 5000,
                complete: runSlider
            });

        };

        function runSlider() {
            shiftIndexes();
            setTimeout(startBackgroundOpacityToggle, interval);
        }

        assignBackgrounds();
        runSlider();
    })();
});

$(document).ready(function () {
    $('.eye').magnificPopup({
        type: 'image'
    });

});
$(document).on('click', function (e) {
    if (!$(e.target).closest("content").length) {
        if ($('input#hamburger').is(":checked")) {

            $('#hamburger').click();

        }
    }
    e.stopPropagation();
});

let serv = $('.servicee')
serv.mouseover(function () {
    console.log('hiden-menu');
    $('.hidden-menu').addClass('block-hide');
})
serv.mouseleave(function () {
    console.log('hiden-menu');
    $('.hidden-menu').removeClass('block-hide');
})
