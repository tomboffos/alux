/*document.getElementById("uslugs").onmouseover=function(){
  document.getElementById("dropdown-services2").style.display = "block";
}*/
/*document.getElementById("uslugs").onmouseout=function(){
  document.getElementById("dropdown-services2").style.display = "none";
}*/


var currentTab = 0;
showTab(currentTab);
function showTab(n) {

  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";

  if (n == 0) {
    document.getElementById("wrapper2").style.display = "none";
  } else {
    document.getElementById("wrapper2").style.display = "flex";
  }
  if (n == (x.length - 1)) {
    document.getElementById("wrapper3").style.display = "block";
  } else {
    // document.getElementById("nextBtn").innerHTML = "Далее";
  }

  fixStepIndicator(n)
}

function nextPrev(n) {
  var x = document.getElementsByClassName("tab");
  if (n == 1 && !validateForm()) return false;
  if (currentTab + n >= x.length) {
    // document.getElementById("regForm").submit();
    $.ajax({
        type: 'POST',
        url: '/al2016kz.php',
        data: {
            event: 'order_task',
            name: $('.order-name').val(),
            contacts: $('.order-contacts').val(),
            task: "SMM заказ"
        },
        cache: false,
        success: function (responce) {
            if (responce == 'done') {
                swal('Заказ успешно отправлен!', 'В ближайшее время мы свяжемся с вами.', 'success');
            }
        }
    });
    return false;
  } else {
    x[currentTab].style.display = "none";
    currentTab = currentTab + n;
    showTab(currentTab);
  }
}

function validateForm() {
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  for (i = 0; i < y.length; i++) {
    if (y[i].value == "") {
      y[i].className += " invalid";
      valid = false;
    }
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid;
}

function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}


$('.slider-sinlge').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       arrows: true,
       fade: false,
       adaptiveHeight: true,
       infinite: true,
       useTransform: true,
       speed: 400,
       dots:true,
       cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
   customPaging: function (slider, i) {

       console.log(slider);
       return  (i + 1) + '/' + slider.slideCount;
   }
   });
        var ctx = document.getElementById('myChart').getContext('2d');
   var myChart = new Chart(ctx, {
       type: 'pie',
       data: {
           datasets: [{
               data: [45, 55],
               backgroundColor: [
                 'rgb(238, 155, 252)',
                 'rgb(126, 127, 217)',
               ],
               borderColor: [
                   'rgb(238, 155, 252)',
                   'rgb(126, 127, 217)',
               ],
               borderWidth: 1
           }]
       },
   });
   var ctx = document.getElementById('chartage').getContext('2d');
   var chartage = new Chart(ctx, {
       type: 'pie',
       data: {
           datasets: [{
               data: [59, 31],
               backgroundColor:['#fff','#fff' ],
               borderColor: [
                   'rgb(243, 224, 148)',
                   'rgb(162, 166, 166)',
               ],
               borderWidth: 3
          }]
       },
   });
$('.slider-owl').owlCarousel(function(){

});
