(function () {
    let reviews = [...document.getElementById('reviews').children];
    let reviewsOnPage = [];
    let currentPage = 0;
    let reviewsPerPage = 1;

    const previousPageBtn = document.querySelector(".pagination .previous");
    const nextPageBtn = document.querySelector(".pagination .next");

    previousPageBtn.addEventListener('click', () => { previousPage(); });
    nextPageBtn.onclick = nextPage;

    // number of reviews per page based on screen width
    if (window.innerWidth > 768) {
        reviewsPerPage = 2;
    } 
    if (window.innerWidth > 1000) {
        reviewsPerPage = 3;
    }
    if (window.innerWidth > 1400) {
        reviewsPerPage = 4;
    }

    let totalPages = Math.ceil(reviews.length / reviewsPerPage);
    nextPage();

    function nextPage() {
        if(currentPage >= totalPages) return;
        currentPage++;
        loadreviewsOnPage();
    }

    function previousPage() {
        if(currentPage >= 0)
            currentPage--;
        loadreviewsOnPage();
    }

    function loadreviewsOnPage() {
        let begin = ((currentPage - 1) * reviewsPerPage);
        let end = begin + reviewsPerPage;

        reviewsOnPage = reviews.slice(begin, end);
        drawReviews();
        check();
    }

    function drawReviews() {
        let reviewList = document.getElementById("reviews");
        while (reviewList.firstChild) {
            reviewList.removeChild(reviewList.firstChild);
        }
        for (r = 0; r < reviewsOnPage.length; r++) {
            reviewsOnPage[r].classList.add('visible');
            reviewList.appendChild(reviewsOnPage[r]); // TODO continue here
        }
    }

    function check() {
        document.querySelector(".pagination .next").disabled = currentPage == reviews.length ? true : false;
        document.querySelector(".pagination .previous").disabled = currentPage == 1 ? true : false;
    }
})();