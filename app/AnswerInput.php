<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerInput extends Model
{
    //
    protected $table = 'answer_input';
    protected $fillable = [
        'test_step_id',
        'title'
    ];
}
