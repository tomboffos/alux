<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepAnswers extends Model
{
    public function answers()
    {
        return $this->belongsTo('App\TestStep');
    }
}
