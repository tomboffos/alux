<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use File;
use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function validator($errors,$rules,$messages = []) {
        return Validator::make($errors,$rules,$messages);
    }
    protected function uploadFile($path , $file){

        $fileName = Str::random(5).time().'.'.$file->getClientOriginalExtension();
        $filePath = public_path($path);
        $file->move($filePath, $fileName);
        // dd($path.$fileName);
        return $path.$fileName;

    }
    protected function deleteFile(string $path){
        if (File::exists($path)) {
            File::delete($path);
            return true;
        }
        else{
            return false;
        }
    }

}
