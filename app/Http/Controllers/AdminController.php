<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Certificate;
use App\Logotype;
use App\Post;
use App\Project;
use App\Project_image;
use App\Review;
use App\Setting;
use App\StepAnswers;
use App\AnswerInput;
use App\Test;
use App\TestStep;
use Facade\Ignition\DumpRecorder\Dump;
use File;
use Illuminate\Support\Facades\Storage;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    //
    public function CustomAnswer($id){

        $custom  = AnswerInput::where('test_step_id',$id)->get();
        return view('admin.test_answer',compact('custom'));
    }
    public function Login(){
        return view('admin.login');
    }
    public function Log(Request $request){
        $rules = [
            'login' => 'required',
            'password' => 'required'
        ];
        $messages =[
            'login.required' => 'Введите логин',
            'password.required' => 'Введите пароль'
        ];
        $validator = $this->validator($request->all(), $rules, $messages);
        if($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            $admin = Admin::where('login', $request['login'])->where('password', $request['login'])->first();
            if (!$admin){
                return back()->withErrors('Не правильный логин или пароль');
            }else{
                session()->put('admin',$admin);
                return redirect()->route('admin.Main');
            }
        }


    }

    public function Main(){
        return view('admin.main');
    }

    public function Tests()
    {
        $data['tests'] = Test::get();
        return view('admin.tests',$data);
    }

    public function createTest(Request $request)
    {
        $validatedData = $request->validate([
           'name' => 'required'
        ]);
        $test = new Test();
        $test['name'] = $validatedData['name'];
        $test->save();

        return back()->with('message','Создано');
    }

    public function Test($id)
    {
        $data['test'] = Test::find($id);
        $data['steps'] = TestStep::where('test_id',$data['test']['id'])->get();
         $data['answers'] = StepAnswers::where('test_step_id',$data['steps']->first()['id'])->get();
        return view('admin.test',$data);
    }

    public function createStep(Request $request){
        $validated = $request->validate([
            'name' => 'required',
            'test_id' => 'required'
        ]);


        $step = new TestStep();
        $step['question'] = $request['name'];
        $step['test_id'] = $request['test_id'];
        $step->save();

        return redirect()->back();
    }

    public function Step($id)
    {
        $data['step'] = TestStep::find($id);
        $data['answers'] = StepAnswers::where('test_step_id',$data['step']['id'])->get();
        return view('admin.step',$data);
    }

    public function createAnswer(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'test_step_id' => 'required',
            // 'category' => 'required'
        ]);


        $step = new StepAnswers();
        $step['answer'] = $request['name'];
        $step['test_step_id'] = $request['test_step_id'];
        $step['next_step_id'] = $request['category'];
        $step['counter'] = 0;
        $step->save();

        return redirect()->back();
    }

    public function EditProject($id)
    {
        $data['project'] = Project::find($id);

        return view('admin.project_edit',$data);
    }

    public function Projects(){
        $data['projects'] = Project::orderBy('created_at', 'desc')->paginate(8);
        return view('admin.projects',$data);
    }
    public function DeleteProject($id){
        $project = Project::find($id);
        if ($project['logo_path'])
            File::delete(public_path($project['logo_path']));
        if ($project['background_path'])
            File::delete(public_path($project['background_path']));
        if ($project['web_image'])
            File::delete(public_path($project['web_image']));
        if ($project['letter_path'])
            File::delete(public_path($project['letter_path']));
        if (!$project->mobiles->isEmpty()){
            foreach ($project->mobiles as $mobile){
                if ($mobile['image_path'])
                    File::delete(public_path($project['image_path']));
                $mobile->delete();
            }
        }
        $project->delete();
        return redirect()->route('admin.Projects')->with('message','Удалено');
    }

    public function deleteWeb($id,$key){
        $project = Project::find($id);

        $new_item = json_decode($project->web_image);
        unset($new_item[$key]);
        $project->update([
            'web_image' => json_encode($new_item)
        ]);

        return back()->with('message', 'Удалено');
    }

    public function FindProject(Request $request){
        $rules = [
            'find' => 'required'
        ];
        $messages = [
            'find.required' => 'Введите что нибудь в строку поиска'
        ];
        $validator = $this->validator($request->all(),$rules,$messages);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            $data['projects'] = Project::where('title','LIKE','%'.$request['find'].'%')->paginate(8);
            return view('admin.projects',$data);

        }
    }
    public function Upload(Request $request){
        $rules = [
            'logo_path' => 'required',
            'title' => 'required',
            'link' => 'required',
            'description' => 'required' ,


            'background_path' => 'required',
            'technologies' => 'required'



        ];
        $messages = [
            "logo_path.required" => 'Загрузите логотип',
            "title.required" => "Введите название",
            "link.required" => "Введите ссылку",

            "review.required" => "Введите отзыв клиента",
            "background_path.required" => "Загрузите задний фон",
            "description.required" => "Введите описание",
            "technologies.required" => "Введите стек технологий"




        ];
        $validator = $this->validator($request->all(),$rules, $messages);
        if ($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }else{

            if (!$request->has('web') && !$request->has('mobile') && !$request->has('branding')){
                return back()->withInput()->withErrors('Выбрите хотя бы одну категорию');

            }

            $project = new Project();
            if ($request['web']=='on'){
                $project['web'] = 1;
            }

            if ($request['mobile']=='on'){
                $project['mobile'] = 1;

            }
            if ($request['mobile']=='on'){
                $project['branding'] = 1;
            }
            $project['description'] = $request['description'];
            $project['technologies'] = $request['technologies'];

            $project['title'] = $request['title'];
            $project['link'] = $request['link'];

            $project['link_to_page'] = $request['link_to_page'];
            $project['client_review'] = $request['review'];

            if ($request->hasFile('logo_path')){

                $project['logo_path'] = $this->uploadFile('/uploads/logotypes/',$request->file('logo_path'));
            }
            if($request->hasFile('background_path')){
                $project['background_path'] = $this->uploadFile('/uploads/project_bg/',$request->file('background_path'));
            }

            if ($request->hasFile('main_image'))
                $project['main_image'] = $this->uploadFile('/uploads/web/',$request->file('main_image'));
            if ($request->has('web_image')){
                $images = [];
                foreach ($request->web_image as $image){
                    $images[] = $this->uploadFile('/uploads/web/',$image);
                }
                $images = json_encode($images);

                $project['web_image'] = $images;
            }
            if ($request->hasFile('letter_path')){
                $project['letter_path'] = $this->uploadFile('/uploads/letters/',$request->file('letter_path'));
            }

            $project->save();

            if ($request->hasFile('mobile_images')){
                $images = $request->file('mobile_images');
                foreach ($images as $image){
                    $project_image = new Project_image();
                    $project_image['project_id'] = $project['id'];
                    $project_image['image_path'] = $this->uploadFile('/uploads/mobile_image/',$image);
                    $project_image->save();

                }
            }
            return back()->with('message','Работа добавлена');
        }
    }
    public function Settings(){
        $data['settings'] = Setting::where('types','settings_index')->paginate(10);

        return view('admin.settings',$data);
    }
    public function MetaTags(){
        $data['settings'] = Setting::where('types','meta_tags')->paginate(10);
        return view('admin.settings',$data);
    }

    public function DeleteQuestion($id)
    {
        $question = TestStep::find($id);
        $answers = StepAnswers::where('test_step_id',$question['id'])->get();
        if ($answers){
            foreach ($answers as $answer){
                $answer->delete();
            }
        }
        $question->delete();

        return back()->with('message','Удалено');
    }

    public function EditPost($id)
    {
        $data['post'] = Post::find($id);
        return view('admin.edit_post',$data);
    }

    public function UpdatePost(Request $request)
    {
        $post = Post::find($request->post_id);
        if ($request->has('title')){
            $post['title'] = $request->title;
        }

        if ($request->has('text'))
            $post['text'] = $request->text;

        $post->save();
        return back()->with('message','Обновлено');
    }

    public function DeleteAnswer($id)
    {
        $answer = StepAnswers::find($id);
        $answer->delete();
        return back()->with('message','Удалено');

    }
    public function DeleteCustom($id)
    {
        $custom = AnswerInput::find($id);
        $custom->delete();
        return back()->with('message','Удалено');

    }
    public function SettingEdit(Request $request){
        $rules = [
            'id' => 'required',
            'content' => 'required'
        ];
        $messages = [
            'id.required' => 'Ошибка',
            'content.required' => 'Введите значение'
        ];
        $validator = $this->validator($request->all(),$rules, $messages);
        if ($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }else{
            $setting = Setting::find($request['id']);
            if ($setting['setting_part']  == 'icon_1' || $setting['setting_part']  == 'icon_2' ||
                $setting['setting_part']  == 'icon_3' || $setting['setting_part']  == 'icon_4'){
                $setting['content'] = $this->uploadFile('/uploads/icons/', $request->file('content'));

            }else{
                $setting['content'] = $request['content'];
                $setting->save();
            }

            return back()->with('message','Изменено');

        }
    }
    public function Logotypes(){
        $data['logotypes'] = Logotype::paginate(12);
        return view('admin.logotypes',$data);
    }

    public function AddLogotype(Request $request){
        $rules = [
            'image_path' => 'required'
        ];
        $messages = [
            'image_path.required' => 'Загрузите фото'
        ];
        $validator = $this->validator($request->all(),$rules,$messages);
        if ($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }else{
            $logotype = new Logotype();
            $logotype['image_path'] = $this->uploadFile('/uploads/logotypes/',$request->file('image_path'));
            $logotype->save();
            return back()->with('message','Добавлено');
        }

    }
    public function DeleteLogotype($id){
        $logo = Logotype::find($id);
        File::delete(public_path($logo['image_path']));
        $logo->delete();
        return back()->with('message','Удалено');

    }
    public function Reviews(){
        $data['reviews'] = Review::paginate(10);
        return view('admin.reviews',$data);
    }
    public function DeleteReview($id){
        $review = Review::find($id);
        $review->delete();
        return back()->with('message','Удалено');
    }
    public function CreateReview(Request $request){
        $rules = [
            'author' => 'required',
            'review' =>'required'
        ];
        $messages = [
            'author.required'=>  'Напишите имя автора',
            'review.required' => 'Напишите отзыв'
        ];
        $validator = $this->validator($request->all(),$rules,$messages);
        if ($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }else{
            $review = new Review();
            $review['author'] = $request['author'];
            $review['review'] = $request['review'];
            $review->save();
            return back()->with('message','Добавлено');

        }
    }
    public function Posts(){
        $data['posts'] = Post::paginate(9);
        return view('admin.posts',$data);
    }
    public function DeletePost($id){
        $post = Post::find($id);
        $post->delete();
        return back()->with('message','Удалено');
    }
    public function CreatePost(Request $request){
        $rules = [
            'title' => 'required',
            'text' =>'required'
        ];
        $messages = [
            'title.required'=>  'Напишите имя автора',
            'text.required' => 'Напишите отзыв'
        ];
        $validator = $this->validator($request->all(),$rules,$messages);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());

        }else{
            $post = new Post();
            $post['title'] = $request['title'];
            $post['text'] = $request['text'];
            $post->save();
            return back()->with('message','Добавлено');
        }
    }
    public function Certificates(){
        $data['certificates'] = Certificate::paginate(9);
        return view('admin.certificates',$data);
    }
    public function DeleteCertificate($id){
        $certificate = Certificate::find($id);
        File::delete(public_path($certificate['mini_logo']));
        File::delete(public_path($certificate['image_link']));
        $certificate->delete();
        return back()->with('message','Удалено');
    }
    public function AddCertificate(Request $request){
        $rules = [
            'certificate' => 'required',
            'logo' => 'required',
            'link' => 'required',
            'title' => 'required'
        ];
        $messages = [
            'certificate.required' => 'Загрузите сертифика',
            'logo.required' => 'Загрузите логотип',
            'link.required' => 'Введите ссылку',
            'title.required' => 'Введите название компаний'
        ];
        $validator = $this->validator($request->all(),$rules,$messages);
        if ($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }else{
            $certificate = new Certificate();
            $certificate['link'] = $request['link'];
            $certificate['image_link'] = $this->uploadFile('/certificates/certificates/',$request->file('certificate'));
            $certificate['mini_logo'] = $this->uploadFile('/certificates/logotypes/',$request->file('logo'));
            $certificate['title'] = $request['title'];
            $certificate->save();

            return back()->with('message','Добавлено');

        }


    }

    public function UpdateProject(Request $request) //не забыть
    {
        $project = Project::find($request->project_id);
        $images = $project['web_image'];
        $images = json_decode($images);
        $validatedData = $request->validate([
            'project_id' => 'required',
            'title'  => '',
            'logo_path' => '',
            'background_path' => '',
            'web' => '',
            'mobile' =>'',
            'branding' => '',
            'web_image' => '',
            'mobile_images'=> '',
            'link_to_page' => '',
            'letter_path' => '',
            'link' => '',
            'review' => '',
            'technologies' => '',
            'description' => '',
            'main_image' => '',
            'order' => '',
            'client_review' => ''

        ]);
        foreach ($validatedData as $key=>$value){
            if ($key == 'project_id'){

            }else {
                $project[$key] = $value;
                if ($key == 'logo_path')
                    $project[$key] = $this->uploadFile('/uploads/logotypes/',$request->file($key));

                if ($key == 'background_path')
                    $project[$key] =$this->uploadFile('/uploads/project_bg/',$request->file($key));

                if ($key == 'letter_path'){
                        $project[$key] = $this->uploadFile('/uploads/letters/',$request->file($key));
                    }

                if ($key == 'web_image'){

                    foreach ($request->web_image as $image){
                        $images[] = $this->uploadFile('/uploads/web/',$image);
                    }

                    $project['web_image'] = json_encode($images);


                }

                if ($key == 'main_image')
                    $project['main_image'] = $this->uploadFile('/uploads/web/',$request->file($key));

                if ($key == 'web')
                    $project[$key] = 1;

                if ($key == 'mobile')
                    $project[$key] = 1;

                if ($key == 'branding')
                    $project[$key] = 1;
            }
        }
        $project->save();
        return back();
    }
}