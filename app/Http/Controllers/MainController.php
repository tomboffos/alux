<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Logotype;
use App\Post;
use App\Project;
use App\Review;
use App\Setting;
use App\StepAnswers;
use App\AnswerInput;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function Index(Request $request){
        $data['active'] = 'main';
        $data['settings'] = Setting::where('types','settings_index')->orderBy('id','asc')->get();

        $data['logotypes'] = Logotype::limit(10)->orderBy('id','desc')->get();
        $data['reviews'] = Review::limit(10)->orderBy('id','desc')->get();
        $data['posts'] = Post::get();
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_index')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_index')->first();

        $data['title'] = Setting::where('setting_part','meta_title_index')->first();
        return view('index', $data);
    }
    public function Shop(){
        $data['active']  = 'services';




        $data['meta_tag_keywords']['content'] = 'Разработка и создание интернет магазина в алматы и Казахстане';



        $data['meta_tag_description']['content'] = 'В нашей веб студии Вы можете заказать разработку профессионального интернет магазина под ключ в Алматы. Созданием занимаются лучшие программисты Казахстана.';

        $data['title']['content'] = 'Создание интернет магазина в Алматы: Цены на разработку';

        return view('shop',$data);
    }
    public function CustomRequest(Request $request)
    {
        # code...
        AnswerInput::create([
            'test_step_id' =>$request->step_id,
            'title' => $request->title,
        ]);
        return response()->json(['msg' => 'Успешно прошли опрос'],200);
    }
    public function CorporateSite(){
        $data['active']  = 'services';




        $data['meta_tag_keywords']['content'] = 'Разработка корпоративных сайтов, создание корпоративных сайтов.';


        $data['meta_tag_description']['content'] = 'в веб студии Алматы Люкс вы можете заказать разработку корпоративного веб сайта под ключ. Более 11 лет опыта, разработано свыше 300 проектов различной сложности. Собственная cms и экслюзивны дизайн.';

        $data['title']['content'] = 'Разработка корпоративных сайтов в Алматы, заказать создание сайта';

        return view('corporate',$data);
    }
    public function Brand(){
        $data['active']  = 'services';




        $data['meta_tag_keywords']['content'] = '';



        $data['meta_tag_description']['content'] = 'Фирменный стиль Алматы. Дизайн логотипа и создание брендбука, разработка дизайна упаковки в Казахстане, brand book, разработка этикетки';

        $data['title']['content'] = 'Разработка фирменного стиля в Алматы. Дизайн логотипа и создание брендбука,  дизайн этикетки и упаковки';

        return view('brand',$data);
    }
    public function Video(){
        $data['active']  = 'services';




        $data['meta_tag_keywords']['content'] = 'создание видеороликов, разработка видеороликов, создать видеоролик';



        $data['meta_tag_description']['content'] = 'Создание видеороликов для бизнеса на профессиональном уровне в Казахстане. Студия «A-LUX» - предоставит полный набор услуг по видеомаркетингу: создание рекламных видеороликов, изготовление, продвижение, а так же раскрутка видеороликов в интернете.';

        $data['title']['content'] = 'Создание видеоинфографики в Алматы, заказать разработку видео роликов в Казахстане';

        return view('video',$data);
    }
    public function SMM(){
        $data['active']  = 'services';




        $data['meta_tag_keywords']['content'] = 'Разработка и создание интернет магазина в алматы и Казахстане';



        $data['meta_tag_description']['content'] = 'Комплексное ведение и продвижение аккаунта Инстаграм в Алматы и других городах Казахстана. Оставьте заявку на продвижение Вашего аккаунта в Instagram и получите бесплатный аудит';

        $data['title']['content'] = 'Комплексное продвижение в социальных сетях: инстаграм, вконтакте в алматы, Раскрутка instagram качество.';

        return view('smm',$data);
    }
    public function Landing(){
        $data['active']  = 'services';




        $data['meta_tag_keywords']['content'] = Setting::where('setting_part','LIKE','%meta_keywords_shop%')->first();



        $data['meta_tag_description']['content'] = 'Создание лендингов под ключ в Алматы. Разработка уникальных Landing Page для любых задач с индивидуальным дизайном не дорого';
        $data['title']['content'] = 'Создание Landing Page в Алматы. Разработка крутых лэндинг-пейдж под ключ';

        return view('landing',$data);
    }

    public function MobileApps(){
        $data['active']  = 'services';
        $data['title']['content'] = 'Заказать разработку и создание мобильных приложений под iOS и Android в Алматы и других городах Казахстана.';
        $data['meta_tag_keywords']['content'] = '';
        $data['meta_tag_description']['content'] = 'Компания ALUX занимаемся разработкой мобильных приложений под iPhone и Android. Создаем приложения для B2B и B2C. 12 лет на рынке.';

        return view('mobile_app',$data);
    }
    public function Projects(){
        $data['active']  = 'projects';

        $data['projects'] = Project::orderBy('created_at','desc')->paginate(8);
        $data['projects_cat'] = 'all';
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_portfolio')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_portfolio')->first();

		  $data['title'] = Setting::where('setting_part','meta_tag_title_portfolio')->first();
        return view('projects', $data);
    }


    public function Work($url){
        $data['project'] = Project::where('link',$url)->first();
        $data['active'] = 'projects';
        $data['previous_project'] = Project::find($data['project']['id']-1);
        $data['next_project'] = Project::find($data['project']['id']+1);
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_portfolio')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_portfolio')->first();
        $data['title'] = Setting::where('setting_part','meta_tag_title_portfolio')->first();
        return view('work', $data);
    }
    public function Services(){
        $data['active']  = 'services';
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_portfolio')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_portfolio')->first();

        $data['title'] = Setting::where('setting_part','meta_tag_title_portfolio')->first();
        return view('services',$data);
    }
    public function WebProjects(){
        $data['active'] = 'projects';
        $data['projects'] = Project::where('web',1)->orderBy('created_at','desc')->paginate(8);
        $data['projects_cat'] = 'web';
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_portfolio')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_portfolio')->first();

        $data['title'] = Setting::where('setting_part','meta_tag_title_portfolio')->first();
        return view('projects',$data);

    }
    public function MobileProjects(){
        $data['active'] = 'projects';
        $data['projects'] = Project::where('mobile',1)->orderBy('order','asc')->paginate(8);
        $data['projects_cat'] = 'mobile';
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_portfolio')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_portfolio')->first();

        $data['title'] = Setting::where('setting_part','meta_tag_title_portfolio')->first();
        return view('projects',$data);
    }
    public function BrandingProjects(){
        $data['active'] = 'projects';
        $data['projects'] = Project::where('branding',1)->orderBy('order','asc')->paginate(8);
        $data['projects_cat'] = 'branding';
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_portfolio')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_portfolio')->first();

        $data['title'] = Setting::where('setting_part','meta_tag_title_portfolio')->first();
        return view('projects',$data);
    }
    public function Contacts(){
        $data['active'] = 'contacts';
        $data['settings'] = Setting::get();
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_keywords_contacts')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_description_contacts')->first();

        $data['title'] = Setting::where('setting_part','meta_title_contacts')->first();
        return view('contacts',$data);
    }
    public function CustomerReviews(){
        $data['active'] = 'reviews';
        $data['settings'] = Setting::get();
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_keywords_reviews')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_description_reviews')->first();
        $data['reviews'] = Certificate::get();
        $data['title'] = Setting::where('setting_part','meta_title_reviews')->first();

        return view('reviews',$data);
    }
    public function AboutUs(){
        $data['active'] = 'about';
        $data['settings'] = Setting::get();
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_keywords_about')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_description_about')->first();

        $data['title'] = Setting::where('setting_part','meta_title_about')->first();

        return view('about',$data);
    }

    public function Request(Request $request)
    {

        $validation =  $request->validate([
           'answers' => 'required'
        ]);

        $answers = json_decode($validation['answers']);

        foreach ($answers as $answer){
            $test_answer =  StepAnswers::find($answer->answer_id);
            $test_answer['counter'] += 1;
            $test_answer->save();
        }

        return response()->json(['msg' => 'Успешно прошли опрос'],200);
    }

    public function PostPage($id)
    {
        $data['post'] = Post::find($id);
        $data['title']['content'] = $data['post']['title'];
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_index')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_index')->first();
        $data['active'] = 'main';


        return view('post',$data);
    }

    public function Posts()
    {
        $data['posts'] = Post::orderBy('id','desc')->paginate(9);
        $data['active'] = 'main';

        $data['title']['content'] = "Все новости Алюкс";
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_index')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_index')->first();

        return view('posts',$data);
    }


    public function Game()
    {
        $data['meta_tag_keywords'] = Setting::where('setting_part','meta_tag_keywords_index')->first();
        $data['meta_tag_description'] = Setting::where('setting_part','meta_tag_description_index')->first();
        $data['active'] = 'is_index';
        $data['title'] = Setting::where('setting_part','meta_title_index')->first();
        return view('game',$data);
    }
}
