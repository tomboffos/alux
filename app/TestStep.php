<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestStep extends Model
{
    //

    public function answers()
    {
        return $this->hasMany('App\StepAnswers');
    }
}
