<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $casts = ['web_image'];
    public function mobiles(){
        return $this->hasMany('App\Project_image');
    }

    protected $fillable = [
        'web_image'
    ];
}
