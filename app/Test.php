<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    //
    public function steps()
    {
        return $this->hasMany('App\TestStep');
    }
}
